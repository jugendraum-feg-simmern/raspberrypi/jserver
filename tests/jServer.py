#!/usr/bin/python3
import socket
import msgpack
import sys
from os import strerror


class jServer:
    def __init__(self, ip='raspberrypi.local', port=6330):
        """create a socket and connect to jServer"""
        self.ip = ip
        self.port = port
        try:
            # create a socket
            print('connecting to {}:{}'.format(ip, port))
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.sock.connect((ip, port))
            cfg = self.sock.recv(4096)
            print(cfg.decode('utf-8'))
        except Exception as e:
            print("error: {}".format(strerror(e.errno)))
            exit(e.errno)

        # self.loadFunctions()
        self.loadParameters()

    def loadFunctions(self):
        """load functions provided by jServer"""
        try:
            self.sock.send(msgpack.packb(["gimmedafuncs", None], use_bin_type=True))
            raw = self.sock.recv(4096)
            funcs = msgpack.unpackb(raw, raw=True)
        except Exception as e:
            print("Error loading functions: {}".format(e))
            return None

        if funcs[0] != 0:
            print("Error loading functions:\n\tjServer returned: {}".format(funcs))
            return None

        for k, v in funcs[1].items():
            f_str = self.dictToFuncString(k, v)
            exec(f_str)

    def dictToFuncString(self, func_name, func_dict):
        """create a string that defines a function"""
        # resist the urge to format this !!
        template = \
            'def {}(self, {}):\n\
    """{} {}({})"""\n\
    self.sock.send(msgpack.packb(["{}", {}], use_bin_type=True))\n\
    raw = self.sock.recv(4096)\n\
    resp = msgpack.unpackb(raw, raw=True)\n\
    return resp\n\
setattr(jServer, "{}", {})'
        args_string = ''
        args_type_string = ''
        fname = func_name.decode('utf-8')

        # extract return value from dict -> all remaining items are arguments
        ret_val_type = func_dict.pop(b'ret_val').decode('utf-8')

        # prepare string to be inserted into tepmplate
        for k, v in func_dict.items():
            args_string += '{}, '.format(k.decode('utf-8'))
            args_type_string += '{} {}, '.format(v.decode('utf-8'), k.decode('utf-8'))
        args_type_string = args_type_string[:-2]
        args_string = args_string[:-2]

        # insert all into the tepmplate
        return template.format(fname, args_string,
                               ret_val_type, fname, args_type_string,
                               fname, args_string,
                               fname, fname)

    def loadParameters(self):
        try:
            self.sock.send(msgpack.packb([0, "gimmedaparams"], use_bin_type=True))
            raw = self.sock.recv(4096)
            params = msgpack.unpackb(raw, raw=True)
        except Exception as e:
            print("Error loading params: {}".format(e))
            return None

        print(params)

    def set(self, param, val):
        try:
            self.sock.send(msgpack.packb([1, param, val], use_bin_type=True))
            raw = self.sock.recv(4096)
        except Exception as e:
            print(e)
            return

        print(msgpack.unpackb(raw, raw=True))

    def get(self, param, val=None):
        try:
            self.sock.send(msgpack.packb([0, param, val], use_bin_type=True))
            raw = self.sock.recv(4096)
        except Exception as e:
            print(e)
            return

        print(msgpack.unpackb(raw, raw=True))

    def listen(self):
        while True:
            raw = self.sock.recv(4096)
            print(msgpack.unpackb(raw, raw=True))

    def close(self):
        print("closing connection")
        self.sock.close()
