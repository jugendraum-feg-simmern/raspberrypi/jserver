#include <QCoreApplication>
#include <QDebug>
#include <QThread>
#include <QObject>
#include <iostream>

#include "inc/hardware/artnet/artnet.h"
#include "inc/hardware/artnet_controller.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    std::cout << "sizeof(art_packet_t) = " << sizeof(art_packet_t) << std::endl;
    std::cout << "sizeof(artpoll_packet_t) = " << sizeof(artpoll_packet_t) << std::endl;
    std::cout << "sizeof(artdmx_packet_t) = " << sizeof(artdmx_packet_t) << std::endl;

    ArtNetController* controller = new ArtNetController();

    auto onNodeAdded = [=](){
        artnetnode_ptr_t node = controller->getNode(QHostAddress("::ffff:192.168.0.247"));
        qDebug() << node;
        if (node.data() != nullptr){
            node.data()->writeDMXBuffer(0, 0, 192);
            node.data()->writeDMXBuffer(0, 1, 255);
            node.data()->writeDMXBuffer(0, 2, 238);
        }
    };

    QObject::connect(controller, &ArtNetController::nodeAdded, onNodeAdded);

    return a.exec();
}
