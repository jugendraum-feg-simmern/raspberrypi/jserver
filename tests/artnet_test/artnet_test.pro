QT -= gui
QT += core network

CONFIG += c++14 console
CONFIG -= app_bundle

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp \
        src/hardware/artnet/artnet_node.cpp \
        src/hardware/artnet/ultim8.cpp \
        src/hardware/artnet_controller.cpp

# remove debug output on release builds
CONFIG(release| debug|release):DEFINES += QT_NO_DEBUG_OUTPUT

# rules for deployment
target.path = /home/pi
INSTALLS += target

HEADERS += \
    inc/config.h \
    inc/hardware/artnet/artnet.h \
    inc/hardware/artnet/artnet_node.h \
    inc/hardware/artnet/ultim8.h \
    inc/hardware/artnet_controller.h \
    inc/tools/errno.h
