
#include <iostream>
#include <string>
#include <functional>
#include <vector>

#include "../../inc/tools/file_handler.h"

class Test {
    public:
        Test(int val) {
            number_ = val;
            file_handler_ = new FileHandler(".test");
            file_handler_->addParameter(&number_, "Test::number_");
        }

        int get(){
            return file_handler_->readValue(&number_);
        }

        void set(int val) {
            number_ = val;
            file_handler_->writeValue(&number_);
        }

    private:
        int number_;
        FileHandler* file_handler_;
};

int main(int argc, char const *argv[]) {
    Test* test = new Test(0);
    std::cout << "Exp: 0 got: " << test->get() << std::endl;
    test->set(1);
    std::cout << "Exp: 1 got: " << test->get() << std::endl;



    return 0;
}
