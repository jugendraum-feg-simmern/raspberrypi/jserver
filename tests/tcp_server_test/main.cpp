#include <QCoreApplication>

#include "server.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    server s(QHostAddress::Any, 6330);

    return a.exec();
}
