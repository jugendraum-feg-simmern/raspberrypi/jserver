#ifndef TELETUBBY_H
#define TELETUBBY_H

#include <QObject>
#include <QVariant>

class Teletubby : public QObject
{
    Q_OBJECT
public:
    explicit Teletubby(QObject *parent = nullptr);
    int setSize(double size);
    int getSize(double& size);
private:
    double size_;
signals:
    void updated(QString param, QVariant val);

signals:

};

#endif // TELETUBBY_H
