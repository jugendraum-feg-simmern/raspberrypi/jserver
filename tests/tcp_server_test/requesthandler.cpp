#include "requesthandler.h"

RequestHandler::RequestHandler(QList<QTcpSocket*>& socket_list)
{
    socket_list_ = &socket_list;
    api_params_.insert(
        REQUEST_PARAMS_STRING,
        QList<handler_func_t>({
            [&](handler_func_args_t& args, handler_func_reply_t& reply){
                QListIterator<QString> iter(api_params_.keys());
                reply << 0;     // pretend reading api parameters went well
                while (iter.hasNext()){
                    reply << iter.next();
                }
            }
        })
    );
}

RequestHandler::~RequestHandler()
{
    qDebug() << Q_FUNC_INFO;
}

void RequestHandler::handleRequest(QVariant& request, QTcpSocket& sender)
{
    QVariantList req;
    int op_id;
    QString param;
    handler_func_args_t args;

    // convert request to QVariantList
    try {
        req = request.toList();
    } catch (QException e) {
        qDebug() << "error converting request to QVariantList: " << e.what();
        MSGPACK_REPLY("error", QString("Conversion error: %1").arg(e.what()), sender);
        return;
    }

    // get operation id, parameter and arguments
    try {
        op_id = req.takeFirst().toInt();
        param = req.takeFirst().toString();
        args = (!req.isEmpty()) ? req : handler_func_args_t();
    } catch (QException e) {
        qDebug() << "error parsing request: " << e.what();
        MSGPACK_REPLY_ERROR(param, 127, QString("Parsing error: %1").arg(e.what()), sender);
        return;
    }

    // validate parameter
    if (not api_params_.contains(param)){
        qDebug() << param << " is not a valid parameter";
        MSGPACK_REPLY_ERROR(param, 127, QString("%1 is not a valid parameter").arg(param), sender);
        return;
    }

    // call function for parameter
    handler_func_reply_t ret_val;
    try {
        api_params_[param].at(op_id)(args, ret_val);
    } catch (QException e) {
        qDebug() << "Error calling function: " << param << " with arg " << args;
        MSGPACK_REPLY_ERROR(param, 127, QString("Error calling %1(%2): %3").arg(param).arg("arg").arg(e.what()), sender);
        return;
    }

    // send return value to client if accessor called or mutator returned not 0
    // changed values are broadcasted to all clients via signal 'updated' and slot
    // 'onAPIParameterChanged'
    if (op_id == 0){
        MSGPACK_REPLY(param, ret_val, sender);
    }
    else if ((op_id == 1) && (ret_val.first().toInt() != 0)) {
        MSGPACK_REPLY_ERROR(param, ret_val.first().toInt(), QString("Mutator for %1 returned %2").arg(param).arg(ret_val.first().toInt()), sender);
    }
}

void RequestHandler::onAPIParameterChanged(QString param, QVariant val)
{
    qDebug() << param << "changed to " << val;
    qDebug() << "sender: " << sender();
    for (auto s : *socket_list_) {
        QVariantList vl;
        vl << param << 0 << val;
        s->write(MsgPack::pack(vl));
        qDebug() << "sending: " << vl;
    }
}
