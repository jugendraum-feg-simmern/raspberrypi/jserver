#ifndef REQUESTHANDLER_H
#define REQUESTHANDLER_H

#define MSGPACK_REPLY(PARAM, VAL, SOCKET){\
    QVariantList vl; \
    vl << PARAM << VAL; \
    qDebug() << "sending: " << vl; \
    SOCKET.write(MsgPack::pack(vl)); \
}

#define MSGPACK_REPLY_ERROR(PARAM, ERR_CODE, ERR_MSG, SOCKET){\
    QVariantList vl; \
    vl << PARAM << ERR_CODE << ERR_MSG; \
    qDebug() << "sending: " << vl; \
    SOCKET.write(MsgPack::pack(vl)); \
}

#define REQUEST_FUNCS_STRING "gimmedafuncs"
#define REQUEST_PARAMS_STRING "APIParameters"

#include <QMap>
#include <QList>
#include <QDebug>
#include <QObject>
#include <QString>
#include <QVariant>
#include <QMetaType>
#include <QTcpSocket>
#include <QByteArray>
#include <QException>

#include <iostream>
#include <typeinfo>
#include <functional>

#include <qmsgpack/msgpack.h>

class RequestHandler : public QObject
{
    Q_OBJECT
public:
    typedef QList<QVariant> handler_func_args_t;
    typedef QList<QVariant> handler_func_reply_t;
    typedef std::function<void(handler_func_args_t&, handler_func_reply_t&)> handler_func_t;

    RequestHandler(QList<QTcpSocket*>& socket_list);
    ~RequestHandler();
    void handleRequest(QVariant& request, QTcpSocket& sender);
    static QVariantList getHandlerFunctionDescriptions(int val);

    // api parameters
    template <class S, typename T>
    void addAPIParameter(QString param, S**cptr, int(S::*acc)(T&), int(S::*mut)(T)){
        QList<handler_func_t> func_list;
        func_list.append(
            // accessor first
            [=](handler_func_args_t& args, handler_func_reply_t& reply){
                T ret_val;
                int ret_code = (*cptr->*acc)(ret_val);
                reply << ret_code << ret_val;
            });
        func_list.append(
            // mutator second
            [=](handler_func_args_t& args, handler_func_reply_t& reply) {
                int ret_code = (*cptr->*mut)(args.at(0).value<T>());
                reply << ret_code << args.at(0).value<T>();
        });
        api_params_.insert(param, func_list);
        connect(*cptr, SIGNAL(updated(QString, QVariant)), this, SLOT(onAPIParameterChanged(QString, QVariant)), Qt::UniqueConnection);
    }

private:
    QMap<QString, handler_func_t> handler_funcs_;
    QMap<QString, QMap<QString, QString>> handler_funcs_descriptions_;
    QList<QTcpSocket*>* socket_list_;

    QMap<QString, QList<handler_func_t>> api_params_;

private slots:
    void onAPIParameterChanged(QString param, QVariant val);
};

#endif // REQUESTHANDLER_H
