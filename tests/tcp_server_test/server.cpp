#include "server.h"

server::server(QHostAddress ip, uint16_t port) :
  srv_(this),
  request_handler_(sockets_)
{
    srv_.listen(ip, port);
    connect(&srv_, SIGNAL(newConnection()), this, SLOT(onNewConnection()));
    qDebug() << "listening on " << ip.toString() << ":" << port;

    teletubby_ = new Teletubby();

    request_handler_.addAPIParameter("TeletubbySize", &teletubby_, &Teletubby::getSize, &Teletubby::setSize);
}

server::~server()
{
    // nop
    qDebug() << "closing...";
}

void server::onNewConnection()
{
    qDebug() << "new connection";

    QTcpSocket* client_socket = srv_.nextPendingConnection();
    connect(client_socket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
    connect(client_socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(onSocketStateChanged(QAbstractSocket::SocketState)));

    sockets_.push_back(client_socket);
}

void server::onSocketStateChanged(QAbstractSocket::SocketState state)
{
    if (state == QAbstractSocket::UnconnectedState)
    {
        QTcpSocket* s = static_cast<QTcpSocket*>(QObject::sender());
        sockets_.removeOne(s);
        qDebug() << "closing client socket " << s;
    }
}

void server::onReadyRead()
{
    QTcpSocket* s = static_cast<QTcpSocket*>(QObject::sender());
    QByteArray raw = s->readAll();
    QVariant data = MsgPack::unpack(raw);
    qDebug() << "received: " << data;

    request_handler_.handleRequest(data, *s);
}
