#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QHostAddress>
#include <QAbstractSocket>
#include <QDebug>

#include <qmsgpack/msgpack.h>

#include "requesthandler.h"
#include "teletubby.h"

class server : public QObject
{
        Q_OBJECT

public:
    explicit server(QHostAddress ip, uint16_t port);
    ~server();
private slots:
    void onNewConnection();
    void onSocketStateChanged(QAbstractSocket::SocketState state);
    void onReadyRead();
private:
    QTcpServer srv_;
    QList<QTcpSocket*> sockets_;
    RequestHandler request_handler_;
    Teletubby* teletubby_;
};

#endif // SERVER_H
