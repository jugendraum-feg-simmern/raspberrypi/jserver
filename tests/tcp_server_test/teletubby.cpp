#include "teletubby.h"

Teletubby::Teletubby(QObject *parent) : QObject(parent)
{
    size_ = 0;
}

int Teletubby::setSize(double size)
{
   if (size > 3)
       return 1;

    size_ = size;
    emit updated("TeletubbySize", size_);
    return 0;
}

int Teletubby::getSize(double& size)
{
    size = size_;
    return 0;
}


