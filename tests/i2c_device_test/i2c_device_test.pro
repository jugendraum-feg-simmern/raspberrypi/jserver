QT -= gui
Qt += core

CONFIG += c++11 console
CONFIG -= app_bundle
CONFIG -= app_bundle

# emit warnings when using deprecated features
DEFINES += QT_DEPRECATED_WARNINGS

# rules for deployment
target.path = /home/jonas
INSTALLS += target

# specific libs for raspberrypi
LIBS += -L/opt/qt5.12.6_rpi/sysroot/usr/local/lib -lwiringPi -lqmsgpack
INCLUDEPATH += /opt/qt5.12.6_rpi/sysroot/usr/local/include

HEADERS += \
    inc/hardware/i2cdevice.h \
    inc/tools/logging.h

SOURCES += \
    src/hardware/i2cdevice.cpp \
    src/tools/logging.cpp \
    main.cpp
