#include <QCoreApplication>

#include "inc/hardware/i2cdevice.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    I2CDevice* dut = new I2CDevice(42);
    // test opening devices
    qDebug() << "open returned" << dut->openI2CDevice();
    // test writing
    uint8_t val = 42;
    qDebug() << "write returned:" << dut->writeReg8(0, val);
    // test reading
    qDebug() << "reading returned: " << dut->readReg8(0, val) << "and read val =" << val;
    // test checking
    qDebug() << "check returned:" << dut->checkI2CDevice();

    return a.exec();
}
