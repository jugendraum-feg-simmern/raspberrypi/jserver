#!/bin/env python3
import signal
import sys

start_time = 0

def signal_handler(sig, frame):
    end_time = datetime.now()
    print("\nstarted at:  ", start_time.ctime())
    print("finished at: ", end_time.ctime())
    print("delta:       ", str(end_time - start_time))
    sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)


import smbus
from random import random
from datetime import datetime

fd = smbus.SMBus(1)
pca_1 = 0x07
pca_2 = 0x1f
mcp = 0x20

success_counter = 0

def write(addr, reg, val):
    fd.write_byte_data(addr, reg, val)

def read(addr, reg):
    return fd.read_byte_data(addr, reg)

def test(dut):
    x = int(random() * 255)
    reg = int(random() * 16) + 2

    write(dut, reg, x)
    aw = read(dut, reg)
    assert aw == x, f"ERROR: {aw} != {x} after {success_counter} successes"

if __name__ == '__main__':
    start_time = datetime.now()
    while True:
        test(0x07)
        success_counter += 1
        if success_counter % 50 == 0:
            print("{} transactions successfull".format(success_counter))
    signal_handler(42, 0)
