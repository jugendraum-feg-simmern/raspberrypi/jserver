#    _ ____
#   (_) ___|  ___ _ ____   _____ _ __       _ __  _ __ ___
#   | \___ \ / _ \ '__\ \ / / _ \ '__|     | '_ \| '__/ _ \
#   | |___) |  __/ |   \ V /  __/ |     _  | |_) | | | (_) |
#  _/ |____/ \___|_|    \_/ \___|_|    (_) | .__/|_|  \___/
# |__/                                     |_|
#
QT += core network concurrent remoteobjects gui

TARGET = jServer
CONFIG += c++14 console
CONFIG -= app_bundle

# emit warnings when using deprecated features
DEFINES += QT_DEPRECATED_WARNINGS

# remove debug output on release builds
CONFIG(release| debug|release):DEFINES += QT_NO_DEBUG_OUTPUT

REPC_SOURCE += \
    jreplicas/dimmable_light.rep \
    jreplicas/rgb_light.rep \
    jreplicas/simple_light.rep

# add header files sorted by folders and alphabetical order
HEADERS += \
    inc/config.h
HEADERS += \
    inc/core/decken_rgb_streifen.h \
    inc/core/dimmable_light.h \
    inc/core/hauptlicht_streifen.h \
    inc/core/j_element.h \
    inc/core/jugendraum.h \
    inc/core/simple_light.h \
    inc/core/simple_rgb.h
HEADERS += \
    inc/hal/analog_input.h \
    inc/hal/analog_output.h \
    inc/hal/digital_input.h \
    inc/hal/digital_output.h \
    inc/hal/hal.h \
    inc/hal/observer.h
HEADERS += \
    inc/hardware/artnet_controller.h \
    inc/hardware/artnet/artnet.h \
    inc/hardware/artnet/artnet_node.h \
    inc/hardware/artnet/ultim8.h \
    inc/hardware/hardware.h \
    inc/hardware/i2cdevice.h \
    inc/hardware/iodevice.h \
    inc/hardware/mcp23017.h \
    inc/hardware/mcp23017_register.h \
    inc/hardware/pca9635.h \
    inc/hardware/pca9635_register.h \
    inc/hardware/powersupply.h \
    inc/hardware/system_monitor.h \
    inc/hardware/temperature_sensor_udp.h \
    inc/hardware/wall_switch.h
HEADERS += \
    inc/temperature/j_temperature_controller.h \
    inc/temperature/temperature.h \
    inc/temperature/temperature_controlled.h
HEADERS += \
    inc/tools/errno.h \
    inc/tools/j_return_type.h \
    inc/tools/logging.h \
    inc/tools/storable.h

# add source files sorted by folders and alphabetical order
SOURCES += \
    src/config.cpp \
    src/main.cpp
SOURCES += \
    src/core/decken_rgb_streifen.cpp \
    src/core/dimmable_light.cpp \
    src/core/hauptlicht_streifen.cpp \
    src/core/j_element.cpp \
    src/core/jugendraum.cpp \
    src/core/simple_light.cpp \
    src/core/simple_rgb.cpp
SOURCES += \
    src/hal/analog_input.cpp \
    src/hal/analog_output.cpp \
    src/hal/digital_input.cpp \
    src/hal/digital_output.cpp \
    src/hal/hal.cpp
SOURCES += \
    src/hardware/artnet_controller.cpp \
    src/hardware/artnet/artnet_node.cpp \
    src/hardware/artnet/ultim8.cpp \
    src/hardware/hardware.cpp \
    src/hardware/i2cdevice.cpp \
    src/hardware/iodevice.cpp \
    src/hardware/mcp23017.cpp \
    src/hardware/pca9635.cpp \
    src/hardware/powersupply.cpp \
    src/hardware/system_monitor.cpp \
    src/hardware/temperature_sensor_udp.cpp \
    src/hardware/wall_switch.cpp
SOURCES += \
    src/temperature/j_temperature_controller.cpp \
    src/temperature/temperature.cpp
SOURCES += \
    src/tools/logging.cpp \
    src/tools/storable.cpp

# Installation
unix:target.path = /usr/local/bin
unix:config.path = /etc/jugendraum
unix:config.files = config/*
unix:service.path = /etc/systemd/system
unix:service.files = jserver.service

INSTALLS += target config service
