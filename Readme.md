# jServer
Device server to run on the RaspberryPi. It is in control over the hardware and exposes control elements via *Qt Remote Objects*.

## Installation
#### 0. Prerequisites
##### 0.1 Install Qt5
We use *Qt Remote Objects* which is not available in the official repos, i.e. we cannot `apt install` it. Hence, we build Qt from the sources.
```shell
wget https://download.qt.io/official_releases/qt/5.12/5.12.9/single/qt-everywhere-src-5.12.9.tar.xz
md5sum qt-everywhere-src-5.12.9.tar.xz
curl https://download.qt.io/official_releases/qt/5.12/5.12.9/single/md5sums.txt
```
Verify the checksums, then extract the archive and create the hf configuration
```shell
tar -xvf qt-everywhere-src-5.12.9.tar.xz
cd qt-everywhere-src-5.12.9
cp -r qtbase/mkspecs/linux-arm-gnueabi-g++ qtbase/mkspecs/linux-arm-gnueabihf-g++
sed -i -e 's/arm-linux-gnueabi-/arm-linux-gnueabihf-/g' qtbase/mkspecs/linux-arm-gnueabihf-g++/qmake.conf
```
Configure the project and specify the components we want to build. Note that, in order to speed up the build, we exclude all modules that are not required for this project.
```shell
./configure -opensource -confirm-license -no-opengl -no-gbm -skip qt3d -skip qtactiveqt -skip qtandroidextras -skip qtcanvas3d -skip qtcharts -skip qtconnectivity -skip qtdatavis3d -skip qtdoc -skip qtgamepad -skip qtgraphicaleffects -skip qtimageformats -skip qtlocation -skip qtmacextras -skip qtmultimedia -skip qtpurchasing -skip qtscript -skip qtscxml -skip qtsensors -skip qtserialbus -skip qtserialport -skip qtspeech -skip qtsvg -skip qttranslations -skip qtvirtualkeyboard -skip qtwayland -skip qtwebchannel -skip qtwebengine -skip qtwebglplugin -skip qtwebsockets -skip qtwebview -skip qtwinextras -skip qtx11extras -skip qtxmlpatterns -nomake tests -nomake examples -make libs -pkg-config -no-use-gold-linker -v 2>&1 | tee configure.log
```
Make sure, the configure script finishes without any errors, then compile and install (takes about 2h on a Pi 3 B)
```shell
make -j 4 2>&1 | tee make.log
sudo make install 2>&1 | tee install.log
export PATH=/usr/local/Qt-5.12.9/bin:$PATH
```
Verify everything is working by running
```shell
qmake --version
```

#### 1. Installation
Clone the repo
```shell
git clone https://gitlab.com/jugendraum-feg-simmern/raspberrypi/jserver.git
cd jserver
git submodule update --init
```
Build the executable
```shell
mkdir build && cd build
qmake ..
make -j 4
```
Install it!
```shell
sudo make install
```

#### 2. Autostart
The installation creates a system service. To have systemd take care of starting the server on boot and restarting it upon failure, enable the service (the `--now` flag starts the service right away)
```shell
sudo systemctl enable --now jserver.service
```
