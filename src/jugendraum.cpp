/*!
 * \file jugendraum.cpp
 * \brief Source der Jugendraum Klasse.
 */
#include "inc/jugendraum.h"

Jugendraum::Jugendraum(QHostAddress ip, uint16_t port) :
    srv_(this),
    request_handler_(sockets_)
{
    qDebug() << Q_FUNC_INFO;
    log(EVENT_LOG, "jServer started");
    log(ERROR_LOG, "[ INFO ] jServer started"); // log to ERROR_LOG to make restarts directly visible in the fileA

    hw::init();

    update_timer_ = new QTimer();
    connect(update_timer_, SIGNAL(timeout()), this, SLOT(update()));
    update_timer_->start(HW_UPDATE_INTERVAL_MS);

    single_shot_timer_ = new QTimer();
    single_shot_timer_->setSingleShot(true);

    // create all the members
    theken_rgb = new ThekenRGB();
    theken_deckenlicht = new ThekenDeckenlicht();
    paletten_licht = new PalettenLicht();
    wand_rgb = new WandRGB();
    hauptlicht_ = new Hauptlicht();

    for (int i = 1; i < 19; i++)
    	rgb_deckenlicht.append(new RGBStreifen(i));
    	
	all_elements_.append(theken_rgb);
	all_elements_.append(theken_deckenlicht);
	all_elements_.append(paletten_licht);
    all_elements_.append(wand_rgb);
    all_elements_.append(hauptlicht_);
		
	for (auto rgb_streifen: rgb_deckenlicht)
        all_elements_.append(rgb_streifen);

    // tcp
    srv_.listen(ip, port);
    connect(&srv_, SIGNAL(newConnection()), this, SLOT(onNewConnection()));
    log(EVENT_LOG, QString("listening on %1:%2").arg(ip.toString()).arg(port));
    createAPI();
}

Jugendraum::~Jugendraum()
{
    update_timer_->stop();
    delete update_timer_;

    qDebug() << Q_FUNC_INFO;
    delete paletten_licht;
    delete theken_rgb;
    delete theken_deckenlicht;
    delete wand_rgb;
    delete hauptlicht_;
        
    for (auto rgb_streifen: rgb_deckenlicht)
        delete rgb_streifen;

    log(EVENT_LOG, "ControlPanel-3-0 closed");
    log(ERROR_LOG, "[ INFO ] ControlPanel-3-0 closed");
}

void Jugendraum::update()
{
    if (hw::switchEventPending())
    {
        jugendraum_is_on_ = !jugendraum_is_on_;
        loadAllFromFile((jugendraum_is_on_) ? DEFAULT_SETTINGS : OFF_SETTINGS);
        emit updated();
        hw::switchEventProcessed();
    }
	
	for (auto elem: all_elements_)
		elem->update();

    checkJugendraumOn();
    checkHauptlichtOn();
}

void Jugendraum::saveAllToFile(QString filename)
{
    log(EVENT_LOG, QString("Saved settings to %1").arg(filename));

	for (auto elem: all_elements_)
		elem->saveToFile(filename);
}   

void Jugendraum::loadAllFromFile(QString filename)
{
	for (auto elem: all_elements_)
		elem->loadFromFile(filename);
}

void Jugendraum::checkHauptlichtOn()
{
    bool tmp = hauptlicht_->isOn();

    // only act when changed
    if (tmp && !hauptlicht_on_)
    {
        hauptlicht_on_ = tmp;
        hw::writeState(NETZTEIL48V, hauptlicht_on_);
        single_shot_timer_->stop();
    }
    else if(!tmp && hauptlicht_on_)
    {
        hauptlicht_on_ = tmp; // should this be set here or in setHauptlichtRelaisOff() ??
        single_shot_timer_->singleShot(5000, this, SLOT(setHauptlichtRelaisOff()));
    }
}

void Jugendraum::checkJugendraumOn()
{
    bool tmp = false;

    for (auto elem: all_elements_)
        tmp |= elem->isOn();

    jugendraum_is_on_ = tmp;
}

void Jugendraum::setHauptlichtRelaisOff()
{
    qDebug() << Q_FUNC_INFO;

    hauptlicht_on_ = false;
    hw::writeState(NETZTEIL48V, false);
}

void Jugendraum::onNewConnection()
{
    QTcpSocket* client_socket = srv_.nextPendingConnection();
    connect(client_socket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
    connect(client_socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(onSocketStateChanged(QAbstractSocket::SocketState)));
    client_socket->write(QByteArray::fromStdString("Connection accepeted"));

    sockets_.append(client_socket);
    qDebug() << QString("Connection from %1:%2").arg(client_socket->peerAddress().toString()).arg(client_socket->peerPort());
    log(EVENT_LOG, QString("Connection from %1:%2").arg(client_socket->peerAddress().toString()).arg(client_socket->peerPort()));
    qDebug() << sockets_;
}

void Jugendraum::onSocketStateChanged(QAbstractSocket::SocketState state)
{
    QTcpSocket* s = static_cast<QTcpSocket*>(QObject::sender());
    qDebug() << QString("Socket of %1:%2 changed state to %3").arg(s->peerAddress().toString()).arg(s->peerPort()).arg(state);
    log(EVENT_LOG, QString("Socket of %1:%2 changed state to %3").arg(s->peerAddress().toString()).arg(s->peerPort()).arg(state));

    if (state == QAbstractSocket::UnconnectedState)
    {
        qDebug() << QString("Lost Connection to %1:%2 -> closing socket").arg(s->peerAddress().toString()).arg(s->peerPort());
        log(EVENT_LOG, QString("Lost Connection to %1:%2 -> closing socket").arg(s->peerAddress().toString()).arg(s->peerPort()));
        sockets_.removeOne(s);
    }
}

void Jugendraum::onReadyRead()
{
    QTcpSocket* s = static_cast<QTcpSocket*>(QObject::sender());
    QByteArray raw = s->readAll();
    QVariant data = MsgPack::unpack(raw);
    qDebug() << "received: " << data;
    qDebug() << "sender: " << s;

    request_handler_.handleRequest(data, *s);
}

void Jugendraum::createAPI()
{
    request_handler_.addAPIParameter("HauptlichtOn", &hauptlicht_, &Hauptlicht::isOn, &Hauptlicht::setOn);
    request_handler_.addAPIParameter("HauptlichtHelligkeit", &hauptlicht_, &Hauptlicht::getBrightness, &Hauptlicht::setBrightness);
    request_handler_.addAPIParameter("HauptlichtGruppeOn", &hauptlicht_, &Hauptlicht::getGroupOn, &Hauptlicht::setGroupOn);
    request_handler_.addAPIParameter("HauptlichtGruppeHelligkeit", &hauptlicht_, &Hauptlicht::getGroupBrightness, &Hauptlicht::setGroupBrightness);
    request_handler_.addAPIParameter("HauptlichtStreifenOn", &hauptlicht_, &Hauptlicht::getStripeOn, &Hauptlicht::setStripeOn);
    request_handler_.addAPIParameter("HauptlichtStreifenHelligkeit", &hauptlicht_, &Hauptlicht::getStripeBrightness, &Hauptlicht::setStripeBrightness);

    request_handler_.addAPIParameter<WandRGB, bool>("WandRGBOn", &wand_rgb, &WandRGB::isOn, &WandRGB::setOn);
    request_handler_.addAPIParameter<WandRGB, int>("WandRGBRedValue", &wand_rgb, &WandRGB::getRedValue, &WandRGB::setRedValue);
    request_handler_.addAPIParameter<WandRGB, int>("WandRGBGreenValue", &wand_rgb, &WandRGB::getGreenValue, &WandRGB::setGreenValue);
    request_handler_.addAPIParameter<WandRGB, int>("WandRGBBlueValue", &wand_rgb, &WandRGB::getBlueValue, &WandRGB::setBlueValue);

    request_handler_.addAPIParameter("PaletteOn", &paletten_licht, &PalettenLicht::isOn, &PalettenLicht::setOn);

    request_handler_.addAPIParameter("ThekeDeckenlichtOn", &theken_deckenlicht, &ThekenDeckenlicht::isOn, &ThekenDeckenlicht::setOn);
    request_handler_.addAPIParameter<ThekenRGB, bool>("ThekeRGBOn", &theken_rgb, &ThekenRGB::isOn, &ThekenRGB::setOn);
    request_handler_.addAPIParameter<ThekenRGB, int>("ThekeRGBRedValue", &theken_rgb, &ThekenRGB::getRedValue, &ThekenRGB::setRedValue);
    request_handler_.addAPIParameter<ThekenRGB, int>("ThekeRGBGreenValue", &theken_rgb, &ThekenRGB::getGreenValue, &ThekenRGB::setGreenValue);
    request_handler_.addAPIParameter<ThekenRGB, int>("ThekeRGBBlueValue", &theken_rgb, &ThekenRGB::getBlueValue, &ThekenRGB::setBlueValue);
}
