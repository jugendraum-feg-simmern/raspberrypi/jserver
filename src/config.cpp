#include "inc/config.h"

#include "inc/tools/storable.h"
#include <QMap>

namespace Config {

    namespace {
        Storable* config_reader = new Storable();

        // map enum to default filenames. Can be overwritten via setFileForCategory
        QMap<Category, QString> filenames = {
            {Core,            "/etc/jugendraum/core_config.json"},
            {Hardware,        "/etc/jugendraum/hardware_config.json"},
            {Temperature,     "/etc/jugendraum/temperature_config.json"},
        };
    }
    namespace Files
    {
        QString Logfile(getElementByKey(Category::Core, {"logfile", "Files"}).toString());
        QString YouthSettings(getElementByKey(Category::Core, {"youth_settings", "Files"}).toString());
        QString TeensSettings(getElementByKey(Category::Core, {"teens_settings", "Files"}).toString());
        QString DefaultSettings(getElementByKey(Category::Core, {"default_settings", "Files"}).toString());
        QString OffSettings(getElementByKey(Category::Core, {"off_settings", "Files"}).toString());
        QString TemperatureSettings(getElementByKey(Category::Core, {"temperature_settings", "Files"}).toString());
    }

    void setFileForCategory(Category category, QString file)
    {
        filenames[category] = file;
    }

    QJsonValue getElementByKey(Category category, QStringList path)
    {
        return config_reader->getJsonValueFromFile(filenames.value(category), path);
    }

    QJsonValue getElementByKey(Category category, QString key)
    {
        return getElementByKey(category, QStringList({key}));
    }

}
