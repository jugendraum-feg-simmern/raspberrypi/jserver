/*!
 * \authors
 *      + [CPLMaddin](https://gitlab.com/CPLMaddin)
 *      + [ChrisSeibel](https://gitlab.com/chrisseibel)
 *      + [JonasWitzenrath](https://gitlab.com/jwitzenrath)
 * \version 4.0
 *
 * \copyright GNU Public License
 *
 * \mainpage device server running on the RaspberryPi
 *
 */

#include <QCommandLineParser>
#include <QCommandLineOption>
#include <QDebug>

#include "inc/core/jugendraum.h"
#include "inc/hardware/hardware.h"
#include "inc/temperature/temperature.h"

int main(int argc, char *argv[])
{
    // start and configure QCoreApplication
    QCoreApplication jServer(argc, argv);
    QCoreApplication::setApplicationName("jServer");
    QCoreApplication::setApplicationVersion("4.0");

    // configure parser
    QCommandLineParser parser;
    parser.setApplicationDescription("device server controlling the room");
    parser.addHelpOption();
    parser.addVersionOption();
    // add options to parser
    QCommandLineOption traceCategoryOption("trace-category",
                                           "enable tracing for <category>, use '*' to trace every category",
                                           "category");
    parser.addOption(traceCategoryOption);
    QCommandLineOption networkVerbosityOption("network-verbosity",
                                              "set network verbosity, default = 'info'",
                                              "network-verbosity");
    parser.addOption(networkVerbosityOption);
    QCommandLineOption apiVerbosityOption("api-verbosity",
                                          "set verbosity for api related calls, default = 'warning'",
                                          "api-verbosity");
    QCommandLineOption mockingOption({"m", "mocking"},
                                     "enable mocking of hardware namespace");
    parser.addOption(mockingOption);
    parser.addOption(apiVerbosityOption);
    QCommandLineOption skipHardwareInitOption({"s", "skip-init"},
                                              "skip the initialization of the hardware");
    parser.addOption(skipHardwareInitOption);

    // process given options
    parser.process(jServer);

    // setup logging rules based upun user request
    QString filter_rules("trace.*.debug=false\nfile=false\n");
    if (parser.isSet(traceCategoryOption)) filter_rules.append(QString("trace.%1.debug=true\n").arg(parser.value(traceCategoryOption)));
    if (parser.isSet(networkVerbosityOption)) filter_rules.append(QString("network.%1=true\n").arg(parser.value(networkVerbosityOption)));
    if (parser.isSet(apiVerbosityOption)) filter_rules.append(QString("api.%1=true\n").arg(parser.value(apiVerbosityOption)));
    QLoggingCategory::setFilterRules(filter_rules);

    // setup message handling
    qInstallMessageHandler(messageHandler);

    // initialize the hardware
    hw::init(parser.isSet(mockingOption), parser.isSet(skipHardwareInitOption));

    Jugendraum jugendraum("Jugendraum");

    // start the event loop.
    int rc = jServer.exec();

    // Somehow the eventloop returned. Clean up and exit.
    Temperature::exit();
    hw::exit();

    return rc;
}
