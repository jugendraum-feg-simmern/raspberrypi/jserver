/*!
 * \file j_element.cpp
 * \brief source of JElement
 */
#include "inc/core/j_element.h"

void JElement::serialize(QJsonObject &json)
{
    json["on"] = on();
}

void JElement::deserialize(const QJsonObject &json)
{
    if (json.contains("on") and json["on"].isBool())
        setOn(json["on"].toBool());
}
