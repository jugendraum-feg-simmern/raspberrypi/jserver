#include "inc/core/dimmable_light.h"

#include <exception>
#include "inc/hal/hal.h"
#include "inc/tools/logging.h"

DimmableLight::DimmableLight(QString id, QJsonValue config, JElement *parent)
    : DimmableLightSource(nullptr),
      JElement(id, parent)
{
    qCDebug(traceInit) << "constructing" << id << "from" << config;

    if (config.isUndefined())
        throw std::runtime_error("Missing config for " + id.toStdString());

    QJsonValue c_analog_output = config["analog_output"];
    if (c_analog_output.isUndefined() or not c_analog_output.isString())
        throw std::runtime_error("Invalid config for " + id.toStdString() + ".analog_output");

    analog_output_name_ = c_analog_output.toString();
    AnalogOutputPtr aout = HAL::getAnalogOutputByName(analog_output_name_);
    if (aout.get() == nullptr)
        connect(HAL::observer, &Observer::analogOutputAdded, this, &DimmableLight::onAnalogOutputAdded);
    else {
        connect(HAL::observer, &Observer::analogOutputRemoved, this, &DimmableLight::onAnalogOutputRemoved);
        connect(aout.get(), &AnalogOutput::valueChanged, this, &DimmableLight::onAnalogOutputValueChanged);
        connect(this, &DimmableLight::requestAnalogOutputValueChange, aout.get(), &AnalogOutput::setValue);
    }
}

bool DimmableLight::on() const
{
    return value() > 0;
}

void DimmableLight::setOn(bool on)
{
    if (on) {
        if (last_value_ == 0)
            return deserialize(getJsonValueFromFile(Config::Files::DefaultSettings).toObject());
        else
            return setValue(last_value_);
    } else {
        last_value_ = value_;
        return setValue(0);
    }
}

quint8 DimmableLight::value() const
{
    return value_;
}

void DimmableLight::setValue(quint8 value)
{
    emit requestAnalogOutputValueChange(value);
}

void DimmableLight::serialize(QJsonObject &json)
{
    json["value"] = value();
}

void DimmableLight::deserialize(const QJsonObject &json)
{
    if (json.contains("value") and json["value"].isDouble())
        setValue(json["value"].toInt(0));
}

void DimmableLight::onAnalogOutputAdded(QString name, AnalogOutputPtr aout)
{
    if (name != analog_output_name_)
        return;

    connect(HAL::observer, &Observer::analogOutputRemoved, this, &DimmableLight::onAnalogOutputRemoved);

    connect(aout.get(), &AnalogOutput::valueChanged, this, &DimmableLight::onAnalogOutputValueChanged);
    connect(this, &DimmableLight::requestAnalogOutputValueChange, aout.get(), &AnalogOutput::setValue);
}

void DimmableLight::onAnalogOutputRemoved(QString name)
{
    // disconnect signals?
}

void DimmableLight::onAnalogOutputValueChanged(quint8 value)
{
    value_ = value;
    emit valueChanged(value_);
}
