/*!
 * \file jugendraum.cpp
 * \brief source of Jugendraum class
 */
#include "inc/core/jugendraum.h"

#include <QDebug>
#include <QList>
#include "inc/hal/hal.h"
#include "inc/hardware/hardware.h"
#include "inc/tools/logging.h"
#include "inc/hardware/mcp23017_register.h"

Jugendraum::Jugendraum(QString id)
    : JElement(id, nullptr)
{
    qCDebug(traceCore) << Q_FUNC_INFO;
    qInfo("%s %s started", QCoreApplication::applicationName().toLocal8Bit().data(), QCoreApplication::applicationVersion().toLocal8Bit().data());

// create Hauptlicht stripes
    // TODO catch name does not contain _ -> will throw ASSERT error
    QJsonValue c_hauptlicht = Config::getElementByKey(Config::Category::Core, "Hauptlicht");
    if (not c_hauptlicht.isArray())
        throw std::runtime_error("invalid config for Hauptlicht");
    QJsonArray c_stripes = c_hauptlicht.toArray();
    for (auto c_stripe : qAsConst(c_stripes)) {
        QJsonObject cs = c_stripe.toObject();
        QString s_name = cs.keys().at(0);
        hauptlicht_.insert(
            s_name.split('_').at(1).toInt(),
            new HauptlichtStreifen(s_name, cs.value(s_name), this)
        );
    }
    for (auto stripe : qAsConst(hauptlicht_)) {
        all_elements_.append(stripe);
        connect(stripe, &HauptlichtStreifen::onChanged, this, &Jugendraum::onElementOnChanged);
    }

// create all DeckenRGB stripes
    // TODO catch name does not contain _ -> will throw ASSERT error
    QJsonValue c_decken_rgb = Config::getElementByKey(Config::Category::Core, "DeckenRGB");
    if (not c_decken_rgb.isArray())
        throw std::runtime_error("invalid config for DeckenRGB");
    c_stripes = c_decken_rgb.toArray();
    for (auto c_stripe : qAsConst(c_stripes)) {
        QJsonObject cs = c_stripe.toObject();
        QString s_name = cs.keys().at(0);
        decken_rgb_.insert(
            s_name.split('_').at(1).toInt(),
            new DeckenRGBStreifen(s_name, cs.value(s_name), this)
        );
    }
    for (auto stripe : qAsConst(hauptlicht_)) {
        all_elements_.append(stripe);
        connect(stripe, &HauptlichtStreifen::onChanged, this, &Jugendraum::onElementOnChanged);
    }

// create other elements
    theken_rgb_ = new SimpleRGB("ThekenRGB", Config::getElementByKey(Config::Category::Core, "ThekenRGB"), this);
    connect(theken_rgb_, &SimpleRGB::onChanged, this, &Jugendraum::onElementOnChanged);
    all_elements_.append(theken_rgb_);

    wand_rgb_ = new SimpleRGB("WandRGB", Config::getElementByKey(Config::Category::Core, "WandRGB"), this);
    connect(wand_rgb_, &SimpleRGB::onChanged, this, &Jugendraum::onElementOnChanged);
    all_elements_.append(wand_rgb_);

    theken_deckenlicht_ = new SimpleLight("ThekenDeckenlicht", Config::getElementByKey(Config::Category::Core, "ThekenDeckenlicht"), this);
    connect(theken_deckenlicht_, &SimpleLight::onChanged, this, &Jugendraum::onElementOnChanged);
    all_elements_.append(theken_deckenlicht_);

    paletten_licht_ = new SimpleLight("Palette", Config::getElementByKey(Config::Category::Core, "Palette"), this);
    connect(paletten_licht_, &SimpleLight::onChanged, this, &Jugendraum::onElementOnChanged);
    all_elements_.append(paletten_licht_);

// connect to wall switch
    connect(hw::wallswitch, &WallSwitch::stateChanged, this, &Jugendraum::onWallSwitchStateChanged);

// connect to indicator relais
    QJsonValue c_indicator_relais = Config::getElementByKey(Config::Category::Core, {"indicator_relais", "Jugendraum"});
    if (c_indicator_relais.isUndefined() or not c_indicator_relais.isString())
        throw std::runtime_error("Invalid config for Jugendraum.indicator_relais");
    indicator_relais_name_ = c_indicator_relais.toString();
    auto dout = HAL::getDigitalOutputByName(indicator_relais_name_);
    if (dout.get() == nullptr)
        connect(HAL::observer, &Observer::digitalOutputAdded, this, &Jugendraum::onDigitalOutputAdded);
    else {
        connect(HAL::observer, &Observer::digitalOutputRemoved, this, &Jugendraum::onDigitalOutputRemoved);
        connect(this, &Jugendraum::onChanged, dout.get(), &DigitalOutput::setState);
    }
}

Jugendraum::~Jugendraum()
{
    qCDebug(traceCore) << Q_FUNC_INFO;

    for (auto s : qAsConst(hauptlicht_))
        delete s;
    for (auto s : qAsConst(decken_rgb_))
        delete s;

    delete paletten_licht_;
    delete theken_rgb_;
    delete theken_deckenlicht_;
    delete wand_rgb_;

    qInfo("%s %s closed", QCoreApplication::applicationName().toLocal8Bit().data(), QCoreApplication::applicationVersion().toLocal8Bit().data());
}

bool Jugendraum::on() const
{
    return on_;
}

void Jugendraum::setOn(bool on)
{
    for (auto e : all_elements_)
        e->setOn(on);
}

void Jugendraum::serialize(QJsonObject &json)
{
    // the stripes of hauptlicht_ and decken_rgb_ are grouped in the settings file, so we need to handle those separately
    QJsonObject hl_json;
    for (auto s : qAsConst(hauptlicht_)) {
        QJsonObject e_json;
        s->serialize(e_json);
        hl_json[s->id()] = e_json;
    }
    json["Hauptlicht"] = hl_json;
    QJsonObject rgb_json;
    for (auto s : qAsConst(decken_rgb_)) {
        QJsonObject e_json;
        s->serialize(e_json);
        rgb_json[s->id()] = e_json;
    }
    json["DeckenRGB"] = rgb_json;

    // process all other elements
    for (auto e : QList<JElement*>{paletten_licht_, wand_rgb_, theken_deckenlicht_, theken_rgb_}) {
        QJsonObject e_json;
        e->serialize(e_json);
        json[e->id()] = e_json;
    }
}

void Jugendraum::deserialize(const QJsonObject &json)
{
    // the stripes of hauptlicht_ and decken_rgb_ are grouped in the settings file, so we need to handle those separately
    if (json.contains("Hauptlicht") and json["Hauptlicht"].isObject()) {
        QJsonObject settings = json["Hauptlicht"].toObject();
        for (auto s : qAsConst(hauptlicht_)) {
            if (settings.contains(s->id()))
                s->deserialize(settings[s->id()].toObject());
        }
    }
    if (json.contains("DeckenRGB") and json["DeckenRGB"].isObject()) {
        QJsonObject settings = json["DeckenRGB"].toObject();
        for (auto s : qAsConst(decken_rgb_)) {
            if (settings.contains(s->id()))
                s->deserialize(settings[s->id()].toObject());
        }
    }

    // process all other elements
    for (auto e : QList<JElement*>{paletten_licht_, wand_rgb_, theken_deckenlicht_, theken_rgb_}) {
        e->deserialize(json[e->id()].toObject());
    }
}

void Jugendraum::saveAllToFile(QString filename)
{
    QJsonObject root_object;
    serialize(root_object);
    updateJsonValueInFile(root_object, filename);
}

void Jugendraum::loadAllFromFile(QString filename)
{
    QJsonValue root_value = getJsonValueFromFile(filename);
    if (not root_value.isUndefined() and root_value.isObject())
        deserialize(root_value.toObject());
}

void Jugendraum::onWallSwitchStateChanged()
{
    loadAllFromFile((on_) ? Config::Files::OffSettings : Config::Files::DefaultSettings);
}

void Jugendraum::onElementOnChanged(bool on)
{
    bool tmp = false;
    for (auto e : qAsConst(all_elements_))
        tmp |= e->on();

    if (tmp == on_)
        return;

    on_ = tmp;
    emit onChanged(on_);
}

void Jugendraum::onDigitalOutputAdded(QString name, DigitalOutputPtr dout)
{
    if (name != indicator_relais_name_)
        return;
    connect(HAL::observer, &Observer::digitalOutputRemoved, this, &Jugendraum::onDigitalOutputRemoved);
    connect(this, &Jugendraum::onChanged, dout.get(), &DigitalOutput::setState);
}

void Jugendraum::onDigitalOutputRemoved(QString name)
{
    if (name != indicator_relais_name_)
        return;
    connect(HAL::observer, &Observer::digitalOutputAdded, this, &Jugendraum::onDigitalOutputAdded);
}
