/*!
 * \file rgb_streifen.cpp
 * \brief source of DeckenRGBStreifen
 */
#include "inc/core/decken_rgb_streifen.h"

#include <QJsonArray>

#include "inc/tools/logging.h"
#include "inc/hal/hal.h"
#include <exception>
#include "inc/temperature/temperature.h"


DeckenRGBStreifen::DeckenRGBStreifen(QString id, QJsonValue config, JElement *parent)
    : SimpleRGB(id, config, parent)
{

    qCDebug(traceInit) << "constructing" << id << "from" << config;

    if (config.isUndefined())
        throw std::runtime_error("Missing config for DeckenRGB." + id.toStdString());

    QJsonValue c_powersupply = config["powersupply"];
    if (c_powersupply.isUndefined() or not c_powersupply.isString())
        throw std::runtime_error("Invalid config for DeckenRGB." + id.toStdString() + ".powersupply");

    powersupply_name_ = c_powersupply.toString();
    powersupply_ = HAL::getPowersupplyByName(powersupply_name_);
    if (powersupply_ == nullptr) {
        connect(HAL::observer, &Observer::powersupplyAdded, this, &DeckenRGBStreifen::onPowersupplyAdded);
    } else {
        powersupply_->registerElement(this);
        connect(HAL::observer, &Observer::powersupplyRemoved, this, &DeckenRGBStreifen::onPowersupplyRemoved);
    }

    Temperature::registerToController(Temperature::powersupply, this);
}

 void DeckenRGBStreifen::setColor(QColor color)
 {
    Temperature::constrain(this, color);

    auto handler = [=](QColor col){
        return SimpleRGB::setColor(col);
    };

    powersupply_->handle(handler)(color);
 }

 void DeckenRGBStreifen::onTemperatureDomainChanged(JTemperatureController::Domain)
 {
     setColor(color());
 }

void DeckenRGBStreifen::onPowersupplyAdded(QString name, Powersupply *ps)
{
    if (name != powersupply_name_)
        return;

    powersupply_ = ps;
    powersupply_->registerElement(this);
    disconnect(HAL::observer, &Observer::powersupplyAdded, this, &DeckenRGBStreifen::onPowersupplyAdded);
    connect(HAL::observer, &Observer::powersupplyRemoved, this, &DeckenRGBStreifen::onPowersupplyRemoved);
}

void DeckenRGBStreifen::onPowersupplyRemoved(QString name)
{
    if (name != powersupply_name_)
        return;

    powersupply_ = nullptr;
    disconnect(HAL::observer, &Observer::powersupplyRemoved, this, &DeckenRGBStreifen::onPowersupplyRemoved);
    connect(HAL::observer, &Observer::powersupplyAdded, this, &DeckenRGBStreifen::onPowersupplyAdded);
}
