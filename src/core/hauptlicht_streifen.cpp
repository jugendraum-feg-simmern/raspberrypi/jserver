/*!
 * \file hauptlicht_streifen.cpp
 * \brief source of the HauptlichtStreifen class
 */
#include "inc/core/hauptlicht_streifen.h"

#include "inc/hal/hal.h"
#include "inc/temperature/temperature.h"

#include "inc/tools/logging.h"
#include <exception>

HauptlichtStreifen::HauptlichtStreifen(QString id, QJsonValue config, JElement *parent)
    : DimmableLight(id, config, parent)
{
    qCDebug(traceInit) << "constructing" << id << "from" << config;

    if (config.isUndefined())
        throw std::runtime_error("missing config for HauptlichtStreifen." + id.toStdString());

    QJsonValue c_powersupply = config["powersupply"];
    if (c_powersupply.isUndefined() or not c_powersupply.isString())
        throw std::runtime_error("Invalid config for HauptlichtStreifen" + id.toStdString() + ".powersupply");

    powersupply_name_ = c_powersupply.toString();
    powersupply_ = HAL::getPowersupplyByName(powersupply_name_);
    if (powersupply_ == nullptr) {
        connect(HAL::observer, &Observer::powersupplyAdded, this, &HauptlichtStreifen::onPowersupplyAdded);
    } else {
        powersupply_->registerElement(this);
        connect(HAL::observer, &Observer::powersupplyRemoved, this, &HauptlichtStreifen::onPowersupplyRemoved);
    }

    Temperature::registerToController(Temperature::powersupply, this);
}

void HauptlichtStreifen::onTemperatureDomainChanged(JTemperatureController::Domain domain)
{
    setValue(value());
}

void HauptlichtStreifen::setValue(quint8 value)
{
    if (powersupply_ == nullptr)
        return;

    Temperature::constrain(this, value);

    auto handler = [=](uint8_t b) {
        return DimmableLight::setValue(value);
    };
    powersupply_->handle(handler)(value);
}

void HauptlichtStreifen::onPowersupplyAdded(QString name, Powersupply *ps)
{
    if (name != powersupply_name_)
        return;

    powersupply_ = ps;
    powersupply_->registerElement(this);
    disconnect(HAL::observer, &Observer::powersupplyAdded, this, &HauptlichtStreifen::onPowersupplyAdded);
    connect(HAL::observer, &Observer::powersupplyRemoved, this, &HauptlichtStreifen::onPowersupplyRemoved);
}

void HauptlichtStreifen::onPowersupplyRemoved(QString name)
{
    if (name != powersupply_name_)
        return;

    powersupply_ = nullptr;
    disconnect(HAL::observer, &Observer::powersupplyRemoved, this, &HauptlichtStreifen::onPowersupplyRemoved);
    connect(HAL::observer, &Observer::powersupplyAdded, this, &HauptlichtStreifen::onPowersupplyAdded);
}
