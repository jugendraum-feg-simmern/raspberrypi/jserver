/*!
 * \file simple_light.cpp
 * \brief Source of the SimpleLight class
 */
#include "inc/core/simple_light.h"

#include "inc/hal/hal.h"
#include <exception>

SimpleLight::SimpleLight(QString id, QJsonValue config, JElement *parent)
    : SimpleLightSource(nullptr),
      JElement(id, parent)
{
    qCDebug(traceInit) << "constructing" << id << "from" << config;

    if (config.isUndefined())
        throw std::runtime_error("Missing config for " + id.toStdString());

    QJsonValue c_digital_output = config["digital_output"];
    if (c_digital_output.isUndefined())
        throw std::runtime_error("Missing config for " + id.toStdString() + "digital_ouput");
    digital_output_name_ = c_digital_output.toString();

    DigitalOutputPtr dout = HAL::getDigitalOutputByName(digital_output_name_);
    if (dout == nullptr)
        connect(HAL::observer, &Observer::digitalOutputAdded, this, &SimpleLight::onDigitalOutputAdded);
    else {
        connect(HAL::observer, &Observer::digitalOutputRemoved, this, &SimpleLight::onDigitalOutputRemoved);
        connect(dout.get(), &DigitalOutput::stateChanged, this, &SimpleLight::onDigitalOutputStateChanged);
        connect(this, &SimpleLight::requestDigitalOutputStateChange, dout.get(), &DigitalOutput::setState);
    }
}

bool SimpleLight::on() const
{
    return on_;
}

void SimpleLight::setOn(bool state)
{
    emit requestDigitalOutputStateChange(state);
}

void SimpleLight::onDigitalOutputAdded(QString name, DigitalOutputPtr dout)
{
    if (name != digital_output_name_)
        return;

    disconnect(HAL::observer, &Observer::digitalOutputAdded, this, &SimpleLight::onDigitalOutputAdded);
    connect(HAL::observer, &Observer::digitalOutputRemoved, this, &SimpleLight::onDigitalOutputRemoved);

    connect(dout.get(), &DigitalOutput::stateChanged, this, &SimpleLight::onDigitalOutputStateChanged);
    connect(this, &SimpleLight::requestDigitalOutputStateChange, dout.get(), &DigitalOutput::setState);
}

void SimpleLight::onDigitalOutputRemoved(QString name)
{
    if (name != digital_output_name_)
        return;

    disconnect(HAL::observer, &Observer::digitalOutputRemoved, this, &SimpleLight::onDigitalOutputRemoved);
    connect(HAL::observer, &Observer::digitalOutputAdded, this, &SimpleLight::onDigitalOutputAdded);

    // TODO: do we need to disconnect signals and slots from the output? If so, where do we get a pointer to the (deleted) object from?
}

void SimpleLight::onDigitalOutputStateChanged(bool state)
{
    on_ = state;
    emit onChanged(on_);
}
