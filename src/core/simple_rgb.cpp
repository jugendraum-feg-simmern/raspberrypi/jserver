/*!
 * \file simple_rgb.cpp
 * \brief source of the SimpleRGB class
 */
#include "inc/core/simple_rgb.h"

#include <QJsonArray>
#include <exception>

#include "inc/tools/logging.h"
#include "inc/hal/hal.h"


SimpleRGB::SimpleRGB(QString id, QJsonValue config, JElement *parent)
    : RGBLightSource(nullptr),
      JElement(id, parent)
{
    qCDebug(traceInit) << "constructing" << id << "from" << config;

    if (config.isUndefined())
        throw std::runtime_error("Missing config for " + id.toStdString());

    QJsonValue c_rgb_outputs = config["rgb_outputs"];
    if (c_rgb_outputs.isUndefined() or not c_rgb_outputs.isArray())
        throw std::runtime_error("Invalid config for " + id.toStdString() + ".rgb_outputs");
    QJsonArray rgb_outputs = c_rgb_outputs.toArray();

    if (rgb_outputs.count() != 3)
        throw std::runtime_error("Invalid config for " + id.toStdString() + ".rgb_outputs");
    red_output_name_ = rgb_outputs.at(0).toString();
    green_output_name_ = rgb_outputs.at(1).toString();
    blue_output_name_ = rgb_outputs.at(2).toString();

    AnalogOutputPtr aout = HAL::getAnalogOutputByName(red_output_name_);
    if (aout.get() != nullptr) {
        connect(this, &SimpleRGB::requestRedOutputChange, aout.get(), &AnalogOutput::setValue);
        connect(aout.get(), &AnalogOutput::valueChanged, this, &SimpleRGB::onRedOutputChanged);
    }
    aout = HAL::getAnalogOutputByName(green_output_name_);
    if (aout.get() != nullptr) {
        connect(this, &SimpleRGB::requestGreenOutputChange, aout.get(), &AnalogOutput::setValue);
        connect(aout.get(), &AnalogOutput::valueChanged, this, &SimpleRGB::onGreenOutputChanged);
    }
    aout = HAL::getAnalogOutputByName(blue_output_name_);
    if (aout.get() != nullptr) {
        connect(this, &SimpleRGB::requestBlueOutputChange, aout.get(), &AnalogOutput::setValue);
        connect(aout.get(), &AnalogOutput::valueChanged, this, &SimpleRGB::onBlueOutputChanged);
    }

    connect(HAL::observer, &Observer::analogOutputAdded, this, &SimpleRGB::onAnalogOutputAdded);
    connect(HAL::observer, &Observer::analogOutputRemoved, this, &SimpleRGB::onAnalogOutputRemoved);
}

void SimpleRGB::serialize(QJsonObject &json)
{
    json["color"] = color().name();
}

void SimpleRGB::deserialize(const QJsonObject &json)
{
    if (json.contains("color") and json["color"].isString())
        setColor(json["color"].toString("#000000"));
}

bool SimpleRGB::on() const
{
    return color().lightness() > 0;
}

void SimpleRGB::setOn(bool on)
{
    if (on) {
        if (last_color_.lightness() == 0) {
            QJsonObject default_settings = getJsonValueFromFile(Config::Files::DefaultSettings).toObject();
            return deserialize(default_settings);
        } else {
            return setColor(last_color_);
        }
    } else {
        last_color_ = color_;
        return setColor(Qt::GlobalColor::black);
    }
}

QColor SimpleRGB::color() const
{
    return color_;
}

void SimpleRGB::setColor(QColor color)
{
    emit requestRedOutputChange(color.red());
    emit requestGreenOutputChange(color.green());
    emit requestBlueOutputChange(color.blue());
}

void SimpleRGB::onAnalogOutputAdded(QString name, AnalogOutputPtr aout)
{
    if (name == red_output_name_) {
        connect(this, &SimpleRGB::requestRedOutputChange, aout.get(), &AnalogOutput::setValue);
        connect(aout.get(), &AnalogOutput::valueChanged, this, &SimpleRGB::onRedOutputChanged);
        return;
    }
    if (name == green_output_name_) {
        connect(this, &SimpleRGB::requestGreenOutputChange, aout.get(), &AnalogOutput::setValue);
        connect(aout.get(), &AnalogOutput::valueChanged, this, &SimpleRGB::onGreenOutputChanged);
        return;
    }
    if (name == blue_output_name_) {
        connect(this, &SimpleRGB::requestBlueOutputChange, aout.get(), &AnalogOutput::setValue);
        connect(aout.get(), &AnalogOutput::valueChanged, this, &SimpleRGB::onBlueOutputChanged);
        return;
    }
}

void SimpleRGB::onAnalogOutputRemoved(QString name)
{
    // disconnect signals?
}

void SimpleRGB::onRedOutputChanged(quint8 value)
{
    color_ = QColor(value, color_.green(), color_.blue());
    emit colorChanged(color_);
}

void SimpleRGB::onGreenOutputChanged(quint8 value)
{
    color_ = QColor(color_.red(), value, color_.blue());
    emit colorChanged(color_);
}

void SimpleRGB::onBlueOutputChanged(quint8 value)
{
    color_ = QColor(color_.red(), color_.green(), value);
    emit colorChanged(color_);
}
