#include "inc/hal/digital_input.h"

DigitalInput::DigitalInput(QString name)
    : name_(name)
{
}

void DigitalInput::update(bool state, bool force_signal)
{
    if ((state == state_) and not force_signal)
        return;
    state_ = state;
    emit stateChanged(state_);
}
