#include "inc/hal/digital_output.h"

DigitalOutput::DigitalOutput(QString name, setter_func_t setter, getter_func_t getter)
    : name_(name),
      getter_(std::move(getter)),
      setter_(std::move(setter))
{

}

bool DigitalOutput::state()
{
    bool tmp_state = false;
    getter_(tmp_state);
    return tmp_state;
}

void DigitalOutput::setState(bool state)
{
    if (state == state_)
        return;

    int rc = setter_(state);

    if (rc)
        return;

    state_ = state;
    emit stateChanged(state_);
}
