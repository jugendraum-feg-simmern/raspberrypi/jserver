#include "inc/hal/analog_output.h"

AnalogOutput::AnalogOutput(QString name, AnalogOutput::setter_func_t setter, AnalogOutput::getter_func_t getter)
    : name_(name),
      getter_(std::move(getter)),
      setter_(std::move(setter))
{

}

quint8 AnalogOutput::value()
{
    quint8 tmp_value;
    getter_(tmp_value);
    return tmp_value;
}

void AnalogOutput::setValue(quint8 value)
{
    if (value == value_)
        return;

    int rc = setter_(value);

    if (rc)
        return;

    value_ = value;
    emit valueChanged(value_);
}
