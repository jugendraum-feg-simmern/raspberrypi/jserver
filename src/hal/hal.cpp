#include "inc/hal/hal.h"

#include <QHash>
#include <exception>

namespace HAL
{
    Observer *observer = new Observer();

    namespace
    {
        QHash<QString, DigitalOutputPtr> digital_outputs;
        QHash<QString, DigitalInputPtr> digital_inputs;
        QHash<QString, AnalogOutputPtr> analog_outputs;
        QHash<QString, AnalogInputPtr> analog_inputs;
        QHash<QString, Powersupply*> powersupplies;
    }

    void addDigitalOutput(DigitalOutputPtr& output)
    {
        QString name = output->name();
        if (digital_outputs.contains(name))
            throw std::runtime_error("DigitalOutput " + name.toStdString() + " already exists in HAL. Please check the config files for duplicate entries");

        digital_outputs.insert(name, output);
        emit observer->digitalOutputAdded(name, output);
    }

    void removeDigitalOutput(QString name)
    {
        if (not digital_outputs.contains(name))
            return;

        digital_outputs.remove(name);
        emit observer->digitalOutputRemoved(name);
    }

    DigitalOutputPtr getDigitalOutputByName(QString name)
    {
        if (not digital_outputs.contains(name))
            return nullptr;
        return digital_outputs.value(name);
    }

    void addDigitalInput(DigitalInputPtr& input)
    {
        QString name = input->name();
        if (digital_inputs.contains(name))
            throw std::runtime_error("DigitalInput " + name.toStdString() + " already exists in HAL. Please check the config files for duplicate entries");

        digital_inputs.insert(name, input);
        emit observer->digitalInputAdded(name, input);
    }

    void removeDigitalInput(QString name)
    {
        if (not digital_inputs.contains(name))
            return;

        digital_inputs.remove(name);
        emit observer->digitalInputRemoved(name);
    }

    DigitalInputPtr getDigitalInputByName(QString name)
    {
        if (not digital_inputs.contains(name))
            return nullptr;
        return digital_inputs.value(name);
    }

    void addAnalogOutput(AnalogOutputPtr& output)
    {
        QString name = output->name();
        if (analog_outputs.contains(name))
            throw std::runtime_error("AnalogOutput " + name.toStdString() + " already exists in HAL. Please check the config files for duplicate entries");

        analog_outputs.insert(name, output);
        emit observer->analogOutputAdded(name, output);
    }

    void removeAnalogOutput(QString name)
    {
        if (not analog_outputs.contains(name))
            return;

        analog_outputs.remove(name);
        emit observer->analogOutputRemoved(name);
    }

    AnalogOutputPtr getAnalogOutputByName(QString name)
    {
        if (not analog_outputs.contains(name))
            return nullptr;
        return analog_outputs.value(name);
    }

    void addAnalogInput(AnalogInputPtr& input)
    {
        QString name = input->name();
        if (analog_inputs.contains(name))
            throw std::runtime_error("AnalogInput " + name.toStdString() + " already exists in HAL. Please check the config files for duplicate entries");

        analog_inputs.insert(name, input);
        emit observer->analogInputAdded(name, input);
    }

    void removeAnalogInput(QString name)
    {
        if (not analog_inputs.contains(name))
            return;

        analog_inputs.remove(name);
        emit observer->analogInputRemoved(name);
    }

    AnalogInputPtr getAnalogInputByName(QString name)
    {
        if (not analog_inputs.contains(name))
            return nullptr;
        return analog_inputs.value(name);
    }

    void registerPowersupply(QString name, Powersupply* powersupply)
    {
        if (powersupplies.contains(name))
            throw std::runtime_error("Powersupply '" + name.toStdString() + "' already exists in HAL! Please check the config files.");

        powersupplies.insert(name, powersupply);
        emit observer->powersupplyAdded(name, powersupply);
    }

    void removePowersupply(QString name)
    {
        if (not powersupplies.contains(name))
            return;

        powersupplies.remove(name);
        emit observer->powersupplyRemoved(name);
    }

    Powersupply *getPowersupplyByName(QString name)
    {
        if (not powersupplies.contains(name))
            return nullptr;
        return powersupplies.value(name);
    }

    void dump()
    {
        qDebug() << "#### DIGITAL OUTPUT ####";
        for (auto it : digital_outputs.keys())
            qDebug() << it;
        qDebug() << "#### ANALOG OUTPUT ####";
        for (auto it : analog_outputs.keys())
            qDebug() << it;
        qDebug() << "#### DIGITAL INPUT ####";
        for (auto it : digital_inputs.keys())
            qDebug() << it;
        qDebug() << "#### ANALOG INPUT ####";
        for (auto it : analog_inputs.keys())
            qDebug() << it;
        qDebug() << "#######################";
    }
}
