#include "inc/hal/analog_input.h"


AnalogInput::AnalogInput(QString name)
    : name_(name)
{
}

void AnalogInput::update(double value, bool force_signal)
{
    if ((value == value_) and not force_signal)
        return;

    value_ = value;
    emit valueChanged(value_);
}

double AnalogInput::value()
{
    return value_;
}
