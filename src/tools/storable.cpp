/*!
 * \file storable.cpp
 * \brief Source of Storable class
 */
#include <QFile>
#include "inc/tools/logging.h"

#include "inc/tools/storable.h"

Storable::~Storable()
{
    for (auto m : mutexes_)
        m->unlock();
    mutexes_.clear();
}

void Storable::updateJsonValueInFile(const QJsonValue &value, QString filename, QStringList path)
{
    // pass value to parent when parent is available
    if (parent_ != nullptr)
        return parent_->updateJsonValueInFile(value, filename, path << id_);

    // create a mutex for that file of not present
    if (!mutexes_.contains(filename))
        createMutex(filename);
    mutexes_.value(filename)->lock();

    QFile file(filename);
    if (!file.open(QIODevice::ReadWrite)) {
        qCWarning(file_log) << "cannot open" << filename << "for reading and writing";
        mutexes_.value(filename)->unlock();
        return;
    }
    QJsonDocument doc(QJsonDocument::fromJson(file.readAll()));
    file.resize(0);	// clear contents

    // load json and modify
    QJsonObject root_object = doc.object();
    updateJsonObject(root_object, value, path);
    file.write(QJsonDocument(root_object).toJson());
    file.close();

    mutexes_.value(filename)->unlock();
}

QJsonValue Storable::getJsonValueFromFile(QString filename, QStringList path)
{
    if (parent_ != nullptr)
        return parent_->getJsonValueFromFile(filename, path << id_);

    // create a mutex for that file of none present
    if (!mutexes_.contains(filename))
        createMutex(filename);
    mutexes_.value(filename)->lock();

    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly)) {
        qCWarning(file_log) << "cannot open" << filename << "for reading";
        mutexes_.value(filename)->unlock();
        return QJsonValue(QJsonValue::Undefined);
    }

    QJsonDocument doc(QJsonDocument::fromJson(file.readAll()));
    QJsonValue current = doc.object();
    for (auto key = path.crbegin(); key != path.crend(); ++key)
        current = current[*key];

    mutexes_.value(filename)->unlock();
    return current;
}

void Storable::updateJsonObject(QJsonObject &root_object, const QJsonValue &new_value, QStringList path)
{
    if (path.isEmpty()) {
        if (new_value.isObject())
            root_object = new_value.toObject();
    } else {
        QString key = path.takeLast();
        QJsonValue sub_value = root_object[key];
        if (path.isEmpty()) {
            sub_value = new_value;
        } else {
            QJsonObject sub_object = sub_value.toObject();
            updateJsonObject(sub_object, new_value, path);
            sub_value = sub_object;
        }
        root_object[key] = sub_value;
    }
}

void Storable::createMutex(QString mutex_id)
{
    // remove all unlocked mutexes from list
    if (mutexes_.size() > MAX_MUTEXES) {
        auto it = mutexes_.begin();
        while (it != mutexes_.end()) {
            if (not isLocked(*it))
                it = mutexes_.erase(it);
            else
                ++it;
        }
    }

    // create a new mutex and insert it into the map.
    mutexes_.insert(mutex_id, new QMutex);
}

bool Storable::isLocked(QMutex *mutex)
{
    if (mutex->tryLock(0)) {
        mutex->unlock();
        return false;
    } else
        return true;
}
