#include "inc/tools/logging.h"
#include "inc/config.h"

// tracing
Q_LOGGING_CATEGORY(traceHardware, "trace.hardware", QtDebugMsg)
Q_LOGGING_CATEGORY(traceArtnet, "trace.artnet", QtDebugMsg)
Q_LOGGING_CATEGORY(traceCore, "trace.core", QtDebugMsg)
Q_LOGGING_CATEGORY(traceTools, "trace.tools", QtDebugMsg)
Q_LOGGING_CATEGORY(traceInit, "trace.init", QtDebugMsg)
Q_LOGGING_CATEGORY(traceTemperature, "trace.temperature", QtDebugMsg)
// general
Q_LOGGING_CATEGORY(hardware_log, "hardware", QtInfoMsg)
Q_LOGGING_CATEGORY(file_log, "file", QtInfoMsg)
Q_LOGGING_CATEGORY(temperature_log, "temperature", QtInfoMsg)
Q_LOGGING_CATEGORY(network_log, "network", QtInfoMsg)
Q_LOGGING_CATEGORY(api_log, "api", QtInfoMsg)

void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    QByteArray localMsg = msg.toLocal8Bit();
    switch (type) {
    case QtDebugMsg:
        fprintf(stderr, "[ DBG ] %s\n", localMsg.constData());
        break;
    case QtInfoMsg:
        fprintf(stderr, "[ INF ] %s\n", localMsg.constData());
        log(type, msg);
        break;
    case QtWarningMsg:
        fprintf(stderr, "[ WRN ] %s : %s\n", context.category, localMsg.constData());
        log(type, msg);
        break;
    case QtCriticalMsg:
        fprintf(stderr, "[ CRT ] %s : %s\n", context.category, localMsg.constData());
        log(type, msg);
        break;
    case QtFatalMsg:
        fprintf(stderr, "[ FTL ] %s : %s \n", context.category, localMsg.constData());
        log(type, msg);
        break;
    }
}

void log(QtMsgType type, const QString& msg)
{
    qCDebug(traceTools) << Q_FUNC_INFO;

    QFile logfile(Config::Files::Logfile);
    if (not logfile.open(QIODevice::Append))
        return;

    QTextStream logstream(&logfile);
    logstream << QDateTime::currentDateTime().toString("[ yyyy-MM-dd hh:mm:ss ] ");
    switch (type)
    {
    case QtDebugMsg:    logstream << "[ DEBUG ] "; break;
    case QtInfoMsg:     logstream << "[ INFO ] "; break;
    case QtWarningMsg:  logstream << "[ WARNING ] "; break;
    case QtCriticalMsg: logstream << "[ CRITICAL ] "; break;
    case QtFatalMsg:    logstream << "[ FATAL ] "; break;
    default:            break;
    }

    logstream << msg << endl;

    // finish and clean up
    logstream.flush();
    logfile.close();
}
