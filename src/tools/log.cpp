/*!
 * \file log.cpp
 * \brief Source der log() function
 */
#include "inc/tools/log.h"

void log(QString file, QString msg)
{
    QFile logfile(file);
    if (logfile.open(QIODevice::Append))
    {
        QTextStream logstream(&logfile);
        logstream << QDateTime::currentDateTime().toString("[ yyyy-MM-dd hh:mm:ss ] ");
        logstream << msg << endl;
    }
}
