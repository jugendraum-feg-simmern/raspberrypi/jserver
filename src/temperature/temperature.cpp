﻿#include <QHash>
#include <QList>
#include <QObject>

#include "inc/temperature/temperature.h"

#include "inc/hardware/hardware.h"
#include "inc/config.h"
#include "inc/tools/storable.h"

namespace Temperature
{
    namespace {
        Storable temperature_file_manager_;
    }

    JTemperatureController *powersupply = new JTemperatureController("powersupply", &temperature_file_manager_, Config::getElementByKey(Config::Category::Temperature, "powersupply"));
    JTemperatureController *cabin = new JTemperatureController("cabin", &temperature_file_manager_, Config::getElementByKey(Config::Category::Temperature, "cabin"));
    JTemperatureController *onkyo = new JTemperatureController("onkyo", &temperature_file_manager_, Config::getElementByKey(Config::Category::Temperature, "onkyo"));
    JTemperatureController *pc = new JTemperatureController("pc", &temperature_file_manager_, Config::getElementByKey(Config::Category::Temperature, "pc"));
    JTemperatureController *pi = new JTemperatureController("pi", &temperature_file_manager_, Config::getElementByKey(Config::Category::Temperature, "pi"));

    QList<JTemperatureController *> all_controllers = {powersupply, cabin, onkyo, pc, pi};
    QHash<TemperatureControlled*, QSet<JTemperatureController*>> element_controller_map;

    void registerToController(JTemperatureController *controller, TemperatureControlled *element)
    {
        // operator[] inserts a default-construted entry to the list with the specified key and returns it.
        element_controller_map[element].insert(controller);
        // This is kinda ugly, but it allows us to not inherit from QObject in TemperatureControlled, as this would interfere with the inheritance of the remote objects
        QObject::connect(controller, &JTemperatureController::domainChanged, [=](JTemperatureController::Domain domain) { element->onTemperatureDomainChanged(domain); });
    }

    void unregisterFromController(JTemperatureController *controller, TemperatureControlled *element)
    {
        if (element_controller_map.contains(element))
            element_controller_map[element].remove(controller);
    }

    void exit()
    {
        for (auto c : qAsConst(all_controllers))
            delete c;
    }

}
