/*!
 * \file j_temperature_controller.cpp
 * \brief source of JTemperatureController class
 */

#include "inc/temperature/j_temperature_controller.h"

#include <QDebug>
#include <QtMath>
#include <QFileInfo>
#include <QtConcurrent>
#include "inc/hardware/hardware.h"
#include "inc/hal/hal.h"
#include "inc/tools/logging.h"
#include "inc/config.h"
#include <exception>
#include <limits.h>

JTemperatureController::JTemperatureController(QString id, Storable *parent, QJsonValue config)
    : Storable(id, parent)
{
    qCDebug(traceInit) << "constructing temperature controller" << id << "from" << config;

    if (config.isUndefined())
        throw std::runtime_error("Missing config for " + id.toStdString());

    QJsonValue c_temperature_sensor = config["temperature_sensor"];
    QJsonValue c_fan = config["fan"];

    // get temperature sensor
    if (c_temperature_sensor.isUndefined())
        has_sensor_ = false;
    else {
        has_sensor_ = true;
        analog_input_name_ = c_temperature_sensor.toString();
        AnalogInputPtr ain = HAL::getAnalogInputByName(analog_input_name_);
        if (ain.get() == nullptr)
            connect(HAL::observer, &Observer::analogInputAdded, this, &JTemperatureController::onAnalogInputAdded);
        else {
            connect(HAL::observer, &Observer::analogInputRemoved, this, &JTemperatureController::onAnalogInputRemoved);
            connect(ain.get(), &AnalogInput::valueChanged, this, &JTemperatureController::onAnalogInputValueChanged);
        }
    }

    // get analog output
    if (c_fan.isUndefined())
        has_fan_ = false;
    else {
        has_fan_ = true;
        analog_output_name_ = c_fan.toString();
        AnalogOutputPtr aout = HAL::getAnalogOutputByName(analog_output_name_);
        if (aout.get() == nullptr)
            connect(HAL::observer, &Observer::analogOutputAdded, this, &JTemperatureController::onAnalogOutputAdded);
        else {
            connect(HAL::observer, &Observer::analogOutputRemoved, this, &JTemperatureController::onAnalogOutputRemoved);
            connect(aout.get(), &AnalogOutput::valueChanged, this, &JTemperatureController::onAnalogOutputValueChanged);
            connect(this, &JTemperatureController::requestFanSpeedChange, aout.get(), &AnalogOutput::setValue);
        }
    }

    // read settings. When none present, create from defaults
    QJsonValue settings = getJsonValueFromFile(Config::Files::TemperatureSettings);
    if (not settings.isUndefined())
        deserialize(settings.toObject());
    else {
        QJsonObject obj;
        serialize(obj);
        updateJsonValueInFile(obj, Config::Files::TemperatureSettings);
    }
}

JTemperatureController::~JTemperatureController()
{
}

bool JTemperatureController::constrain(uint8_t &value)
{
    if (domain_ == High) {
        value = std::min(value, uint8_t(LIMIT_UINT8_T));
        return true;
    }
    if (domain_ == Critical) {
        value = 0;
        return true;
    }
    return false;
}

bool JTemperatureController::constrain(bool &value)
{
    if (domain_ == Critical) {
        value = false;
        return true;
    }
    return false;
}

bool JTemperatureController::constrain(QColor &color)
{
    if (domain_ == High) {
        color.setHsv(color.hue(), color.saturation(), std::min(color.value(), int(LIMIT_UINT8_T)));
        return true;
    }
    if (domain_ == Critical) {
        color = Qt::GlobalColor::black;
        return true;
    }
    return false;
}

void JTemperatureController::serialize(QJsonObject &json)
{
    if (has_fan_) {
        QJsonObject fan_json;
        fan_json["Minimum"] = fan_min_;
        json["Fan"] = fan_json;
    }
    if (has_sensor_) {
        QJsonObject temp_json;
        temp_json["FanStart"] = temp_fan_start_;
        temp_json["FanMax"] = temp_fan_max_;
        temp_json["High"] = temp_high_;
        temp_json["Critical"] = temp_crit_;
        temp_json["Hysteresis"] = temp_hysteresis_;
        temp_json["NoiseThreshold"] = temp_noise_threshold_;
        json["Temperature"] = temp_json;
    }
}

void JTemperatureController::deserialize(const QJsonObject &json)
{
    if (has_fan_) {
        if (json.contains("Fan") and json["Fan"].isObject()) {
            QJsonObject fan_json = json["Fan"].toObject();
            fan_min_ = fan_json["Minimum"].toInt(FAN_MIN_DEFAULT);
        } else {
            fan_min_ = FAN_MIN_DEFAULT;
        }
    }

    if (has_sensor_) {
        if (json.contains("Temperature") and json["Temperature"].isObject()) {
            QJsonObject temp_json = json["Temperature"].toObject();
            temp_fan_start_ = temp_json["FanStart"].toDouble(TEMP_FAN_START_DEFAULT);
            temp_fan_max_ = temp_json["FanMax"].toDouble(TEMP_FAN_MAX_DEFAULT);
            temp_high_ = temp_json["High"].toDouble(TEMP_HIGH_DEFAULT);
            temp_crit_ = temp_json["Critical"].toDouble(TEMP_CRIT_DEFAULT);
            temp_hysteresis_ = temp_json["Hysteresis"].toDouble(TEMP_HYSTERESIS_DEFAULT);
            temp_noise_threshold_ = temp_json["NoiseThreshold"].toDouble(TEMP_NOISE_THRESHOLD_DEFAULT);
        } else {
            temp_fan_start_ = TEMP_FAN_START_DEFAULT;
            temp_fan_max_ = TEMP_FAN_MAX_DEFAULT;
            temp_high_ = TEMP_HIGH_DEFAULT;
            temp_crit_ = TEMP_CRIT_DEFAULT;
            temp_hysteresis_ = TEMP_HYSTERESIS_DEFAULT;
            temp_noise_threshold_ = TEMP_NOISE_THRESHOLD_DEFAULT;
        }
    }

}

void JTemperatureController::onAnalogInputAdded(QString name, AnalogInputPtr ain)
{
    if (name != analog_input_name_)
        return;

    disconnect(HAL::observer, &Observer::analogInputAdded, this, &JTemperatureController::onAnalogInputAdded);
    connect(HAL::observer, &Observer::analogInputRemoved, this, &JTemperatureController::onAnalogInputRemoved);

    connect(ain.get(), &AnalogInput::valueChanged, this, &JTemperatureController::onAnalogInputValueChanged);
}

void JTemperatureController::onAnalogInputRemoved(QString name)
{
    if (name != analog_input_name_)
        return;

    disconnect(HAL::observer, &Observer::analogInputRemoved, this, &JTemperatureController::onAnalogInputRemoved);
    connect(HAL::observer, &Observer::analogInputAdded, this, &JTemperatureController::onAnalogInputAdded);
}

void JTemperatureController::onAnalogOutputAdded(QString name, AnalogOutputPtr aout)
{
    if (name != analog_output_name_)
        return;

    disconnect(HAL::observer, &Observer::analogOutputAdded, this, &JTemperatureController::onAnalogOutputAdded);
    connect(HAL::observer, &Observer::analogOutputRemoved, this, &JTemperatureController::onAnalogOutputRemoved);

    connect(aout.get(), &AnalogOutput::valueChanged, this, &JTemperatureController::onAnalogOutputValueChanged);
    connect(this, &JTemperatureController::requestFanSpeedChange, aout.get(), &AnalogOutput::setValue);
}

void JTemperatureController::onAnalogOutputRemoved(QString name)
{
    if (name != analog_output_name_)
        return;

    disconnect(HAL::observer, &Observer::analogOutputRemoved, this, &JTemperatureController::onAnalogOutputRemoved);
    connect(HAL::observer, &Observer::analogOutputAdded, this, &JTemperatureController::onAnalogOutputAdded);
}

void JTemperatureController::onAnalogOutputValueChanged(quint8 value)
{
    if (value == fan_speed_)
        return;

    fan_speed_ = value;
    emit fanSpeedChanged(fan_speed_);
}

void JTemperatureController::onAnalogInputValueChanged(double value)
{
    if (value == temperature_)
        return;

    // only act on relevant changes
    if (fabs(value - temperature_) < temp_noise_threshold_)
        return;

    // determine domain
    if (value <= temp_high_ - temp_hysteresis_)
        setDomain(Domain::Normal);
    else if ((value > temp_high_ - temp_hysteresis_) and (value < temp_high_ + temp_hysteresis_))
        setDomain((domain_ == Domain::Normal) ? Domain::Normal : Domain::High);
    else if ((value >= temp_high_ + temp_hysteresis_) and (value < temp_crit_ - temp_hysteresis_))
        setDomain(Domain::High);
    else if ((value > temp_crit_ - temp_hysteresis_) and (value < temp_crit_ + temp_hysteresis_))
        setDomain((domain_ == Domain::High) ? Domain::High : Domain::Critical);
    else
        setDomain(Domain::Critical);

    if (has_fan_) {
        // adjust fan speed according to new temperature
        if (value < temp_fan_start_ - temp_hysteresis_)
            setFanSpeed(0);
        else if ((value >= temp_fan_start_ - temp_hysteresis_) && (value <= temp_fan_start_ + temp_hysteresis_))
            setFanSpeed((fan_speed_ != 0) ? fan_min_ : 0);
        else if ((value > temp_fan_start_ + temp_hysteresis_) && (value < temp_fan_max_))
            setFanSpeed(int((255 - fan_min_)/qPow((temp_fan_max_ - temp_fan_start_), 2) * qPow((value - temp_fan_start_), 2) + fan_min_));
        else if (value >= temp_fan_max_)
            setFanSpeed(255);
    }

    temperature_ = value;
    emit temperatureChanged(temperature_);
}

void JTemperatureController::setFanSpeed(quint8 fan_speed)
{
    if (fan_speed == fan_speed_)
        return;

    fan_speed_ = fan_speed;
    emit requestFanSpeedChange(fan_speed_);
}

void JTemperatureController::setDomain(JTemperatureController::Domain domain)
{
    if (domain == domain_)
        return;

    domain_ = domain;
    emit domainChanged(domain_);
}

void JTemperatureController::setTemperatureFanStart(double temp_fan_start)
{
    if (temp_fan_start == temp_fan_start_)
        return;

    temp_fan_start_ = temp_fan_start;
    updateJsonValueInFile(temp_fan_start_, Config::Files::TemperatureSettings, {"FanStart", "Temperature"});
    emit temperatureFanStartChanged(temp_fan_start_);
}

void JTemperatureController::setTemperatureFanMax(double temp_fan_max)
{
    if (temp_fan_max == temp_fan_max_)
        return;

    temp_fan_max_ = temp_fan_max;
    updateJsonValueInFile(temp_fan_max_, Config::Files::TemperatureSettings, {"FanMax", "Temperature"});
    emit temperatureFanMaxChanged(temp_fan_max_);
}

void JTemperatureController::setTemperatureHigh(double temp_high)
{
    if (temp_high == temp_high_)
        return;

    temp_high_ = temp_high;
    updateJsonValueInFile(temp_high_, Config::Files::TemperatureSettings, {"High", "Temperature"});
    emit temperatureHighChanged(temp_high_);
}

void JTemperatureController::setTemperatureCritical(double temp_crit)
{
    if (temp_crit == temp_crit_)
        return;

    temp_crit_ = temp_crit;
    updateJsonValueInFile(temp_crit_, Config::Files::TemperatureSettings, {"Critical", "Temperature"});
    emit temperatureCriticalChanged(temp_crit_);
}

void JTemperatureController::setTemperatureHysteresis(double temp_hysteresis)
{
    if (temp_hysteresis == temp_hysteresis_)
        return;

    temp_hysteresis_ = temp_hysteresis;
    updateJsonValueInFile(temp_hysteresis_, Config::Files::TemperatureSettings, {"Hysteresis", "Temperature"});
    emit temperatureHysteresisChanged(temp_hysteresis_);
}

void JTemperatureController::setTemperatureNoiseThreshold(double temp_noise_threshold)
{
    if (temp_noise_threshold == temp_noise_threshold_)
        return;

    temp_noise_threshold_ = temp_noise_threshold;
    updateJsonValueInFile(temp_noise_threshold_, Config::Files::TemperatureSettings, {"NoiseThreshold", "Temperature"});
    emit temperatureHysteresisChanged(temp_hysteresis_);
}

void JTemperatureController::setFanSpeedMinimum(uint8_t min)
{
    if (min == fan_min_)
        return;

    fan_min_ = min;
    updateJsonValueInFile(fan_min_, Config::Files::TemperatureSettings, {"Minumum", "Fan"});
    emit fanSpeedMinimumChanged(fan_min_);
}
