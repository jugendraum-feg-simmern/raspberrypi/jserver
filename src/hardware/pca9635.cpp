#include "inc/hardware/pca9635.h"
#include "inc/hardware/pca9635_register.h"

#include <exception>
#include <QJsonArray>

#include "inc/hal/hal.h"

#include "inc/tools/errno.h"
#include "inc/tools/logging.h"

// helper to pre-process config for initializaion of super class
static uint8_t addressFromConfig(QString name, QJsonValue config)
{
    QJsonValue c_i2c_address = config["i2c_address"];
    if (not c_i2c_address.isDouble())
        throw std::runtime_error("Invalid config for " + name.toStdString() + ".i2c_address");
    return c_i2c_address.toInt();
}

PCA9635::PCA9635(QString name, QJsonValue config)
    : IODevice(),
      I2CDevice(addressFromConfig(name, config)),
      name_(name)
{
    qCDebug(traceInit) << "constructing" << name << "from" << config;

    QJsonValue c_analog_outputs = config["analog_outputs"];
    if (not c_analog_outputs.isArray())
        throw std::runtime_error("Invalid config for " + name.toStdString() + ".analog_outputs");
    QJsonArray analog_outputs = c_analog_outputs.toArray();
    for (auto it : analog_outputs)
        outputs_.insert(analogOutputFromConfig(it.toObject()));
}

int PCA9635::init(bool mocking, bool skip_init)
{
    qCDebug(traceHardware) << Q_FUNC_INFO << "with mocking =" << mocking;

    auto handler = [=]() -> int {
        // mock I2CDevice if requested
        setMocking(mocking);

        // optain a file descriptor
        if (openI2CDevice() != 0)
            return -1;

        if (!skip_init){
            // since writeReg8 returns only 0 or -1, it is safe to sum up the return values
            int ok = writeReg8(PCA9635_MODE1, 0x00);
            ok += writeReg8(PCA9635_MODE2, 0x1d);
            // enable individual control for each channel
            ok += writeReg8(PCA9635_GRPPWM, 0x00);
            ok += writeReg8(PCA9635_LEDOUT0, 0xaa);
            ok += writeReg8(PCA9635_LEDOUT1, 0xaa);
            ok += writeReg8(PCA9635_LEDOUT2, 0xaa);
            ok += writeReg8(PCA9635_LEDOUT3, 0xaa);

            // write 0 into all registers
            for (int i = 0; i < 16; i++)
                ok += writeReg8(i + 2, 0x00);

            if (ok != 0)
                return -1;
        }

        return 0;
    };

    int rc = ioInit(handler);
    if (rc == OK)
        for (auto aout : qAsConst(outputs_))
            HAL::addAnalogOutput(aout);
    return rc;
}

int PCA9635::getRegisterValue(uint8_t reg, uint8_t& val)
{
    qCDebug(traceHardware()) << Q_FUNC_INFO;
    auto handler = [=, &val]() -> int {
        // That looks kinda ugly... Is there a nicer way to convert between int and uint8_t?
        uint8_t val8;
        int rc = readReg8(reg, val8);
        val = val8;
        return rc;
    };

    return ioRead(handler);
}

int PCA9635::setRegisterValue(uint8_t reg, uint8_t val)
{
    qCDebug(traceHardware()) << Q_FUNC_INFO;

    auto handler = [=]() -> int {
        return writeReg8(reg, val);
    };

    return ioWrite(handler);
}

AnalogOutputPtr PCA9635::analogOutputFromConfig(QJsonObject config)
{
    QString name = config.keys().at(0);
    QJsonValue c_reg = config[name];
    if (c_reg.isUndefined() or not c_reg.isDouble())
        throw std::runtime_error("Invalid config for " + name_.toStdString() + "." + name.toStdString());
    uint8_t reg = config[name].toInt();

    QString hal_name = QString("%1.%2").arg(name_, name);

    auto setter = [=](uint8_t val) -> int {
        return this->setRegisterValue(reg, val);
    };

    auto getter = [=](uint8_t& val) -> int {
        return this->getRegisterValue(reg, val);
    };

    return AnalogOutputPtr(new AnalogOutput(hal_name, setter, getter));
}

