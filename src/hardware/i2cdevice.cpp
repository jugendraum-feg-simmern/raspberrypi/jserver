#include "inc/hardware/i2cdevice.h"

#include <QDebug>

I2CDevice::I2CDevice(uint8_t addr)
{
    if (addr > 0x7f)
        throw std::runtime_error("I2C addresses must not have MSB set");
    else
        addr_ = addr;
}

I2CDevice::~I2CDevice()
{
    close(fd_);
}

void I2CDevice::setMocking(bool mocking)
{
    mocked_ = mocking;
    for (uint8_t r : mock_regs_)
        r = 0;
}

int I2CDevice::openI2CDevice()
{
    if (mocked_)
        return 0;

    if (fd_ > 0)
        return 0;

    if ((fd_ = open("/dev/i2c-1", O_RDWR)) < 0)
        return errno;

    if ((ioctl(fd_, I2C_SLAVE, addr_)) < 0) {
        int e = errno;
        close(fd_); fd_ = -1;
        return e;
    }

    return 0;
}

int I2CDevice::checkI2CDevice()
{
    if (mocked_)
        return 0;

    int ok = fcntl(fd_, F_GETFL);
    return (ok == 0) ? 0 : errno;
}

int I2CDevice::closeI2CDevice()
{
    if (mocked_)
        return 0;

    qCDebug(traceHardware) << Q_FUNC_INFO;

    int ok = close(fd_);
    fd_ = -1;
    return (ok == 0) ? 0 : errno;
}

int I2CDevice::readReg8(uint8_t reg, uint8_t& data)
{
    if (mocked_) {
        data = mock_regs_[reg];
        return 0;
    }

    ioctl_smbus_args args;
    args.rw = I2C_SMBUS_READ;
    args.command = reg;
    args.size = I2C_SMBUS_BYTE_DATA;
    args.data = &data;

    if (ioctl(fd_, I2C_SMBUS, &args) != 0){
        last_error_ = errno;
        return -1;
    }
    return 0;
}

int I2CDevice::writeReg8(uint8_t reg, uint8_t data)
{
    if (mocked_){
        mock_regs_[reg] = data;
        return 0;
    }

    ioctl_smbus_args args;
    args.rw = I2C_SMBUS_WRITE;
    args.command = reg;
    args.size = I2C_SMBUS_BYTE_DATA;
    args.data = &data;

    if (ioctl(fd_, I2C_SMBUS, &args) != 0){
        last_error_ = errno;
        return -1;
    }
    return 0;
}
