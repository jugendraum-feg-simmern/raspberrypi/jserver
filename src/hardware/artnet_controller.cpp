/*!
 * \file artnet_controller.cpp
 * \brief source of the ArtNetController class
 */
#include "inc/hardware/artnet_controller.h"

#include <QNetworkInterface>
#include <QJsonArray>
#include <QJsonObject>
#include <exception>

#include "inc/tools/logging.h"

ArtNetController::ArtNetController(QJsonValue config)
{
    qCDebug(traceInit) << "constructing ArtNet controller from" << config;

    if (config.isUndefined())
        throw std::runtime_error("missing config for artnet_controller");

    QJsonValue c_port = config["port"];
    QJsonValue c_poll_interval_ms = config["poll_interval_ms"];
    QJsonValue c_max_node_timeout_events = config["max_node_timeout_events"];

    if (c_port.isUndefined())
        throw std::runtime_error("missing config for artnet_controller.port");
    if (c_poll_interval_ms.isUndefined())
        throw std::runtime_error("missing config for artnet_controller.poll_interval_ms");
    if (c_max_node_timeout_events.isUndefined())
        throw std::runtime_error("missing config for artnet_controller.max_node_timeout_events");

    if (not c_port.isDouble())
        throw std::runtime_error("invalid config for artnet_controller.port");
    if (not c_poll_interval_ms.isDouble())
        throw std::runtime_error("invalid config for artnet_controller.poll_interval_ms");
    if (not c_max_node_timeout_events.isDouble())
        throw std::runtime_error("invalid config for artnet_controller.max_node_timeout_events");

    port_ = c_port.toInt();
    poll_interval_ms_ = c_poll_interval_ms.toInt();
    max_node_timeout_events_ = c_max_node_timeout_events.toInt();

    local_address_ = getLocalAddress();
    broadcast_address_ = getBroadcastAddress();
}

void ArtNetController::init(bool mocked)
{
    qCDebug(traceArtnet) << Q_FUNC_INFO;
    mocked_ = mocked;
    if (mocked_)
        return;

    // set up socket
    socket_ = new QUdpSocket(this);
    socket_->bind(port_);
    connect(socket_, &QUdpSocket::readyRead, this, &ArtNetController::onReadyRead);

    // setup periodic polling
    poll_timer_ = new QTimer(this);
    poll_timer_->setInterval(poll_interval_ms_);
    connect(poll_timer_, &QTimer::timeout, this, &ArtNetController::processPollReplyQueue);
    connect(poll_timer_, &QTimer::timeout, this, &ArtNetController::poll);
    poll_timer_->start();
    poll();
}

static QJsonValue combineConfigs(QJsonValue base, QJsonValue extra)
{
    // TODO: harden against different config types
    QJsonObject base_obj = base.toObject();
    QJsonObject extra_obj = extra.toObject();

    for (auto it = extra_obj.constBegin(); it != extra_obj.constEnd(); it++)
        base_obj[it.key()] = it.value();

    return base_obj;
}

void ArtNetController::addNode(QHostAddress ip, uint16_t port, uint16_t oem)
{
    ip = QHostAddress(ip.toIPv4Address());
    qCDebug(traceArtnet) << Q_FUNC_INFO;
    if (nodes_.contains({ip, port}))
        return;

    qDebug().nospace() << "Adding node with OEM 0x" << hex << oem << dec << " @ " << ip.toString() << ":" << port;

    artnetnode_ptr_t node;
    QJsonValue node_config;
    QString node_name = nodeNameFromConfig(Config::getElementByKey(Config::Category::Hardware, {"nodes", "artnet_controller"}), ip);
    QJsonValue extra_config(Config::getElementByKey(Config::Category::Hardware, {node_name, "nodes", "artnet_controller"}));
    switch (oem) {
    case (ARTNET_OEM_ULTIM8):
        node_config = Config::getElementByKey(Config::Category::Hardware, {"ultim8_config", "artnet_controller"});
        node = artnetnode_ptr_t(new Ultim8(node_name, ip, port, combineConfigs(node_config, extra_config)));
        break;
    default:
        node_config = Config::getElementByKey(Config::Category::Hardware, {"default_node_config", "artnet_controller"});
        node = artnetnode_ptr_t(new ArtNetNode(node_name, ip, port, node_config));
    }
    if (!mocked_)
        connect(node.get(), &ArtNetNode::datagramReady, this, &ArtNetController::onNodeDatagramReady);

    node_identifier_t id({ip, port});
    nodes_.insert(id, node);
}

void ArtNetController::removeNode(node_identifier_t id)
{
    qCDebug(traceArtnet) << Q_FUNC_INFO;
    if (nodes_.contains(id))
        nodes_.remove(id);
}

artnetnode_ptr_t ArtNetController::getNode(node_identifier_t id)
{
    if (!nodes_.contains(id)) {
        if (!mocked_)
            return nullptr;
        else
            // when mocked, create the node
            addNode(id.first, id.second, ARTNET_OEM_ULTIM8);
    }

    return nodes_[id];
};

void ArtNetController::onReadyRead()
{
    while (socket_->hasPendingDatagrams()) {
        QNetworkDatagram datagram = socket_->receiveDatagram();
        handleDatagram(datagram);
    }
}

void ArtNetController::onNodeDatagramReady(QNetworkDatagram datagram)
{
    socket_->writeDatagram(datagram);
}

void ArtNetController::poll()
{
    artpoll_packet_t* packet = new artpoll_packet_t;
    packet->opcode = ARTNET_OP_POLL;
    QByteArray data = QByteArray::fromRawData(reinterpret_cast<char*>(packet), sizeof(artpoll_packet_t));
    socket_->writeDatagram(data, broadcast_address_, ARTNET_DEFAULT_PORT);
}

void ArtNetController::processPollReplyQueue()
{
    // check which nodes have responded; remove theit reply from the list of replies
    auto node_it = nodes_.keyBegin();
    while ((node_it != nodes_.keyEnd()) && !poll_reply_queue_.isEmpty()) {
        if (poll_reply_queue_.remove(*node_it) == 0) {
            nodes_[*node_it]->increaseTimeoutEvents();
        } else {
            nodes_[*node_it]->resetTimeoutEvents();
        }
        node_it++;
    }

    // remove nodes, that have not responded too often
    for (auto it = nodes_.begin(); it != nodes_.end();) {
        if (it.value()->timeoutEvents() >= max_node_timeout_events_)
            it = nodes_.erase(it);
        else
            ++it;
    }

    // handle remaining replies
    bool external_controller = false;
    auto it = poll_reply_queue_.begin();
    while (it != poll_reply_queue_.end()) {
        artpollreply_packet_t* packet = it.value();

        switch (packet->style) {
            case ARTNET_ST_NODE:
                addNode(it.key().first, it.key().second, uint16_t(packet->oem_H) << 8 | packet->oem_L);
                break;
            case ARTNET_ST_CONTROLLER:
                qDebug() << "encountered another artnet controller @" << it.key().first.toString() << ":" << it.key().second;
                external_controller = true;
                break;
            default:
                qDebug().nospace() << "Unhandled Style encountered: 0x " << hex << packet->opcode;
            }
        it++;
    }
    poll_reply_queue_.clear();

    // tell nodes wether an external controller has been found
    for (auto node : nodes_)
        node->hasExternalController(external_controller);
}

void ArtNetController::handleDatagram(QNetworkDatagram &datagram)
{
    art_packet_t* packet = reinterpret_cast<art_packet_t*>(datagram.data().data());

    if (strcmp(packet->id, ARTNET_ID))
        return;

    switch (packet->opcode) {
        case ARTNET_OP_POLL:
            handleOpPoll(datagram);
            break;
        case ARTNET_OP_POLLREPLY:
            handleOpPollReply(datagram);
            break;
        case ARTNET_OP_DMX:
            handleOpDMX(datagram);
            break;
        default:
            qCWarning(traceArtnet()).nospace() << "Unhandled opcode 0x" << hex << packet->opcode;
    }
}

void ArtNetController::handleOpPoll(QNetworkDatagram &datagram)
{
    // build reply
    artpollreply_packet_t* packet = new artpollreply_packet_t;
    uint32_t ip = socket_->localAddress().toIPv4Address();
    packet->opcode = ARTNET_OP_POLLREPLY;
    packet->ip[0] = (ip >> 3) & 0xff;
    packet->ip[1] = (ip >> 2) & 0xff;
    packet->ip[2] = (ip >> 1) & 0xff;
    packet->ip[3] = (ip >> 0) & 0xff;
    packet->port = socket_->localPort();
    packet->style = ARTNET_ST_CONTROLLER;

    // send reply back to sender
    QByteArray data = QByteArray::fromRawData(reinterpret_cast<char*>(packet), sizeof(artpollreply_packet_t));
    QNetworkDatagram reply(data, datagram.senderAddress(), datagram.senderPort());
    socket_->writeDatagram(reply);
}

void ArtNetController::handleOpPollReply(QNetworkDatagram &datagram)
{
    // ignore own OpPolls
    if (datagram.senderAddress().toIPv4Address() == local_address_.toIPv4Address())
        return;

    // we must make a copy of the contents of datagram to assure it does not get overriden by the next packet
    artpollreply_packet_t* packet = new artpollreply_packet_t;
    std::copy(datagram.data().data_ptr()->begin(),
              datagram.data().data_ptr()->end(),
              reinterpret_cast<char*>(packet));

    if (packet->bind_index != 1)
        return;

    poll_reply_queue_.insert({QHostAddress(datagram.senderAddress().toIPv4Address()), datagram.senderPort()}, packet);
}

void ArtNetController::handleOpDMX(QNetworkDatagram &datagram)
{
    artdmx_packet_t* packet = reinterpret_cast<artdmx_packet_t*>(datagram.data().data());
    artnet_packet_dump(packet);
}

QHostAddress ArtNetController::getLocalAddress()
{
    for (auto iface : QNetworkInterface::allInterfaces()) {
        if (iface.name() == "eth0") {
            for (auto entry: iface.addressEntries()){
                if (entry.ip().protocol() == QAbstractSocket::IPv4Protocol) {
                    return entry.ip();
                }
            }
        }
    }
    return QHostAddress::Null;
}

QHostAddress ArtNetController::getBroadcastAddress()
{
    for (auto iface : QNetworkInterface::allInterfaces()) {
        if (iface.name() == "eth0") {
            for (auto entry: iface.addressEntries()){
                if (entry.ip().protocol() == QAbstractSocket::IPv4Protocol) {
                    return entry.broadcast();
                }
            }
        }
    }
    return QHostAddress::Null;
}

QString ArtNetController::nodeNameFromConfig(QJsonValue config, QHostAddress ip)
{
    QJsonArray nodes = config.toArray();
    for (auto node : nodes ) {
        QJsonObject n = node.toObject();
        if (n["host"] == ip.toString()) {
            qDebug() << "found node with ip" << ip;
            return n.keys().at(0);
        }
    }

    return QString("artnet_node_%1").arg(nodes_.count());
}
