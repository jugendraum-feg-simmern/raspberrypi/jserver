/*!
 * \file temperature_sensor_udp.cpp
 * \brief Source of the TemperatureSensorUDP class
 */
#include <QRandomGenerator>
#include <QtNetwork>
#include <QDataStream>
#include <exception>

#include "inc/tools/logging.h"
#include "inc/tools/errno.h"
#include "inc/hardware/temperature_sensor_udp.h"
#include "inc/hal/hal.h"


TemperatureSensorUDP::TemperatureSensorUDP(QString name, QJsonValue config)
    : name_(name)
{
    qCDebug(traceInit) << "constructing" << name << "from" << config;

    if (config.isUndefined())
        throw std::runtime_error("missing config for temperature_udp");

    QJsonValue c_host = config["host"];
    QJsonValue c_port = config["port"];
    QJsonValue c_timeout_ms = config["timeout_ms"];

    if (c_host.isUndefined())
        throw std::runtime_error("missing config for temperature_udp.host");
    if (c_port.isUndefined())
        throw std::runtime_error("missing config for temperature_udp.port");
    if (c_timeout_ms.isUndefined())
        throw std::runtime_error("missing config for temperature_udp.timeout_ms");

    if (not c_host.isString())
        throw std::runtime_error("invalid config for temperature_udp.host");
    if (not c_port.isDouble())
        throw std::runtime_error("invalid config for temperature_udp.port");
    if (not c_timeout_ms.isDouble())
        throw std::runtime_error("invalid config for temperature_udp.timeout_ms");

    udp_socket_ = new QUdpSocket();
    host_ = QHostAddress(c_host.toString());
    port_ = c_port.toInt();
    timeout_ms_ = c_timeout_ms.toInt();

    QJsonValue c_analog_inputs = config["analog_inputs"];
    if ((c_analog_inputs.isUndefined()) or (not c_analog_inputs.isArray()))
        throw std::runtime_error("Invalid config for " + name_.toStdString() + ".analog_inputs");
    QJsonArray analog_inputs = c_analog_inputs.toArray();
    for (auto it : analog_inputs)
        analogInputFromConfig(it.toObject());

    update_timer_ = new QTimer(this);
    connect(update_timer_, &QTimer::timeout, this, &TemperatureSensorUDP::update);
}

TemperatureSensorUDP::~TemperatureSensorUDP()
{
    udp_socket_->close();
}

int TemperatureSensorUDP::init(bool mocking, bool skip_init)
{
    qCDebug(traceHardware) << Q_FUNC_INFO;

    mocked_ = mocking;
    auto handler = [=]() -> int {
        if (mocking or skip_init)
            return 0;

        QByteArray buffer;
        QList<uint16_t> tmp_list = {};
        udp_socket_->writeDatagram(QByteArray("0"), host_, port_);
        if (udp_socket_->waitForReadyRead(timeout_ms_)) {
            buffer.resize(udp_socket_->pendingDatagramSize());
            if (udp_socket_->readDatagram(buffer.data(), buffer.size()) == 0)
                return ECONVERSION;
        }
        else
            return ETIMEDOUT;

        // convert QByteArray to QList<uint16_t>
        char* data = buffer.data();
        for (int i = 0; i < buffer.size(); i+=2, data+=2) {
            tmp_list.append((*data << 8) | (*(data + 1)));
        }
        regs_ = tmp_list;
        return OK;
    };

    int rc = ioInit(handler);
    if (rc == OK) {
        for (auto it : inputs_)
            HAL::addAnalogInput(it);
        update_timer_->start(3000);
    }
    return rc;
}

void TemperatureSensorUDP::update()
{
    for (auto it = inputs_.constKeyValueBegin(); it != inputs_.constKeyValueEnd(); ++it) {
        double temp;
        if (readTemperature(it.base().key(), temp) == OK)
            it.base().value()->update(temp);
    }
}

int TemperatureSensorUDP::readTemperature(const int sensor, double &temperature)
{
    qCDebug(traceHardware) << Q_FUNC_INFO;

    auto handler = [=, &temperature]() -> int {
        if (mocked_){
            temperature = QRandomGenerator::global()->generateDouble() * -60 - 100;
            return OK;
        }
        // request content of register by sending its address
        QByteArray datagram;
        QDataStream out(&datagram, QIODevice::ReadWrite);

        if (regs_.size() <= sensor)
            return EOUTOFRANGE;
        out << regs_[sensor];
        udp_socket_->writeDatagram(datagram, host_, port_);

        // read response and convert to float
        if (udp_socket_->waitForReadyRead(timeout_ms_)) {
            QByteArray buffer;
            buffer.resize(udp_socket_->pendingDatagramSize());
            if (udp_socket_->readDatagram(buffer.data(), buffer.size()) != 4)
                return EBADMSG;
            temperature = double(*reinterpret_cast<float*>(buffer.data()));
        } else
            return ETIMEDOUT;
        return OK;
    };

    return ioRead(handler);
}

AnalogInputPtr TemperatureSensorUDP::analogInputFromConfig(QJsonObject config)
{
    QString name = config.keys().at(0);
    int sensor = config[name].toInt();

    QString hal_name = QString("%1.%2").arg(name_, name);
    auto ain = AnalogInputPtr(new AnalogInput(hal_name));

    inputs_.insert(sensor, ain);
    return ain;
}
