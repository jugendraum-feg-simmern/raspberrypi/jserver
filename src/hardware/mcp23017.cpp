#include "inc/hardware/mcp23017.h"
#include "inc/hardware/mcp23017_register.h"

#include <exception>

#include "inc/hal/hal.h"

#include "inc/tools/errno.h"
#include "inc/tools/logging.h"
#include <QJsonArray>

// helper to pre-process config for initializaion of base class
static uint8_t addressFromConfig(QString name, QJsonValue config)
{
    QJsonValue c_i2c_address = config["i2c_address"];
    if (not c_i2c_address.isDouble())
        throw std::runtime_error("Invalid config for " + name.toStdString() + ".i2c_address");
    return c_i2c_address.toInt();
}

MCP23017::MCP23017(QString name, QJsonValue config)
    : IODevice(),
      I2CDevice(addressFromConfig(name, config)),
      name_(name)
{
    qCDebug(traceInit) << "constructing" << name << "from" << config;

    QJsonValue c_digital_outputs = config["digital_outputs"];
    if (not c_digital_outputs.isArray())
        throw std::runtime_error("Invalid config for " + name.toStdString() + ".digital_outputs");
    QJsonArray digital_outputs = c_digital_outputs.toArray();
    for (auto it : digital_outputs)
        outputs_.insert(digitalOutputFromConfig(it.toObject()));

    QJsonValue c_digital_inputs = config["digital_inputs"];
    if (not c_digital_inputs.isArray())
        throw std::runtime_error("Invalid config for " + name.toStdString() + ".digital_inputs");
    QJsonArray digital_inputs = c_digital_inputs.toArray();
    for (auto it : digital_inputs)
        inputs_.insert(digitalInputFromConfig(it.toObject()));
}

int MCP23017::init(bool mocking, bool skip_init)
{
    qCDebug(traceHardware) << Q_FUNC_INFO << "with mocking =" << mocking;

    auto handler = [=]() -> int {
        // mock I2CDevice if requested
        setMocking(mocking);

        // open a file descriptor
        if (openI2CDevice() != 0)
            return -1;

        if (! skip_init){
            // since writeReg8 returns only 0 or -1, it is safe to sum up the return values
            int ret_code_sum = 0;
            ret_code_sum += writeReg8(MCP23017_IODIRA, 0x00);
            ret_code_sum += writeReg8(MCP23017_IODIRB, 0x0f);
            ret_code_sum += writeReg8(MCP23017_GPIOA, 0x00);
            ret_code_sum += writeReg8(MCP23017_GPIOB, 0x00);
            if (ret_code_sum != 0)
                return -1;
        }

        return 0;
    };

    int rc = ioInit(handler);
    if (rc == OK) {
        for (auto dout : outputs_)
            HAL::addDigitalOutput(dout);
        for (auto din : inputs_)
            HAL::addDigitalInput(din);
    }
    return rc;
}

int MCP23017::setPinState(uint8_t pin, uint8_t port, bool state)
{
    qCDebug(traceHardware).nospace() << "MCP23017@" << showbase << hex << address() << ": setState (port: " << port << ", pin:" << pin << ", state:" << state << ")";

    auto handler = [=]() -> int {
        uint8_t reg_value;
        if (readReg8(port, reg_value) != 0)
            return -1;

        if (state)
            reg_value |= pin;
        else
            reg_value &= ~pin;

        return writeReg8(port, reg_value);
    };

    return ioWrite(handler);
}

int MCP23017::getPinState(uint8_t pin, const uint8_t port, bool &state)
{
    auto handler = [=, &state]() -> int {
        uint8_t reg_value;
        if (readReg8(port, reg_value) != 0)
            return -1;
        state = reg_value & pin;
        qCDebug(traceHardware).nospace() << "MCP23017@" << showbase << hex << address() << ": getState(port: " << port << ", pin:" << pin << ") -> " << state;
        return OK;
    };

    return ioRead(handler);
}

DigitalOutputPtr MCP23017::digitalOutputFromConfig(QJsonObject root_node)
{
    QString name = root_node.keys().at(0);

    if (root_node[name].isUndefined() or not root_node[name].isObject())
        throw std::runtime_error("Invalid config for " + name_.toStdString() + "." + name.toStdString());
    QJsonObject cfg = root_node[name].toObject();
    if (cfg["port"].isUndefined() or not cfg["port"].isDouble())
        throw std::runtime_error("Invalid config for " + name_.toStdString() + "." + name.toStdString() + ".port");
    if (cfg["pin"].isUndefined() or not cfg["pin"].isDouble())
        throw std::runtime_error("Invalid config for " + name_.toStdString() + "." + name.toStdString() + ".pin");

    uint8_t port = cfg["port"].toInt();
    uint8_t pin = cfg["pin"].toInt();

    QString hal_name = QString("%1.%2").arg(name_).arg(name);

    auto setter = [=](bool state) -> int {
        return this->setPinState(pin, port, state);
    };

    auto getter = [=](bool& state) -> int {
        return this->getPinState(pin, port, state);
    };

    return DigitalOutputPtr(new DigitalOutput(hal_name, setter, getter));
}

DigitalInputPtr MCP23017::digitalInputFromConfig(QJsonObject root_node)
{
    // TODO: validate config before use
    QString name = root_node.keys().at(0);
    QJsonObject cfg = root_node[name].toObject();
    uint8_t port = cfg["port"].toInt();
    uint8_t pin = cfg["pin"].toInt();

    QString hal_name = QString("%1.%2").arg(name_).arg(name);

    return DigitalInputPtr(new DigitalInput(hal_name));
}


