#include "inc/hardware/hardware.h"

#include <QDebug>
#include "inc/config.h"
#include "inc/tools/logging.h"

#include "inc/temperature/temperature.h"

namespace hw
{
    bool mocked = false;

    TemperatureSensorUDP* temperature_sensor_udp = new TemperatureSensorUDP("temperature_udp", Config::getElementByKey(Config::Category::Hardware, "temperature_udp"));
    SystemMonitor* system_monitor = new SystemMonitor("system_monitor", Config::getElementByKey(Config::Category::Hardware, "system_monitor"));
    ArtNetController* artnet_controller = new ArtNetController(Config::getElementByKey(Config::Category::Hardware, "artnet_controller"));
    WallSwitch* wallswitch = new WallSwitch(Config::getElementByKey(Config::Category::Hardware, "wallswitch"));
    PCA9635* pca_light = new PCA9635("pca_light", Config::getElementByKey(Config::Category::Hardware, "pca_light"));
    PCA9635* pca_fan = new PCA9635("pca_fan", Config::getElementByKey(Config::Category::Hardware, "pca_fan"));
    MCP23017* portexpander = new MCP23017("portexpander", Config::getElementByKey(Config::Category::Hardware, "portexpander"));
    Powersupply *ps_12V = new Powersupply("ps_12V", Config::getElementByKey(Config::Category::Hardware, "ps_12V"));
    Powersupply *ps_48V = new Powersupply("ps_48V", Config::getElementByKey(Config::Category::Hardware, "ps_48V"));
}

void hw::init(bool mocking, bool skip_init)
{
    qCDebug(traceHardware) << Q_FUNC_INFO;
    if (mocking) qCWarning(hardware_log) << "mocking hardware! Is that intentional?";
    if (skip_init) qCWarning(hardware_log) << "skipping intialization of hardware! Is that intentional?";

    mocked = mocking;

    temperature_sensor_udp->init(mocked, skip_init);
    system_monitor->start();
    artnet_controller->init(mocked);
    portexpander->init(mocked, skip_init);
    pca_light->init(mocked, skip_init);
    pca_fan->init(mocked, skip_init);
    wallswitch->init(mocked);

    Temperature::registerToController(Temperature::powersupply, ps_12V);
    Temperature::registerToController(Temperature::powersupply, ps_48V);
}

void hw::exit()
{
    delete ps_48V;
    delete ps_12V;
    delete portexpander;
    delete pca_fan;
    delete pca_light;
    delete wallswitch;
    delete artnet_controller;
    delete temperature_sensor_udp;
}
