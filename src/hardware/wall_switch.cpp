/*!
 * \file wall_switch.cpp
 * \brief source of WallSwitch class
 */
#include "inc/hardware/wall_switch.h"

#include "inc/config.h"
#include <exception>


WallSwitch::WallSwitch(QJsonValue config)
{
    qCDebug(traceInit) << "constructing wallswitch from" << config;

    if (config.isUndefined())
        throw std::runtime_error("missing config for wallswitch");

    QJsonValue c_pin_1 = config["pin_1"];
    QJsonValue c_pin_2 = config["pin_2"];
    QJsonValue c_debounce_time_ms = config["debounce_time_ms"];

    if (c_pin_1.isUndefined())
        throw std::runtime_error("missing config for wallswitch.pin_1");
    if (c_pin_2.isUndefined())
        throw std::runtime_error("missing config for wallswitch.pin_2");
    if (c_debounce_time_ms.isUndefined())
        throw std::runtime_error("missing config for wallswitch.debounce_time_ms");


    if (not c_pin_1.isDouble())
        throw std::runtime_error("invalid config for wallswitch.pin_1");
    if (not c_pin_2.isDouble())
        throw std::runtime_error("invalid config for wallswitch.pin_2");
    if (not c_debounce_time_ms.isDouble())
        throw std::runtime_error("invalid config for wallswitch.debounce_time_ms");

    isr_pin_ = c_pin_1.toInt();
    ctrl_pin_ = c_pin_2.toInt();
}

WallSwitch::~WallSwitch()
{
    watcher_.cancel();
    delete wall_switch_thread_pool_;

    close(isr_pin_fd_);
    close(ctrl_pin_fd_);
}

void WallSwitch::init(bool mocking)
{
    if (mocking)
        return;

    // create private thread pool
    wall_switch_thread_pool_ = new QThreadPool();
    connect(&watcher_, &QFutureWatcher<void>::finished, this, &WallSwitch::onInterrupt);

    // setup the pins
    int isr_setup_ret_val = setupPin(isr_pin_, isr_pin_fd_);
    int ctrl_setup_ret_val = setupPin(ctrl_pin_, ctrl_pin_fd_);

    // start waiting for interrupts
    if (isr_setup_ret_val != 0 or ctrl_setup_ret_val != 0)
        qFatal("failed to configure wall switch pins!");
    watcher_.setFuture(QtConcurrent::run(wall_switch_thread_pool_, this, &WallSwitch::waitForInterrupt, isr_pin_fd_));
}

void WallSwitch::onInterrupt()
{
    qCDebug(traceHardware) << Q_FUNC_INFO;
    qDebug() << Q_FUNC_INFO;

    // restart
    watcher_.setFuture(QtConcurrent::run(wall_switch_thread_pool_, this, &WallSwitch::waitForInterrupt, isr_pin_fd_));

    // debounce
    uint64_t now = QDateTime::currentMSecsSinceEpoch();
    if ((now - last_isr_timestamp_) < debounce_time_ms_)
        return;

    // check the pins for equality
    bool isr_pin_state, ctrl_pin_state;
    int ret_val_isr_pin, ret_val_ctrl_pin;

    // always read both pins, even if first read fails
    if ((ret_val_isr_pin = readPin(isr_pin_fd_, isr_pin_state)) != 0)
        qCWarning(hardware_log) << "error reading isr_pin:" << ret_val_isr_pin;
    if ((ret_val_ctrl_pin = readPin(ctrl_pin_fd_, ctrl_pin_state)) != 0)
        qCWarning(hardware_log) << "error reading ctrl_pin:" << ret_val_ctrl_pin;

    if (ret_val_isr_pin != 0 or ret_val_ctrl_pin != 0)
        return;

    if (isr_pin_state != ctrl_pin_state){
        qCWarning(hardware_log) << "states of ctrl_pin (" << isr_pin_state << ") and isr_pin (" << ctrl_pin_state << ") not equal";
        return;
    }

    last_isr_timestamp_ = now;
    emit stateChanged();
}

int WallSwitch::setupPin(int pin, int& pin_fd)
{
    FILE *fd;
    char fName [128];

    // export the pin
    if ((fd = fopen("/sys/class/gpio/export", "w")) == NULL)
        return errno;
    fprintf(fd, "%d\n", pin);
    fclose(fd);

    // need to wait for export to be completed and the device to become available
    /*
     * A couble of methods have been tried, each testing "/sys/class/gpio/gpio%d/direction".
     * Each was used as exit condition of while-loop with and without using QThread::msleep(100) in between cycles
     *  a) stat() == 0 -> returned true, but the following write failed with errno 13
     *  b) ifstream().good() -> returned true, but the following write failed with errno 13
     *  c) access() != -1 -> returned true, but the following write failed with errno 13
     *  d) QFile().isWriteable() -> endless loop
     *  e) fopen(,"w") == 0 -> endless loop
     * Strangely, a single call to QThread::msleep(50) is enough. But why doesn't this
     * work from within a loop?
     */
    QThread::msleep(100); // even though 50 ms was enough in testing, using 100 ms to be safe

    // set as input
    sprintf(fName, "/sys/class/gpio/gpio%d/direction", pin);
    if ((fd = fopen(fName, "w")) == NULL)
        return errno;
    fprintf(fd, "in\n");
    fclose(fd);

    // configure edge sensibility
    sprintf(fName, "/sys/class/gpio/gpio%d/edge", pin);
    if ((fd = fopen(fName, "w")) == NULL)
        return errno;
    fprintf(fd, "both\n");
    fclose(fd);

    // get a file descriptor for that pin
    sprintf(fName, "/sys/class/gpio/gpio%d/value", pin);
    if ((pin_fd = open(fName, O_RDWR)) < 0)
        return errno;

    return OK;
}

void WallSwitch::waitForInterrupt(int pin_fd)
{
    // clear all pending interrupts
    int count;
    char tmp;
    ioctl(pin_fd, FIONREAD, &count);
    for (int i = 0; i < count; i++)
        read(pin_fd, &tmp, 1);

    // setup args for poll
    struct pollfd fds;
    fds.fd = pin_fd;
    fds.events = POLLPRI | POLLERR;
    nfds_t nsdf = 1;
    int timeout = -1;

    // the actual waiting
    int poll_ret_val = poll(&fds, nsdf, timeout);

    // do a dummy read to clear the interrupt
    if (poll_ret_val > 0) {
        lseek(pin_fd, 0, SEEK_SET); // rewind
        read(pin_fd, &tmp, 1);      // read and clear
    }
    else
        qCWarning(hardware_log) << "poll returned" << poll_ret_val;
}

int WallSwitch::readPin(int pin_fd, bool &state)
{
    uint8_t tmp;
    lseek(pin_fd, 0L, SEEK_SET);
    if (read(pin_fd, &tmp, 1) < 0)
        return errno;
    state = (tmp == 1);
    return OK;

}
