#include "inc/hardware/powersupply.h"
#include "inc/hal/hal.h"
#include "inc/temperature/temperature.h"

#include <QDebug>
#include <QThread>

#include "inc/tools/logging.h"

Powersupply::Powersupply(QString name, QJsonValue config)
    : name_(name)
{
    qCDebug(traceInit) << "constructing" << name << "from" << config;

    if (config.isUndefined())
        throw std::runtime_error("missing config for powersupply");

    QJsonValue c_digital_output = config["digital_output"];
    QJsonValue c_startup_delay_ms = config["startup_delay_ms"];
    QJsonValue c_shutdown_delay_ms = config["shutdown_delay_ms"];

    if (c_digital_output.isUndefined() or not c_digital_output.isString())
        throw std::runtime_error("Invalid config for " + name_.toStdString() + ".digital_output");
    if (c_startup_delay_ms.isUndefined() or (not c_startup_delay_ms.isDouble()))
        throw std::runtime_error("Invalid config for " + name_.toStdString() + ".startup_delay_ms");
    if (c_shutdown_delay_ms.isUndefined() or (not c_shutdown_delay_ms.isDouble()))
        throw std::runtime_error("Invalid config for " + name_.toStdString() + ".shutdown_delay_ms");

    startup_delay_ms_ = c_startup_delay_ms.toInt();
    shutdown_delay_ms_ = c_shutdown_delay_ms.toInt();

    digital_output_name_ = c_digital_output.toString();
    DigitalOutputPtr dout = HAL::getDigitalOutputByName(digital_output_name_);
    if (dout.get() == nullptr)
        connect(HAL::observer, &Observer::digitalOutputAdded, this, &Powersupply::onDigitalOutputAdded);
    else {
        connect(HAL::observer, &Observer::digitalOutputRemoved, this, &Powersupply::onDigitalOutputRemoved);
        connect(dout.get(), &DigitalOutput::stateChanged, this, &Powersupply::onDigitalOutputStateChanged);
        connect(this, &Powersupply::requestDigitalOutputStateChange, dout.get(), &DigitalOutput::setState);
    }


    // create handler to switch off. Retry until successfull or stopped
    auto turn_off_handler = [=](){
        qCDebug(traceHardware).nospace() << "switch off " << name_;
        emit requestDigitalOutputStateChange(false);
    };

    shutdown_timer_ = new QTimer(this);
    shutdown_timer_->setSingleShot(true);
    connect(shutdown_timer_, &QTimer::timeout, turn_off_handler);

    // control timer via signal and slot since QTimer must be controlled from thread
    //      it belongs to
    // connecting the qt5 style does not work here :( because of ambiguity of QTimer::start
    // wrapping timer->start() and timer->stop into lambdas does not resolve thread dependency
    connect(this, SIGNAL(startShutdownTimer(int)), shutdown_timer_, SLOT(start(int)));
    connect(this, SIGNAL(stopShutdownTimer()), shutdown_timer_, SLOT(stop()));

    HAL::registerPowersupply(name, this);
}

Powersupply::~Powersupply()
{
    delete shutdown_timer_;
}

void Powersupply::registerElement(JElement *element)
{
    if (elements_.contains(element))
        return;

    elements_.append(element);
}

void Powersupply::unregisterElement(JElement *element)
{
    elements_.removeOne(element);
}

void Powersupply::onTemperatureDomainChanged(JTemperatureController::Domain domain)
{
    setOn(on_);
}

void Powersupply::onDigitalOutputAdded(QString name, DigitalOutputPtr dout)
{
    if (name != digital_output_name_)
        return;

    disconnect(HAL::observer, &Observer::digitalOutputAdded, this, &Powersupply::onDigitalOutputAdded);
    connect(HAL::observer, &Observer::digitalOutputRemoved, this, &Powersupply::onDigitalOutputRemoved);

    connect(dout.get(), &DigitalOutput::stateChanged, this, &Powersupply::onDigitalOutputStateChanged);
    connect(this, &Powersupply::requestDigitalOutputStateChange, dout.get(), &DigitalOutput::setState);
}

void Powersupply::onDigitalOutputRemoved(QString name)
{
    if (name != digital_output_name_)
        return;

    disconnect(HAL::observer, &Observer::digitalOutputRemoved, this, &Powersupply::onDigitalOutputRemoved);
    connect(HAL::observer, &Observer::digitalOutputAdded, this, &Powersupply::onDigitalOutputAdded);
}

void Powersupply::onDigitalOutputStateChanged(bool state)
{
    if (state == on_)
        return;

    if (on_)
        emit stopShutdownTimer();

    on_ = state;
}

JReturnType<void> Powersupply::setOn(bool state)
{
    Temperature::constrain(this, state);

    // want to turn on
    if (state) {
        emit stopShutdownTimer();
        if (on_)
            return OK;
        else {
            // turn on and give time to stabilize
            qCDebug(traceHardware).nospace() << "switch on " << name_;
            emit requestDigitalOutputStateChange(true);
            QThread::msleep(startup_delay_ms_);
            return OK;
        }
    // want to turn off
    } else {
        emit startShutdownTimer(shutdown_delay_ms_);
        return OK;
    }
}
