#include "inc/hardware/system_monitor.h"

#include <QJsonArray>
#include <QFile>
#include "inc/tools/logging.h"
#include "inc/hal/hal.h"

SystemMonitor::SystemMonitor(QString name, QJsonValue config)
    : name_(name)
{
    qCDebug(traceInit) << "constructing" << name << "from" << config;

    QJsonValue c_update_interval = config["update_interval"];
    if (c_update_interval.isUndefined() or not c_update_interval.isDouble())
        throw std::runtime_error("Invalid config for " + name.toStdString() + ".udpate_interval");
    update_timer_ = new QTimer(this);
    update_timer_->setInterval(c_update_interval.toInt() * 1000);
    connect(update_timer_, &QTimer::timeout, this, &SystemMonitor::update);

    // Manually build different observables, but force them to be present in config file.
    // 		By that, the config must provide the complete picture.
    if (not config["analog_inputs"].toArray().contains("cpu_temperature"))
        throw std::runtime_error("Missing entry cpu_temperature in " + name_.toStdString() + ".analog_inputs");
    cpu_temperature_ = AnalogInputPtr(new AnalogInput(QString("%1.cpu_temperature").arg(name_)));
    HAL::addAnalogInput(cpu_temperature_);
}

void SystemMonitor::update()
{
    cpu_temperature_->update(getCpuTemperature());
}

double SystemMonitor::getCpuTemperature()
{
    QFile pi_cpu_temp("/sys/class/thermal/thermal_zone0/temp");
    if (!pi_cpu_temp.open(QIODevice::ReadOnly)) {
        qCWarning(temperature_log) << "Error reading temp of pi from /sys/class/thermal/thermal_zone0/temp; got " << errno;
        return Q_INFINITY;
    }

    // try reading file and converting content
    bool ok;
    double temperature = pi_cpu_temp.readLine().chopped(1).toInt(&ok) / 1000.;
    pi_cpu_temp.close();
    if (not ok)
        return Q_INFINITY;
    return temperature;
}
