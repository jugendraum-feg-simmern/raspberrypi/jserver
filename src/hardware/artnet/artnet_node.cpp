﻿/*!
 * \file artnet_node.cpp
 * \brief source of the ArtNetNode class
 */
#include "inc/hardware/artnet/artnet_node.h"

#include "inc/hal/hal.h"
#include <QJsonArray>

ArtNetNode::ArtNetNode(QString name, QHostAddress ip, uint16_t port, QJsonValue config)
{
    qCDebug(traceInit) << "constructing" << name << "from" << config;

    name_ = name;
    ip_ = ip;
    port_ = port;

    if (config.isUndefined())
        throw std::runtime_error("missing config for artnet_node");

    QJsonValue c_num_universes = config["num_universes"];
    QJsonValue c_max_fps = config["max_fps"];
    QJsonValue c_keepalive_interval_ms = config["keepalive_interval_ms"];

    // verify config is present
    if (c_num_universes.isUndefined())
        throw std::runtime_error("missing config for artnet_controller.node_config.num_universes");
    if (c_max_fps.isUndefined())
        throw std::runtime_error("missing config for artnet_controller.node_config.max_fps");
    if (c_keepalive_interval_ms.isUndefined())
        throw std::runtime_error("missing config for artnet_controller.node_config.keepalive_interval_ms");

    // verify config is valid
    if (not c_num_universes.isDouble())
        throw std::runtime_error("invalid config for artnet_controller.node_config.num_universes");
    if (not c_max_fps.isDouble())
        throw std::runtime_error("invalid config for artnet_controller.node_config.max_fps");
    if (not c_keepalive_interval_ms.isDouble())
        throw std::runtime_error("invalid config for artnet_controller.node_config.keepalive_interval_ms");

    keepalive_interval_ms_ = c_keepalive_interval_ms.toInt();
    frame_interval_ms_ = int(1000 / c_max_fps.toDouble());

    // create one buffer for each universe
    for (int i = 0; i < c_num_universes.toInt(); ++i)
        addUniverse(i);

    QJsonValue c_analog_outputs = config["analog_outputs"];
    if (c_analog_outputs.isUndefined())
        return;	// default constructed nodes may not have any outputs defined. That's ok.

    QJsonArray analog_outputs = c_analog_outputs.toArray();
    for (auto it : analog_outputs)
        outputs_.insert(analogOutputFromConfig(it.toObject()));
    for (auto it : outputs_)
        HAL::addAnalogOutput(it);
}

int ArtNetNode::addUniverse(uint16_t address)
{
    if (universes_.contains(address))
        return EADDRNOTAVAIL;

    universe_t* universe = new universe_t(new QTimer(this), new QTimer(this), new artdmx_packet_t);
    universe->buffer->opcode = ARTNET_OP_DMX;
    universe->buffer->universe = address;
    universe->keepalive_timer->setInterval(keepalive_interval_ms_);
    connect(universe->keepalive_timer, &QTimer::timeout, this, [=](){this->publishUniverse(universe); });
    universe->frame_timer->setSingleShot(true);
    universe->frame_timer->setInterval(frame_interval_ms_);
    connect(universe->frame_timer, &QTimer::timeout, this, [=](){this->publishUniverse(universe); });

    // not so elegant way of connecting the timer conbtrol signals to the correct universe
    connect(this, &ArtNetNode::startKeepaliveTimer, this, [=](universe_t* u){if (u == universe) u->keepalive_timer->start();} );
    connect(this, &ArtNetNode::startFrameTimer, this, [=](universe_t* u){if (universe == u) u->frame_timer->start();} );

    universes_.insert(address, universe);
    return OK;
}

int ArtNetNode::removeUniverse(uint16_t address)
{
    if (!universes_.contains(address))
        return EADDRNOTAVAIL;

    universes_.remove(address);
    return OK;
}

int ArtNetNode::readDMXBuffer(uint16_t univserse_addr, uint8_t dmx_addr, uint8_t& value)
{
    if (!universes_.contains(univserse_addr))
        return EOUTOFRANGE;

    value = universes_[univserse_addr]->buffer->data[dmx_addr];
    return OK;
}

int ArtNetNode::writeDMXBuffer(uint16_t univserse_addr, uint8_t dmx_addr, uint8_t value)
{
    if (has_external_controller_)
        return ENOTEMPTY;

    if (!universes_.contains(univserse_addr))
        return EOUTOFRANGE;

    universe_t* universe = universes_[univserse_addr];
    universe->buffer->data[dmx_addr] = value;
    universe->has_new_data = true;
    emit startKeepaliveTimer(universe);

    if (!universe->frame_timer->isActive())
        emit startFrameTimer(universe);

    return OK;
}

void ArtNetNode::publishUniverse(universe_t* universe)
{
    qDebug() << Q_FUNC_INFO;
    QNetworkDatagram datagram = makeDatagram(universe->buffer);
    emit datagramReady(datagram);
    universe->has_new_data = false;
}

QNetworkDatagram ArtNetNode::makeDatagram(artdmx_packet_t* buffer)
{
    // The QByteArray contructor creates a deep copy of the data.
    // We don't need this here since QNetworkDatagram contructor also copies the data. So we
    // 		use QByteArray::fromRawData(), which does not copy the data
    QNetworkDatagram datagram(QByteArray::fromRawData(reinterpret_cast<char*>(buffer), sizeof(artdmx_packet_t)), ip_, port_);
    return datagram;
}

AnalogOutputPtr ArtNetNode::analogOutputFromConfig(QJsonObject root_node)
{
    QString name = root_node.keys().first();

    if (root_node[name].isUndefined() or not root_node[name].isObject())
        throw std::runtime_error("Invalid config for " + name_.toStdString() + "." + name.toStdString());
    QJsonObject cfg = root_node[name].toObject();
    if (cfg["universe"].isUndefined() or not cfg["universe"].isDouble())
        throw std::runtime_error("Invalid config for " + name_.toStdString() + "." + name.toStdString() + ".universe");
    if (cfg["address"].isUndefined() or not cfg["address"].isDouble())
        throw std::runtime_error("Invalid config for " + name_.toStdString() + "." + name.toStdString() + ".address");

    uint16_t universe = cfg["universe"].toInt();
    uint8_t address = cfg["address"].toInt();

    QString hal_name = QString("%1.%2").arg(name_, name);

    auto setter = [=](quint8 val) -> int {
        return this->writeDMXBuffer(universe, address, val);
    };

    auto getter = [=](quint8& val) -> int {
        return this->readDMXBuffer(universe, address, val);
    };

    return AnalogOutputPtr(new AnalogOutput(hal_name, setter, getter));
}
