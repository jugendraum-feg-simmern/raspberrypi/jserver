/*!
 * \file ultm8.cpp
 * \brief source of the Ultim8 class
 */
#include "inc/hardware/artnet/ultim8.h"
#include "inc/tools/logging.h"


Ultim8::Ultim8(QString name, QHostAddress ip, uint16_t port, QJsonValue config)
    : ArtNetNode(name, ip, port, config)
{
    // no spezialization needed
}

Ultim8::~Ultim8()
{
    // no spezialization needed
}

void Ultim8::publishUniverse(universe_t *universe)
{
    qCDebug(traceArtnet) << Q_FUNC_INFO;
    // the ulim8s want the data for all universes, even when just updating one
    for (auto universe : universes_) {
        QNetworkDatagram datagram = makeDatagram(universe->buffer);
        emit datagramReady(datagram);
    }
}
