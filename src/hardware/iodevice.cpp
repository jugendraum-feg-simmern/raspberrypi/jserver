#include "inc/hardware/iodevice.h"

#include <QDebug>

IODevice::IODevice(int init_retry_interval_s, QObject *parent)
    : QObject(parent)
{
    retry_timer_ = new QTimer(this);
    retry_timer_->setInterval(init_retry_interval_s);
    connect(this, SIGNAL(startRetryTimer()), retry_timer_, SLOT(start()));
    connect(this, &IODevice::stopRetryTimer, retry_timer_, &QTimer::stop);
}

IODevice::~IODevice()
{
    retry_timer_->stop();
    delete retry_timer_;
}

int IODevice::ioInit(io_init_func_t init)
{
    init_ = std::move(init);

    int rc = -1, i = 0;
    while (((rc = init_()) != 0) and (i < MAX_INIT_RETRY))
        ++i;

    setReady(rc == 0);

    return rc;
}

int IODevice::ioRead(io_read_func_t read)
{
    if (not ready_)
        return ENXIO;

    int rc = -1, i = 0;
    while (((rc = read()) != 0) and (i < MAX_READ_RETRY))
        ++i;

    consecutive_fails_mutex_.lock();
    consecutive_fails_ = (rc == 0) ? 0 : consecutive_fails_ + 1;
    if (consecutive_fails_ > MAX_CONSECUTIVE_FAILS)
        setReady(false);
    consecutive_fails_mutex_.unlock();

    return rc;
}

int IODevice::ioWrite(io_write_func_t write)
{
    if (not ready_)
        return ENXIO;

    int rc = -1, i = 0;
    while (((rc = write()) != 0) and (i < MAX_WRITE_RETRY))
        ++i;

    consecutive_fails_mutex_.lock();
    consecutive_fails_ = (rc == 0) ? 0 : consecutive_fails_ + 1;
    if (consecutive_fails_ > MAX_CONSECUTIVE_FAILS)
        setReady(false);
    consecutive_fails_mutex_.unlock();

    return rc;
}

void IODevice::initRetry()
{
    ioInit(init_);
}

void IODevice::setReady(bool ready)
{
    if (ready_ == ready)
        return;
    ready_ = ready;

    emit (ready_) ? stopRetryTimer() : startRetryTimer();
}
