/*!
 * \file hardware.h
 * \brief defining the hw namespace as the connection for jElement to the hardware
 */

#ifndef HARDWARE_H
#define HARDWARE_H

#include "temperature_sensor_udp.h"
#include "artnet_controller.h"
#include "mcp23017.h"
#include "pca9635.h"
#include "wall_switch.h"
#include "system_monitor.h"

#include "powersupply.h"


/*!
 * \brief the hw namespace (abbreviation for 'hardware') provides access to
 *     devices on the I2C bus, the UART bus and the wall switch
 */
namespace hw
{
    /*!
     * \brief initializes the hardware
     */
    void init(bool mocking = false, bool skip_init = false);

    /*!
     * \brief delete all HW elements on program exit
     */
    void exit();

    extern bool mocked;

    extern TemperatureSensorUDP* temperature_sensor_udp;
    extern SystemMonitor* system_monitor;
    extern ArtNetController* artnet_controller;
    extern MCP23017* portexpander;
    extern PCA9635* pca_light;
    extern PCA9635* pca_fan;
    extern WallSwitch* wallswitch;

    extern Powersupply *ps_48V, *ps_12V;
}

#endif // HARDWARE_H
