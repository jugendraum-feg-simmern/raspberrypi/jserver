/*!
 * \file artnet_node.h
 * \brief header of the ArtNetNode class
 * \todo Do we want the MAX_TIMEOUT_EVENTS read from config -> discuss with @nope
 * 		\link https://gitlab.com/jugendraum-feg-simmern/raspberrypi/jserver/-/issues/73
 */
#ifndef ARTNET_NODE_H
#define ARTNET_NODE_H

#include <QMap>
#include <QTimer>
#include <QObject>
#include <QHostAddress>
#include <QSharedPointer>
#include <QNetworkDatagram>

#include "artnet.h"
#include "inc/config.h"
#include "inc/tools/errno.h"
#include "inc/hal/analog_output.h"

#define MAX_TIMEOUT_EVENTS 3

/*!
 * \brief keep all elements neccessary to manage a universe (512 DMX channels)
 */
typedef struct universe_s {
    bool has_new_data;
    artdmx_packet_t* buffer;
    QTimer* keepalive_timer;
    QTimer* frame_timer;

    universe_s(QTimer* ktimer, QTimer* ftimer, artdmx_packet_t* buffer) : buffer(buffer), keepalive_timer(ktimer), frame_timer(ftimer){};
} universe_t;

/*!
 * \brief The ArtNetNode class represents the control satructure for a single node in the ArtNet
 * \link http://www.artisticlicence.com/WebSiteMaster/User%20Guides/art-net.pdf
 */
class ArtNetNode : public QObject
{
    Q_OBJECT
public:
    explicit ArtNetNode(QString name, QHostAddress ip, uint16_t port, QJsonValue config);
    ~ArtNetNode() {};

    /*!
     * \brief writeDMXBuffer write a value into the buffer to be transmitted in the next frame
     * \details Firstly, the desired \a value is written into the buffer of the universe.
     * 		Then, if the frame_timer is not running, it is started. Thereby, all calls to this
     * 		function in the next \a min_frame_interval_ms miliseconds are collected and transmitted in
     * 		the same buffer. Also we ensure to not exceed the maximum framereate of the node.
     * 		Additionally, the keepalive_timer for the universe is (re-)started. By that we repeat
     * 		the buffer after keepalive_interval_ms miliseconds, when no new values were written in the
     * 		meantime.
     * \param univserse the universe to which the \a dmx_address is associated
     * \param dmx_addr address of the dmx receiver
     * \param value the value to be written
     * \return OK when successful, else errno
     */
    int writeDMXBuffer(uint16_t univserse, uint8_t dmx_addr, uint8_t value);

    /*!
     * \brief readDMXBuffer fetch the current value from the buffer
     * \details This resembles the actual state of the component since we cannot
     * 		read its value directly (no DMX-RDM support). Additionally, the buffer
     * 		is being send periodically, thereby giving further ensurance that the compont
     * 		displays this value.
     * \param univserse the universe to which the \a dmx_address is associated
     * \param dmx_addr address of the dmx element
     * \param value address to write the current state to
     * \return OK when successful, else errno
     */
    int readDMXBuffer(uint16_t univserse, uint8_t dmx_addr, uint8_t& value);

    /*!
     * \brief addUniverse attach a universe with the given address to the node
     * \param address the address of the universe
     * \return OK when successful, else errno
     */
    int addUniverse(uint16_t address);

    /*!
     * \brief removeUniverse remove a universe from the node
     * \param address the address of the universe
     * \return OK when successful, else errno
     */
    int removeUniverse(uint16_t address);

    /*!
     * \brief hasExternalController put node in external controller mode
     * \details ignore all write requests from jServer and do not write anything
     * 		on the bus. Automatically called when external Art-Net™ controller is detected.
     * \param has_external_controller true when node shall be controlled externally
     */
    void hasExternalController(bool has_external_controller) { has_external_controller_ = has_external_controller; };

    /*!
     * \brief hasExternalController getter for the hasExternalController property
     * \return true when node controlled externally
     */
    bool hasExternalController() { return has_external_controller_; };

    /*!
     * \brief timeoutEvents how many times in a row the node has failed to respond to an ArtPoll
     * 		We use this to remove nodes,
     * \return the number of timeout events
     */
    int timeoutEvents() { return timeout_events_; };

    /*!
     * \brief increaseTimeoutEventsthe increase the amount of times in a row the node has failed to respond to an ArtPoll
     */
    void increaseTimeoutEvents() { ++timeout_events_;};

    /*!
     * \brief resetTimeoutEvents set the number of timeout events to 0, used e.g. on a reception of an ArtPollReply
     */
    void resetTimeoutEvents() { timeout_events_ = 0; };

    /*!
     * \brief the IP address of the node
     */
    QHostAddress ip() { return ip_; };

    /*!
     * \brief the port of the node
     */
    uint16_t port() { return port_; };

protected slots:
    //! create a datagram from the given universe and emit \a newDatagramReady
    virtual void publishUniverse(universe_t* universe);
signals:
    //! emitted whenever a datagram needs to be send. Datagram is complely finished to be send with QUdpSocket::writeDatagram()
    void datagramReady(QNetworkDatagram datagram);
    //! start the frame_timer of the specified universe
    void startFrameTimer(universe_t* universe);
    //! start the keepalive_timer of the specified universe
    void startKeepaliveTimer(universe_t* universe);
protected:
    QNetworkDatagram makeDatagram(artdmx_packet_t* buffer);

    // variables
    QHostAddress ip_;
    uint16_t  port_;
    QTimer* frame_timer_;
    bool has_external_controller_ = false;
    int timeout_events_ = 0;

    //! one buffer for each universe, use universe address as key
    QMap<uint16_t, universe_t*> universes_;
    int keepalive_interval_ms_, frame_interval_ms_;
private:
    AnalogOutputPtr analogOutputFromConfig(QJsonObject config);
    QString name_;
    QSet<AnalogOutputPtr> outputs_;
};

typedef QSharedPointer<ArtNetNode> artnetnode_ptr_t;

#endif // ARTNET_NODE_H
