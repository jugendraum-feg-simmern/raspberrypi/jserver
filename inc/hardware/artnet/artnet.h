/*!
 * \file artnet.h
 * \brief constants of the Art-Net™ 4 Protocol
 * \link http://www.artisticlicence.com/WebSiteMaster/User%20Guides/art-net.pdf
 */
#ifndef ARTNET_H
#define ARTNET_H

#include <cstdint>
#include <cstdio>

// fixed
#define ARTNET_ID 				"Art-Net"
#define ARTNET_DEFAULT_PORT		6454
#define ARTNET_HEADER_SIZE		18
#define ARTNET_MAX_DATA_SIZE	512
#define ARTNET_POLL_INTERVAL_MS 2500

// protocol version
#define ARTNET_PROT_VERSION		14

// selection of opcodes
#define ARTNET_OP_POLL 			0x2000
#define ARTNET_OP_POLLREPLY		0x2100
#define ARTNET_OP_DMX 			0x5000

// selection of style codes
#define ARTNET_ST_NODE			0x00
#define ARTNET_ST_CONTROLLER	0x01
#define ARTNET_ST_MEDIA			0x02
#define ARTNET_ST_ROUTE			0x03
#define ARTNET_ST_BACKUP		0x04
#define ARTNET_ST_CONFIG		0x05
#define ARTNET_ST_VISUAL		0x06

// selection of OEM codes
#define ARTNET_OEM_ULTIM8		0x29AD

#pragma pack(1)
//! the generic part of the header shared by all packet types
typedef struct {
    const char id[8] = ARTNET_ID;
    uint16_t opcode = 0x0000;
} art_packet_t;

//! layout of an ArtPoll packet
typedef struct : art_packet_t {
    uint8_t prot_version_H = ARTNET_PROT_VERSION >> 8;
    uint8_t prot_version_L = ARTNET_PROT_VERSION;
    uint8_t talk_to_me = 0x00;
    uint8_t priority = 0x00;
} artpoll_packet_t;

//! layout of an ArtPollReply packet
typedef struct : art_packet_t {
    uint8_t ip[4] = {};
    uint16_t port = 0x0000;
    uint8_t firmware_version_H = 0x00;
    uint8_t firmware_version_L = 0x00;
    uint8_t net_switch = 0x00;
    uint8_t sub_switch = 0x00;
    uint8_t oem_H = 0x00;
    uint8_t oem_L = 0x00;
    uint8_t ubea = 0x00;
    uint8_t status1 = 0x00;
    uint8_t esta_man_L = 0x00;
    uint8_t esta_man_H = 0x00;
    char short_name[18] = {};
    char long_name[64] = {};
    char node_report[64] = {};
    uint8_t num_ports_H = 0x00;
    uint8_t num_ports_L = 0x00;
    uint8_t port_types[4] = {};
    uint8_t good_input[4] = {};
    uint8_t good_output[4] = {};
    uint8_t sw_in[4] = {};
    uint8_t sw_out[4] = {};
    uint8_t sw_video = 0x00;
    uint8_t sw_macro = 0x00;
    uint8_t sw_remote = 0x00;
    uint8_t spare[3] = {};
    uint8_t style = 0x00;
    uint8_t mac[6] = {};
    uint8_t bind_ip[4] = {};
    uint8_t bind_index = 0x00;
    uint8_t status2 = 0x00;
    uint8_t filler[26] = {};
} artpollreply_packet_t;

//! layout of an ArtDmx packet
typedef struct : art_packet_t {
    uint8_t prot_version_H = ARTNET_PROT_VERSION >> 8;
    uint8_t prot_version_L = ARTNET_PROT_VERSION;
    uint8_t seq = 0x00;
    uint8_t phys = 0x00;
    uint16_t universe = 0x0000;
    uint16_t data_length = ARTNET_MAX_DATA_SIZE;
    uint8_t data[ARTNET_MAX_DATA_SIZE] = {};
} artdmx_packet_t;
#pragma pack(0)

//! helper function to display packets as raw data
static void artnet_packet_dump_raw(artdmx_packet_t* packet) {
    char *p = reinterpret_cast<char *>(packet);
    for (unsigned int i = 0; i < sizeof(artdmx_packet_t); ++i) {
        printf("%02x", p[i]);
        if (i % 4 == 3) printf(" ");
        if (i % 32 == 31) printf("\n");
    }
    printf("\n");
}

//! helper function to decode and display artdmx_packets in key: value layout
static void artnet_packet_dump(artdmx_packet_t* packet) {
    printf("id: %s\n", packet->id);
    printf("opcode: 0x%04x\n", packet->opcode);
    printf("prot_version_H: 0x%04x\n", packet->prot_version_H);
    printf("prot_version_L: 0x%04x\n", packet->prot_version_L);
    printf("seq: 0x%02x\n", packet->seq);
    printf("phys: 0x%02x\n", packet->phys);
    printf("universe: 0x%04x\n", packet->universe);
    printf("data_length: 0x%04x\n", packet->data_length);
    printf("data: ");
    for (int i = 0; i < packet->data_length; ++i) {
        printf("%02x", packet->data[i]);
        if (i % 4 == 3) printf(" ");
        if (i % 32 == 31) printf("\n      ");
    }
    printf("\n");
}

#endif // ARTNET_H
