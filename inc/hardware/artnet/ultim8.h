/*!
 * \file ultim8.h
 * \brief header of Ultim8 class
 */
#ifndef ULTIM8_CONTROLLER_H
#define ULTIM8_CONTROLLER_H

#include "artnet.h"
#include "artnet_node.h"

#include "inc/config.h"

/*!
 * \brief The Ultim8 class ArtNet node with some spezialisations as requested by the Ultim8s
 */
class Ultim8 : public ArtNetNode
{
    Q_OBJECT
public:
    explicit Ultim8(QString name, QHostAddress ip, uint16_t port, QJsonValue config);
    ~Ultim8();
protected slots:
    /*!
     * \brief publishUniverse Since the ulimtates are configured to expect data
     * 		for all universes, we override ArtNetNode::publishUnivers to always send
     * 		all universes
     */
    void publishUniverse(universe_t* universe);
};

#endif // ULTIM8_CONTROLLER_H
