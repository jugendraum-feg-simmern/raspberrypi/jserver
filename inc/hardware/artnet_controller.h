/*!
 * \file artnet_controller.h
 * \brief header of the ArtNetController class
 */
#ifndef ARTNET_CONTROLLER_H
#define ARTNET_CONTROLLER_H

#include <QHash>
#include <QTimer>
#include <QDebug>
#include <QObject>
#include <QUdpSocket>
#include <QSharedPointer>

#include <algorithm>

#include "inc/config.h"

#include "artnet/artnet.h"
#include "artnet/artnet_node.h"
#include "artnet/ultim8.h"
#include "inc/tools/logging.h"

/*!
 * use the pair of ip and port as identifier for a node.
 * Maybe increase performance by using faster key. The contructor for the key must accept {ip, port}.
 */
typedef QPair<QHostAddress, uint16_t> node_identifier_t;

/*!
 * \brief The ArtNetController class manges all ArtNetNodes available on the network
 * \link http://www.artisticlicence.com/WebSiteMaster/User%20Guides/art-net.pdf
 */
class ArtNetController : public QObject
{
    Q_OBJECT
public:
    /*!
     * \brief ArtNetController contructor for an Art-Net™ controller
     * \todo to be compliant with the Art-Net™ protocol, we must broadcast an ArtPollReply on startup
     * 		see \link https://gitlab.com/jugendraum-feg-simmern/raspberrypi/jserver/-/issues/74
     */
    explicit ArtNetController(QJsonValue config);
    ~ArtNetController() {};

    /*!
     * \brief init initializes the controller
     * \param mocked when set, the controller emulates any node that is requested by \a getNode
     * \details This is done outside of the constructor because we create an instance in the hardware namespace
     * 		what is done before the event loop is initialized. So we can call init() after the event loop has been
     * 		created.
     */
    void init(bool mocked = false);

    /*!
     * \brief isMocked tell wether the controller is mocking its nodes (i.e. pretending they were available)
     * \return true when mocked
     */
    bool isMocked() { return mocked_; };

    /*!
     * \brief addArtNetNode create a new node and add it to the list of controlled nodes
     * \param ip IP of the node
     * \param port listening port of the node
     * \param n_universes number of universes controlled by the node. Must have a continuous address
     * 		space staring from 0
     */
    void addNode(QHostAddress ip, uint16_t port, uint16_t oem=0x0000);

    /*!
     * \brief removeNode remove a node from the list of controlled nodes
     * \param id pair of IP and port of the node
     */
    void removeNode(node_identifier_t id);

    /*!
     * \brief getNode get the pointer to the node identified by \a ip and \a port
     * \param id pair of IP and port of the node
     * \return QSharedPointer to an ArtNetNode. When no node is found, nullptr is returned
     */
    artnetnode_ptr_t getNode(node_identifier_t id);
private slots:
    //! handler for the readyRead signal of the QUdpSocket
    void onReadyRead();

    //!handler to publish datagrams by nodes
    void onNodeDatagramReady(QNetworkDatagram);

    /*!
     * \brief poll broadcast a ArtPoll packet
     * \details After the ArtPoll packet is send, a timer is started. While it is running, all
     * 		incoming ArtPollReply packets are queued and evaluated on its timeout.
     * \todo what bits in artpoll_packet_t.talk_to_me do we want to set -> discuss with @nope
     * 		\link https://gitlab.com/jugendraum-feg-simmern/raspberrypi/jserver/-/issues/76
     */
    void poll();

private:
    /*!
     * \brief processPollReplyQueue Evaluate the response from the last ArpPoll packet
     * \details When new nodes are detected, they get added to the list of known_nodes.
     * 		When an external controller is detected, all nodes get switched to external control mode
     * \todo upon entry, create a local copy of \a poll_reply_queue_ and clear \a poll_reply_queue_.
     * 		Thereby, incoming replies do not interfere with the processing of the current set of replies.
     */
    void processPollReplyQueue();

    // ########################################################################
    // ## artnet handlers
    // ########################################################################
    /*!
     * \brief handleDatagram handler for all data received on the UDP socket
     * \param datagram the received datagram
     */
    void handleDatagram(QNetworkDatagram &datagram);

    /*!
     * \brief handleOpPoll handler for Art-Net™ packets with the OpPoll opcode
     * \param datagram the received datagram
     * \note using the whole QNetworkDatagram here to preseve the information about the sender
     * \todo discuss with @nope which entries of the reply have to be filled with which data
     * 		\link https://gitlab.com/jugendraum-feg-simmern/raspberrypi/jserver/-/issues/75
     */
    void handleOpPoll(QNetworkDatagram &datagram);

    /*!
     * \brief handleOpPollReply handler for Art-Net™ packets with the OpPollReply opcode
     * \param datagram the received datagram
     * \note using the whole QNetworkDatagram here to preseve the information about the sender
     */
    void handleOpPollReply(QNetworkDatagram &datagram);

    /*!
     * \brief handleOpDMX handler for Art-Net™ packets with the OpDMX opcode. Intended primarily for
     * 		development purposes.
     * \param datagram the received datagram
     * \todo the controller should not receive dmx packets -> properly react; do not spam logs/console
     * 		by dumping the packet
     */
    void handleOpDMX(QNetworkDatagram &datagram);

    /*!
     * \brief get the ip address if the pi
     * \return the ip address of the pi
     * \todo remove hardcoded "eth0"
     */
    QHostAddress getLocalAddress();

    /*!
     * \brief get the broadcast address for the subnet
     * \return the broadcast address for the subnet
     * \todo remove hardcoded "eth0"
     */
    QHostAddress getBroadcastAddress();

    /*!
     * \brief helper to check if a node with the specified IP is present in the hardware config.
     * 		If so, the name from the config is used, otherwise a default name is constructed "artnet_node_<nodes_.count()>"
     */
    QString nodeNameFromConfig(QJsonValue config, QHostAddress ip);

    bool mocked_;
    QUdpSocket* socket_;
    QHostAddress local_address_, broadcast_address_;
    int port_;
    QHash<node_identifier_t, artnetnode_ptr_t> nodes_;
    QTimer* poll_timer_;
    int poll_interval_ms_, max_node_timeout_events_;

    QHash<node_identifier_t, artpollreply_packet_t*> poll_reply_queue_;
};


#endif // ARTNET_CONTROLLER_H
