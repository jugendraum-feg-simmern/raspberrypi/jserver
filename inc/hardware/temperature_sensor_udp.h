/*!
 * \file temperature_sensor_udp.h
 * \brief Header of the TemperatureSensorUDP class
 */
#ifndef TEMPERATURE_SENSOR_UDP_H
#define TEMPERATURE_SENSOR_UDP_H

#include <QUdpSocket>
#include <QHostAddress>
#include <QTimer>

#include "iodevice.h"
#include "inc/config.h"

#include "inc/hal/analog_input.h"

/*!
 * \brief Access remote temperature sensors via UDP.
 * \todo refactor once the arduino uses a proper protocol, see \link https://gitlab.com/jugendraum-feg-simmern/firmware/arduino-temperature-sensors-udp/-/issues/4
 */
class TemperatureSensorUDP : public IODevice
{
public:
    TemperatureSensorUDP(QString name, QJsonValue config);
    ~TemperatureSensorUDP();

    /*!
     * \brief initialize the device
     * \param mocking when set to true, the device will not try to access real hardware.
     * \param skip_init when set to true, the device will not initialize its hardware.
     * \return 0 when successful, else errno
     */
    int init(bool mocking=false, bool skip_init=false);

private:
    /*!
     * \brief read the temperature of a sensor
     * \param sensor id of the sensor
     * \param temperature reference to the read value. When the device is mocked, the temperature
     * 		is a random value on each read between [-100, -160)
     * \return 0 when successful, else errno
     */
    int readTemperature(const int sensor, double& temperature);

    /*!
     * \brief read values from arduino and update analog inputs.
     */
    void update();

    AnalogInputPtr analogInputFromConfig(QJsonObject config);
    QString name_;
    QHash<int, AnalogInputPtr> inputs_;

    bool mocked_;
    QList<uint16_t> regs_;
    QUdpSocket* udp_socket_ = nullptr;
    QHostAddress host_;
    quint16 port_;
    int timeout_ms_;

    QTimer *update_timer_;
};

#endif // TEMPERATURE_SENSOR_UDP_H
