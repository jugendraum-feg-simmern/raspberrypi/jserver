/*!
 * \file powersupply.h
 * \brief header for the Powersupply class
 */
#ifndef POWERSUPPLY_H
#define POWERSUPPLY_H

#include <QObject>
#include <QTimer>
#include <QColor>

#include <type_traits>

#include "inc/core/j_element.h"
#include "inc/tools/errno.h"
#include "inc/temperature/temperature_controlled.h"
#include "inc/hal/digital_output.h"

template<typename T>
bool operator>(const QColor &lval, const T &rval) { return lval.value() > rval; };
//template<typename T>
//bool operator>(const T &lval, const QColor &rval) { return not (rval < lval); }

/*!
 * \brief Representation of a power supply.
 * \details The powersupply object is used to turn on a physical powersupply when necessary
 * 		and turn it off when not in use. To determine the desired state of the powersuppyly,
 * 		any connected device must register itself to the Powersupply using the \a registerElement()
 * 		method.	After that, its state is taken into account. Then all setters of the element
 * 		that influence \a isOn() of the element have to be wrapped into \a handle().
 * \todo this needs some refinement. ATM, just the bare minimum of semantic errors have been fixed
 * 		to compile the code. We need to properly use the new HAL here.
 */
class Powersupply : public QObject, public TemperatureControlled
{
    Q_OBJECT
public:
    explicit Powersupply(QString name, QJsonValue config);
    ~Powersupply();

    /*!
     * \brief register an Element to the powersupply. By that the supply is not turned off
     * 		as long as \a element->isOn returns true.
     * \param element Pointer to the JElement to be registered
     */
    void registerElement(JElement *element);

    /*!
     * \brief unregister an Element from the powersupply.
     * \param element Pointer to the JElement to be unregistered
     */
    void unregisterElement(JElement *element);

    /*!
     * \brief wrap a setter into powersupply handeling
     * \details When setting a value greater than 0, the powersupply is turned off, it is
     * 		switched on and, after a short delay to allow the device to stabilize, the handler is
     * 		called.
     * 		When setting a value equal to zero, all registered elements are checked and if no one is
     * 		on, a timer is started to switch off the device after some time.
     * 		Have a look into \a HauptlichStreifen::setBrightness for an example of how this is
     * 		intended to be used
     * \param handler the actual setter. Must take exactly one argument comparable to (int)0
     * 		and return JReturnType<void>
     * \returns wrapped handler. Takes the same arg as \a handler
     */
    template <typename FUNC>
    auto handle(FUNC &&handler){
        return [handler = std::forward<FUNC>(handler), this](auto arg)
        {
            JReturnType<void> rc;

            // turn on when necessary
            if (arg > 0) {
                rc = setOn(true);
                if (rc.err_code != OK) return rc;
            }

            // set the brightness
            handler(arg);

            // turn off powersupply when all elements are off.
            // 		Elements of which the status cannot be read are assumed off.
            bool on = false;
            for (auto e : elements_)
                on |= e->on();
            if (not on) {
                rc = setOn(false);
                if (rc.err_code != OK)
                    return rc;
            }
            return rc;
        };
    }

signals:
    void startShutdownTimer(int timeout);
    void stopShutdownTimer();
    void requestDigitalOutputStateChange(bool state);

private slots:
    void onTemperatureDomainChanged(JTemperatureController::Domain domain) override;
    void onDigitalOutputAdded(QString name, DigitalOutputPtr dout);
    void onDigitalOutputRemoved(QString name);
    void onDigitalOutputStateChanged(bool state);

private:
    //! the state of the powersupply is exclusively changed by the registered elements. This is just an internal helper.
    bool on() { return on_; };
    //! the state of the powersupply is exclusively changed by the registered elements. This is just an internal helper.
    JReturnType<void> setOn(bool on);

    QString name_, digital_output_name_;
    QTimer *shutdown_timer_;
    QList<JElement *> elements_;
    int startup_delay_ms_, shutdown_delay_ms_;
    bool on_ = false;
};

#endif // POWERSUPPLY_H
