#ifndef IODEVICE_H
#define IODEVICE_H

#include <QObject>
#include <QMutex>
#include <QTimer>
#include <functional>

#define MAX_INIT_RETRY			3	///< number of attempts before init is considered to have failed
#define MAX_READ_RETRY			5	///< number of attempts before read is considered to have failed
#define MAX_WRITE_RETRY			5	///< number of attempts before write is considered to have failed
#define MAX_CONSECUTIVE_FAILS	5	///< number of consecutive failed r/w operations before device is considered not ready

#define RETRY_INTERVAL_DEFAULT	1

typedef std::function<int(void)> io_init_func_t;
typedef std::function<int(void)> io_read_func_t;
typedef std::function<int(void)> io_write_func_t;

/*!
 * \brief The IODevice class provides wrappers for hardware access to handle errors
 * \details when accessing hardware, it is possible that the device does not respond, e.g.
 * 		due to bus errors or the device being disconnected. This class centralizes the handling
 * 		of such errors by providing wrappers for read(), write() and init() calls to hardware.
 * \example IODevice
 * 		This example shows how to handle errors using the IODevice class
 * 		\code{.cpp}
 * 		#include <QObject>
 * 		#include "iodevice.h"
 *
 * 		class MyClass : public IODevice
 * 		{
 * 			Q_OBJECT
 * 		public:
 * 			int readValueFromHardware(char &val) {
 * 				auto handler = [=, &val]() -> int {
 * 					return hw::device->readValue(val);
 * 				};
 * 				return ioRead(handler);
 * 			}
 * 		};
 * 		\endcode
 */
class IODevice : public QObject
{
    Q_OBJECT
public:
    /*!
     * \brief Constructor of the IODevice class
     * \param interval interval in seconds to retry initialization upon failure
     * \param parent pointer to the parent of the QObject
     */
    IODevice(int interval=RETRY_INTERVAL_DEFAULT, QObject *parent=nullptr);
    ~IODevice();

    /*!
     * \brief check whether the device is present and operational
     * \return true if present and operational
     */
    bool ready() { return ready_; }

    /*!
     * \brief the interval in seconds with which the init function is called when the initial call to ioInit() failed
     * \return interval in s
     */
    int retryInterval() { return retry_timer_->interval(); }

    /*!
     * \brief set the interval in seconds with which the init function is called when the initial call to ioInit() failed
     * \param interval interval in s
     */
    void setRetryInterval(int interval) { retry_timer_->setInterval(interval * 1000); }

protected:
    /*!
     * \brief wrap the \a init() function for error handeling
     * \details when the call to \a init() fails \a MAX_INIT_RETRY times, the device is considered absent.
     * 		A timer is started, which calls this method every \a initRetryInterval() seconds until it succeeds.
     * 		Then the timer is stopped and readyChanged(true) is emitted.
     * \param init initialization method. Must return 0 on success.
     * \return return value of the last call to \a init()
     */
    int ioInit(io_init_func_t init);

    /*!
     * \brief wrap the \a read() function for error handeling
     * \details When the call to \a read() fails \a MAX_READ_RETRY times, the opeeration is considered to have failed.
     * 		In that case, the device-global counter is increased. When the value of that counter exceeds
     * 		\a MAX_CONSECUTIVE_FAILS, the device is considered absent and a timer is started which calls the
     * 		init() funtion of \a ioInit() every \a retryInterval() seconds. Also \a readyChanged(False) is emitted.
     * 		When the call to \a read() succeeds, the device-global counter is resetted.
     * \param read the method to read a value from the hardware. Must return 0 on success.
     * \return return value of the last call to \a read()
     */
    int ioRead(io_read_func_t read);

    /*!
     * \brief wrap the \a write() function for error handeling
     * \details When the call to \a write() fails \a MAX_WRITE_RETRY times, the opeeration is considered to have failed.
     * 		In that case, the device-global counter is increased. When the value of that counter exceeds
     * 		\a MAX_CONSECUTIVE_FAILS, the device is considered absent and a timer is started which calls the
     * 		init() funtion of \a ioInit() every \a retryInterval() seconds. Also \a writeyChanged(False) is emitted.
     * 		When the call to \a write() succeeds, the device-global counter is resetted.
     * \param write the method to write a value to the hardware. Must return 0 on success.
     * \return return value of the last call to \a write()
     */
    int ioWrite(io_write_func_t write);

signals:
    void startRetryTimer();
    void stopRetryTimer();
private slots:
    void initRetry();
private:
    void setReady(bool ready);
    bool ready_ = false;
    int consecutive_fails_ = 0;
    QMutex consecutive_fails_mutex_;
    io_init_func_t init_;

    QTimer *retry_timer_;
};

#endif // IODEVICE_H
