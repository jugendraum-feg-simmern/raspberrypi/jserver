#ifndef SYSTEM_MONITOR_H
#define SYSTEM_MONITOR_H

#include <QObject>
#include <QJsonValue>
#include <QTimer>
#include <QSet>
#include "inc/hal/analog_input.h"

class SystemMonitor : public QObject
{
    Q_OBJECT
public:
    SystemMonitor(QString name, QJsonValue config);
    virtual ~SystemMonitor() {};

    void start() { update_timer_->start(); };
    void stop() { update_timer_->stop(); };

private slots:
    void update();

private:
    double getCpuTemperature();

    QString name_;
    AnalogInputPtr cpu_temperature_;
    QTimer *update_timer_;
};

#endif // SYSTEM_MONITOR_H
