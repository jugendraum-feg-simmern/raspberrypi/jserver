/*!
 * \file mcp23017.h
 * \brief header of MCP23017 class
 */
#ifndef MCP23017_H
#define MCP23017_H

#include "i2cdevice.h"
#include "iodevice.h"
#include "inc/hal/digital_output.h"
#include "inc/hal/digital_input.h"

/*!
 * \brief Provide digital channels via an MCP23017
 */
class MCP23017 : public IODevice, private I2CDevice
{
public:
    MCP23017(QString name, QJsonValue config);
    ~MCP23017() {};

    /*!
     * \brief setup IODIR for ports and initialize outputs to 0
     * \param mocking when set to true, write to and read from RAM insted of hardware
     * \param skip_init when set to true, the hardware is left unchanged, so no is not initialized. Default false.
     * \return 0 on successful initialization, else -1
     */
    int init(bool mocking, bool skip_init = false);

    int setPinState(uint8_t pin, uint8_t port, bool state);
    int getPinState(uint8_t pin, uint8_t port, bool& state);

private:
    DigitalOutputPtr digitalOutputFromConfig(QJsonObject config);
    DigitalInputPtr digitalInputFromConfig(QJsonObject config);

    QString name_;
    QSet<DigitalOutputPtr> outputs_;
    QSet<DigitalInputPtr> inputs_;
};

#endif // MCP23017_H
