#ifndef WALL_SWITCH_H
#define WALL_SWITCH_H

#include <QObject>
#include <QThreadPool>
#include <QtConcurrent>
#include <QFutureWatcher>
#include <QDateTime>
#include <QDebug>

#include <stdio.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <poll.h>

#include "inc/tools/errno.h"
#include "inc/tools/logging.h"

/*!
 * \brief The WallSwitch class
 * \details use poll to wait until an event occurs and then emit a signal
 * \note This is heavily inspired by wiringPiISR, but by doing it manually,
 *      we have a lot more control over the callback.
 */
class WallSwitch : public QObject
{
    Q_OBJECT
public:
    WallSwitch(QJsonValue config);
    ~WallSwitch();
    /*!
     * \brief init
     * \details start monitoring the isr_pin
     */
    void init(bool mocking = false);
signals:
    /*!
     * \brief stateChanged gets emitted when the wall switch is pressed
     */
    void stateChanged();
private slots:
    /*!
     * \brief onInterrupt
     */
    void onInterrupt();
private:
    /*!
     * \brief setupPin
     * \param pin use bcm pin numbering
     * \return
     */
    int setupPin(int pin, int& pin_fd);

    /*!
     * \brief waitForInterrupt
     * \todo how to ract to non-zero return value of poll?
     */
    void waitForInterrupt(int pin_fd);

    /*!
     * \brief readPin reads the state of a pin
     * \param pin_fd the file descriptor associated with that pin as returned
     *      e.g. by setupPin
     * \param state location to write the new state of the pin
     * \return OK when successfull, else errno.
     */
    int readPin(int pin_fd, bool& state);

    int isr_pin_, ctrl_pin_;
    int isr_pin_fd_, ctrl_pin_fd_;
    QThreadPool* wall_switch_thread_pool_;  //!< private thread pool to free up global pool for important tasks
    QFutureWatcher<void> watcher_;

    unsigned int debounce_time_ms_ = 0;
    uint64_t last_isr_timestamp_ = 0;
};

#endif // WALL_SWITCH_H
