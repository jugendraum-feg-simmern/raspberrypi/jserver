/*!
 * \file i2cdevice.h
 * \brief Header for I2CDevice class
 */

#ifndef I2CDEVICE_H
#define I2CDEVICE_H

#include <stdint.h>
#include <stdexcept>
#include <errno.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include "inc/tools/logging.h"

// SMBUS config and commands
// fetched from wiringPi/wiringPiI2C.c
#define I2C_SMBUS               0x0720
#define I2C_SLAVE               0x0703
#define I2C_SMBUS_WRITE         0
#define I2C_SMBUS_READ          1
#define I2C_SMBUS_BYTE_DATA	    2

/*!
 * \brief The ioctl_smbus_args struct
 * \details group args needed for ioctl to access i2c device
 */
struct ioctl_smbus_args
{
    uint8_t rw;
    uint8_t command;
    int size;
    uint8_t* data;
};

/*!
 * \brief I2CDevice class
 * \details implements read(), write() and open() functionality for I²C devices
 * \note based upon work by Gordon Ramsey in the wiringPi library
 */
class I2CDevice
{
public:
    I2CDevice(uint8_t addr);
    ~I2CDevice();

    /*!
     * \brief setMocking
     * \param mocking when set to true, write to and read from RAM insted of hardware
     */
    void setMocking(bool mocking);

    /*!
     * \brief isMocked
     * \return true if the device is mocked
     */
    bool isMocked() {return mocked_;}

    /*!
     * \brief sets an internal filesdescriptor with open() and configures
     *      stream properties with ioctl()
     * \return 0 when successful, else errno
     */
    int openI2CDevice();

    /*!
     * \brief closes the internal filedescriptor
     * \return 0 when successful, else errno
     * \todo check if close(fd_) returns 0 when device not available
     */
    int closeI2CDevice();

    /*!
     * \brief checks if the filedescriptor is still valid using fcntl()
     * \return 0 when available, else errno
     * \todo verify return behaviour with real slave on i2c bus; if not
     *      as expected use test read of a WHOAMI register instead
     */
    int checkI2CDevice();

    /*!
     * \brief read content of reg into data
     * \param reg register to be read
     * \param data location to store register content
     * \return 0 when successful, else errno
     */
    int readReg8(uint8_t reg, uint8_t& data);

    /*!
     * \brief write byte of data into register
     * \param reg register to be written
     * \param data data to be written
     * \return 0 when successful, else errno
     */
    int writeReg8(uint8_t reg, uint8_t data);

    /*!
     * \brief return last encountered errno
     * \return last encountered errno
     */
    int lastError() {return last_error_;};

    uint8_t address() {return addr_;};

protected:
    int fd_ = -1;

private:
    uint8_t addr_;
    int last_error_;

    bool mocked_ = false;
    uint8_t mock_regs_[255] = {};    // act as regs of slave when mocked
};

#endif // I2CDEVICE_H
