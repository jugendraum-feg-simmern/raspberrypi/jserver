/*!
 * \file mcp23017_register.h
 * \brief register definition of MCP23017 portexpander
 */

#ifndef MCP23017_REGISTER_H
#define MCP23017_REGISTER_H

#define MCP23017_IODIRA     0x00
#define MCP23017_IODIRB     0x01
#define MCP23017_GPIOA      0x12
#define MCP23017_GPIOB      0x13

#endif // MCP23017_REGISTER_H
