/*!
 * \file pca9635.h
 * \brief header of PCA9635 class
 */
#ifndef PCA9635_H
#define PCA9635_H

#include "i2cdevice.h"
#include "iodevice.h"
#include "inc/hal/analog_output.h"

/*!
 * \brief Provide analog outputs via an PCA9635
 */
class PCA9635 : public IODevice, private I2CDevice
{
public:
    PCA9635(QString name, QJsonValue config);
    ~PCA9635() {};

    /*!
     * \brief initialize chip
     * \param mocking when set to true, write to and read from RAM insted of hardware
     * \param skip_init when set to true, the hardware is left unchanged, so no is not initialize. Default false.
     * \return 0 on successful initialization, else -1
     */
    int init(bool mocking, bool skip_init = false);

    int setRegisterValue(uint8_t reg, uint8_t value);
    int getRegisterValue(uint8_t reg, uint8_t& value);

private:
    AnalogOutputPtr analogOutputFromConfig(QJsonObject config);

    QString name_;
    QSet<AnalogOutputPtr> outputs_;
};

#endif // PCA9635_H
