/*!
 * \file simple_light.h
 * \brief Header of the SimpleLight class
 */
#ifndef SIMPLE_LIGHT_H
#define SIMPLE_LIGHT_H

#include <QObject>
#include "j_element.h"
#include "inc/hal/digital_output.h"

#include "rep_simple_light_source.h"

/*!
 * \brief A simple light that is either on or off and is not temperature controlled
 */
class SimpleLight : public SimpleLightSource, public JElement
{
    Q_OBJECT
public:
    SimpleLight(QString id, QJsonValue config, JElement *parent=nullptr);
    ~SimpleLight() {};

    bool on() const override;
    void setOn(bool on) override;

signals:
     void requestDigitalOutputStateChange(bool state);

private slots:
    void onDigitalOutputAdded(QString name, DigitalOutputPtr output);
    void onDigitalOutputRemoved(QString name);
    void onDigitalOutputStateChanged(bool state);

private:
    QString digital_output_name_;
    bool on_ = false;
};


#endif // SIMPLE_LIGHT_H
