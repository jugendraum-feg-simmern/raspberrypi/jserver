/*!
 * \file jugendraum.h
 * \brief header of main Jugendraum class.
 */
#ifndef JUGENDRAUM_H
#define JUGENDRAUM_H

#include <QTimer>
#include <QObject>
#include <QString>

#include "j_element.h"
#include "simple_rgb.h"
#include "simple_light.h"
#include "hauptlicht_streifen.h"
#include "decken_rgb_streifen.h"
#include "inc/hal/digital_output.h"

/*!
 * \brief Main class managing all elements and their availability via the API
 */
class Jugendraum : public QObject, public JElement
{
    Q_OBJECT
public:
    Jugendraum(QString id);
    ~Jugendraum();

    bool on() const override;
    void setOn(bool on) override;

    void saveAllToFile(QString filename);
    void loadAllFromFile(QString filename);
    void serialize(QJsonObject &json) override ;
    void deserialize(const QJsonObject &json) override;

signals:
    void onChanged(bool on);

private slots:
    void onWallSwitchStateChanged();
    void onElementOnChanged(bool on);
    void onDigitalOutputAdded(QString name, DigitalOutputPtr dout);
    void onDigitalOutputRemoved(QString name);

private:
    QList<JElement*> all_elements_;

    SimpleRGB* theken_rgb_;              			//!< RGB stripes inside the counter
    SimpleRGB* wand_rgb_;               	 		//!< RGB stripes at the gable
    SimpleLight* theken_deckenlicht_;  				//!< overhead lights of the counter
    SimpleLight* paletten_licht_;          			//!< illumination of some pallets behind the stage
    QHash<int, DeckenRGBStreifen*> decken_rgb_;		//!< all RGB stripes at the ceiling
    QHash<int, HauptlichtStreifen*> hauptlicht_;	//!< all white LED stripes at the cieling

    bool on_ = false;
    QString indicator_relais_name_;

};

#endif // JUGENDRAUM_H
