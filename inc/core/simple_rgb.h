/*!
 * \file simple_rgb.h
 * \brief header of the SimpleRGB class
 */
#ifndef SIMPLE_RGB_H
#define SIMPLE_RGB_H

#include <QColor>
#include "j_element.h"
#include "rep_rgb_light_source.h"
#include "inc/hal/analog_output.h"

/*!
 * \brief A simple RGB element using three \a AnalogOutputs without
 * 		dedicated power supply or temperature control
 */
class SimpleRGB : public RGBLightSource, public JElement
{
    Q_OBJECT
public:
    SimpleRGB(QString id, QJsonValue config, JElement *parant);
    ~SimpleRGB() {};

    bool on() const override;
    void setOn(bool on) override;
    QColor color() const override;
    void setColor(QColor color) override;

    void serialize(QJsonObject &json) override;
    void deserialize(const QJsonObject &json) override;

signals:
    void requestRedOutputChange(quint8 value);
    void requestGreenOutputChange(quint8 value);
    void requestBlueOutputChange(quint8 value);

private slots:
    void onAnalogOutputAdded(QString name, AnalogOutputPtr aout);
    void onAnalogOutputRemoved(QString name);
    void onRedOutputChanged(quint8 value);
    void onGreenOutputChanged(quint8 value);
    void onBlueOutputChanged(quint8 value);

private:
    QString red_output_name_, green_output_name_, blue_output_name_;
    QColor color_ = Qt::GlobalColor::black, last_color_ = Qt::GlobalColor::black;
};

#endif // SIMPLE_RGB
