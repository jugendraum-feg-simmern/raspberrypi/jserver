/*!
 * \file hauptlicht_streifen.h
 * \brief Header of the HauptlichtStreifen class
 */
#ifndef HAUPTLICHT_STREIFEN_H
#define HAUPTLICHT_STREIFEN_H

#include "dimmable_light.h"
#include "inc/hardware/powersupply.h"
#include "inc/temperature/temperature_controlled.h"

/*!
 * \brief The HauptlichtStreifen class extends a \a DimmableLight with a dedicated power supply
 * 		and a temperature controller
 */
class HauptlichtStreifen : public DimmableLight, public TemperatureControlled
{
public:
    HauptlichtStreifen(QString id, QJsonValue config, JElement *parent);
    ~HauptlichtStreifen() {};

    void setValue(quint8 value) override;

private slots:
    void onTemperatureDomainChanged(JTemperatureController::Domain domain) override;
    void onPowersupplyAdded(QString name, Powersupply *ps);
    void onPowersupplyRemoved(QString name);

private:
    QString powersupply_name_;
    Powersupply *powersupply_ = nullptr;
};

#endif // HAUPTLICHT_STREIFEN_H
