/*!
 * \file dimmable_light.h
 * \brief Header of DimmableLight class
 */
#ifndef DIMMABLE_LIGHT_H
#define DIMMABLE_LIGHT_H

#include "j_element.h"
#include "rep_dimmable_light_source.h"
#include "inc/hal/analog_output.h"

/*!
 * \brief A simple light whose brightness can be adjusted via an \a AnalogOutput
 * 		without dedicated powersupply or temperature control
 */
class DimmableLight : public DimmableLightSource, public JElement
{
    Q_OBJECT
public:
    DimmableLight(QString id, QJsonValue config,  JElement *parent);
    virtual ~DimmableLight() {};

    bool on() const override;
    void setOn(bool on) override;
    quint8 value() const override;
    void setValue(quint8) override;

    void serialize(QJsonObject& json) override;
    void deserialize(const QJsonObject& json) override;

signals:
    void requestAnalogOutputValueChange(quint8 value);

private slots:
    void onAnalogOutputAdded(QString name, AnalogOutputPtr aout);
    void onAnalogOutputRemoved(QString name);
    void onAnalogOutputValueChanged(quint8 value);

private:
    quint8 value_ = 0, last_value_ = 0;
    QString analog_output_name_;
};

#endif // DIMMABLE_LIGHT_H
