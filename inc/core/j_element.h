/*!
 * \file j_element.h
 * \brief header of JElement base class
 */
#ifndef J_ELEMENT_H
#define J_ELEMENT_H

#include <QDebug>
#include <QString>

#include "inc/tools/logging.h"
#include "inc/tools/storable.h"
#include "inc/tools/j_return_type.h"

/*!
 * \brief base class of all core elements
 */
class JElement : public Storable
{
public:
    JElement(const JElement&) = default;
    JElement(QString id, JElement *parent) : Storable(id, parent) {};
    virtual ~JElement() {};

    virtual bool on() const = 0;
    virtual void setOn(bool on) = 0;

    virtual void serialize(QJsonObject &json) override;
    virtual void deserialize(const QJsonObject &json) override;
};

#endif // J_ELEMENT_H
