/*!
 * \file rgb_streifen.h
 * \brief header of DeckenRGBStreifen
 */
#ifndef DECKEN_RGB_STREIFEN_H
#define DECKEN_RGB_STREIFEN_H

#include "simple_rgb.h"
#include "inc/hardware/powersupply.h"
#include "inc/temperature/temperature_controlled.h"

/*!
 * \brief The DeckenRGBStreifen class extends a \a SimpleRGB light with a dedicated power supply
 * 		and a temperature controller
 */
class DeckenRGBStreifen : public SimpleRGB, public TemperatureControlled
{
public:
    DeckenRGBStreifen(QString id, QJsonValue config, JElement *parent);
    ~DeckenRGBStreifen() {};

    void setColor(QColor) override;

private slots:
    void onTemperatureDomainChanged(JTemperatureController::Domain) override;
    void onPowersupplyAdded(QString name, Powersupply *ps);
    void onPowersupplyRemoved(QString name);
private:
    QString powersupply_name_;
    Powersupply *powersupply_;
};

#endif // RGB_STREIFEN_H
