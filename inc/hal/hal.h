#ifndef HAL_H
#define HAL_H

#include <QString>

#include "observer.h"
#include "analog_output.h"
#include "digital_output.h"
#include "analog_input.h"
#include "digital_input.h"

#include "inc/hardware/powersupply.h"

/*!
 * \brief The Hardware Abstraction Layer provides analog and digital controllers as well as powersupplies
 * 		to high level components.
 * \todo It feels like powersupplies do not really belong into the HAL...
 * 		Can we get rid of them without accessing the hw directly from the core elements?
 */
namespace HAL
{
    extern Observer *observer;

    void addDigitalOutput(DigitalOutputPtr& dout);
    void removeDigitalOutput(QString name);
    DigitalOutputPtr getDigitalOutputByName(QString name);

    void addDigitalInput(DigitalInputPtr& din);
    void removeDigitalInput(QString name);
    DigitalInputPtr getDigitalInputByName(QString name);

    void addAnalogOutput(AnalogOutputPtr& aout);
    void removeAnalogOutput(QString name);
    AnalogOutputPtr getAnalogOutputByName(QString name);

    void addAnalogInput(AnalogInputPtr& ain);
    void removeAnalogInput(QString name);
    AnalogInputPtr getAnalogInputByName(QString name);

    void registerPowersupply(QString name, Powersupply *powersupply);
    void removePowersupply(QString name);
    Powersupply *getPowersupplyByName(QString name);

    void dump();
}

#endif // HAL_H
