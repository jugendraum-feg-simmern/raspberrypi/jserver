#ifndef ANALOG_INPUT_H
#define ANALOG_INPUT_H

#include <QObject>
#include <QSharedPointer>

class AnalogInput : public QObject
{
    Q_OBJECT
    Q_PROPERTY(double value READ value NOTIFY valueChanged)

public:
    AnalogInput(QString name);
    virtual ~AnalogInput() {}
    QString name() { return name_; };
    void update(double value, bool force_signal=false);

public slots:
    double value();

signals:
    void valueChanged(double value);

private:
    QString name_;
    double value_ = 0;
};

typedef QSharedPointer<AnalogInput> AnalogInputPtr ;

#endif // ANALOG_INPUT_H
