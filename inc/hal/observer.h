#ifndef OBSERVER_H
#define OBSERVER_H

#include <QObject>
#include <QString>

#include "analog_output.h"
#include "analog_input.h"
#include "digital_output.h"
#include "digital_input.h"
#include "inc/hardware/powersupply.h"

/*!
 * \brief The Observer class is used to emit signals on Events of the HAL layer.
 * 		An instance of a QObject is required as a sender, so we use this class.
 */
class Observer : public QObject
{
    Q_OBJECT
public:
    Observer() {};
    virtual ~Observer() {};
signals:
    void digitalOutputAdded(QString, DigitalOutputPtr);
    void digitalOutputRemoved(QString);
    void digitalInputAdded(QString, DigitalInputPtr);
    void digitalInputRemoved(QString);
    void analogOutputAdded(QString, AnalogOutputPtr);
    void analogOutputRemoved(QString);
    void analogInputAdded(QString, AnalogInputPtr);
    void analogInputRemoved(QString);
    void powersupplyAdded(QString, Powersupply*);
    void powersupplyRemoved(QString);
};

#endif //OBSERVER_H
