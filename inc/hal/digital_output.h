#ifndef DIGITAL_OUTPUT_H
#define DIGITAL_OUTPUT_H

#include <QObject>
#include <QSharedPointer>
#include <functional>

class DigitalOutput : public QObject
{
    typedef std::function<int(bool)> setter_func_t;
    typedef std::function<int(bool&)> getter_func_t;

    Q_OBJECT
    Q_PROPERTY(bool state READ state WRITE setState NOTIFY stateChanged)

public:
    DigitalOutput(QString name, setter_func_t setter, getter_func_t getter);
    virtual ~DigitalOutput() {}
    QString name() { return name_; };
public slots:
    bool state();
    void setState(bool state);
signals:
    void stateChanged(bool state);
private:
    QString name_;
    bool state_ = false;
    getter_func_t getter_;
    setter_func_t setter_;
};

typedef QSharedPointer<DigitalOutput> DigitalOutputPtr;

#endif // DIGITAL_OUTPUT_H
