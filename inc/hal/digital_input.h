#ifndef DIGITAL_INPUT_H
#define DIGITAL_INPUT_H

#include <QObject>
#include <QSharedPointer>

class DigitalInput : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool state READ state NOTIFY stateChanged)

public:
    DigitalInput(QString name);
    virtual ~DigitalInput() {}

    QString name() { return name_; };
    void update(bool state, bool force_signal=false);

public slots:
    bool state() { return state_; };

signals:
    void stateChanged(bool state);

private:
    QString name_;
    bool state_;
};

typedef QSharedPointer<DigitalInput> DigitalInputPtr;

#endif // DIGITAL_INPUT_H
