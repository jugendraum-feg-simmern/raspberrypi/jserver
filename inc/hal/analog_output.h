#ifndef ANALOG_OUTPUT_H
#define ANALOG_OUTPUT_H

#include <QObject>
#include <QSharedPointer>
#include <functional>


class AnalogOutput : public QObject
{
    typedef std::function<int(quint8)> setter_func_t;
    typedef std::function<int(quint8&)> getter_func_t;

    Q_OBJECT
    Q_PROPERTY(quint8 value READ value WRITE setValue NOTIFY valueChanged)

public:
    AnalogOutput(QString name, setter_func_t setter, getter_func_t getter);
    virtual ~AnalogOutput() {}
    QString name() { return name_; };
public slots:
    quint8 value();
    void setValue(quint8 value);
signals:
    void valueChanged(quint8 value);
private:
    QString name_;
    quint8 value_ = 0;
    getter_func_t getter_;
    setter_func_t setter_;
};

typedef QSharedPointer<AnalogOutput> AnalogOutputPtr;

#endif // ANALOG_OUTPUT_H
