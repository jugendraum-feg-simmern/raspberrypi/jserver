/*!
 * \file config.h
 * \brief main config
 */
#ifndef CONFIG_H
#define CONFIG_H

// time constants of hardware
#define JUGENDRAUM_POLL_INTERVAL_S      5

#include <QJsonValue>
#include <QStringList>

namespace Config
{
    enum Category {
        Core,
        Hardware,
        Temperature,
    };

    namespace Files
    {
        extern QString Logfile;
        extern QString YouthSettings;
        extern QString TeensSettings;
        extern QString DefaultSettings;
        extern QString OffSettings;
        extern QString TemperatureSettings;
    }

    /*!
     * \brief set the file from which the values of a given category are retrieved.
     * \param category The category to be updated.
     * \param file Path to the config file.
     */
    void setFileForCategory(Category category, QString file);

    /*!
     * \brief getElementByKey Get the config from a category
     * \param category The category of the configuration. Ultimately determines the file from which the config is read.
     * \param path Path in reverse order to the node (e.g. "path.to.key" -> ["key", "to", "path"]).
     * 		May be empty to read the entire node of the element
     * \return json value at \a path in \a category. Is undefined if file not present or path invalid.
     */
    QJsonValue getElementByKey(Category category, QStringList path = QStringList());
    QJsonValue getElementByKey(Category category, QString key);
}

#endif // CONFIG_H
