#ifndef TEMPERATURE_H
#define TEMPERATURE_H

#include <algorithm>
#include <QSet>
#include "j_temperature_controller.h"
#include "temperature_controlled.h"

/*!
 * \brief The \a Temperature namespace holds all JTemperatureControllers and provides
 * 		the \a constrain() method to limit values depending upon the current domain
 * 		of a temperature controller
 */
namespace Temperature
{
    extern JTemperatureController *powersupply;
    extern JTemperatureController *cabin;
    extern JTemperatureController *onkyo;
    extern JTemperatureController *pc;
    extern JTemperatureController *pi;

    extern QHash<TemperatureControlled*, QSet<JTemperatureController*>> element_controller_map;

    /*!
     * \brief add a temperature controlled element to a controller. Automatically
     * 		connects the onDomainChanged slot of the element to the controller.
     */
    extern void registerToController(JTemperatureController *controller, TemperatureControlled *element);
    extern void unregisterFromController(JTemperatureController *controller, TemperatureControlled *element);

    template <typename T>
    bool constrain(TemperatureControlled *element, T& value)
    {
        bool constrained = false;
        for (auto tc : element_controller_map.value(element))
            constrained |= tc->constrain(value);
        return constrained;
    }

    extern void exit();
}

#endif // TEMPERATURE_H
