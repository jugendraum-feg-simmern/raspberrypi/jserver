/*!
 * \file j_temperature_controller.h
 * \brief header of jTemperatureController class
 */
#ifndef J_TEMPERATUR_CONTROLLER_H
#define J_TEMPERATUR_CONTROLLER_H

#include <QObject>
#include <QString>
#include <QTimer>
#include "inc/tools/errno.h"

#include "inc/tools/j_return_type.h"
#include "inc/tools/storable.h"
#include "inc/hal/analog_input.h"
#include "inc/hal/analog_output.h"


// default parameters for jTemperatureController
#define TEMP_UPDATE_INTERVAL_DEFAULT    3
#define TEMP_FAN_MAX_DEFAULT            40
#define TEMP_HIGH_DEFAULT               40
#define TEMP_CRIT_DEFAULT               60
#define TEMP_NOISE_THRESHOLD_DEFAULT	1
#define TEMP_HYSTERESIS_DEFAULT         5
#define TEMP_FAN_START_DEFAULT          25
#define FAN_MIN_DEFAULT                 0x7f

#define LIMIT_UINT8_T					(255 / 2)

/*!
 * \brief base class for temperature controllers
 * \details This class handles fan control based upon the current temperature. The fan curve is depicted here:
 *  \code
 *     FAN  ^
 *          |                      :      :      :
 *      255 +                      +------:------:--
 *          |                     /:      :      :
 *          |                    / :      :      :
 *          |                  ,'  :      :      :
 *          |              ..'     :      :      :
 *  fan_min +      +----'''        :      :      :
 *          |      |               :      :      :
 *        0 +------:---------------:------:------:-->  temperature
 *             fan_start       fan_max  high  crit
 *                                        :      :
 *      DOMAINS                           :      :
 *          |------------Normal-----------|-High-|-Critical
 *
 *  \endcode
 * Description of the values marked temperatures
 *   \li \a fan_start: below that temperature, the fan is off. On the threshold, the fan speed is set to its minimal speed.
 *   \li \a fan_max: at this temperature, the fan is spinning with max rpm
 *   \li \a high: the power is reduced
 *   \li \a crit[ical]: force the device to off
 *  All temperatures have a hysteresys of \a fanHysteresis().
 */
class JTemperatureController : public QObject, public Storable
{
    Q_OBJECT
    Q_PROPERTY(double temperature READ temperature NOTIFY temperatureChanged)
    Q_PROPERTY(quint8 fanSpeed READ fanSpeed NOTIFY fanSpeedChanged)
    Q_PROPERTY(double temperatureFanStart READ temperatureFanStart WRITE setTemperatureFanStart NOTIFY temperatureFanStartChanged)
    Q_PROPERTY(double temperatureFanMax READ temperatureFanMax WRITE setTemperatureFanMax NOTIFY temperatureFanMaxChanged)
    Q_PROPERTY(double temperatureHigh READ temperatureHigh WRITE setTemperatureHigh NOTIFY temperatureHighChanged)
    Q_PROPERTY(double temperatureCritical READ temperatureCritical WRITE setTemperatureCritical NOTIFY temperatureCriticalChanged)
    Q_PROPERTY(double temperatureHysteresis READ temperatureHysteresis WRITE setTemperatureHysteresis NOTIFY temperatureHysteresisChanged)
    Q_PROPERTY(double temperatureNoiseThreshold READ temperatureNoiseThreshold WRITE setTemperatureNoiseThreshold NOTIFY temperatureNoiseThresholdChanged)
    Q_PROPERTY(quint8 fanSpeedMinimum READ fanSpeedMinimum WRITE setFanSpeedMinimum NOTIFY fanSpeedMinimumChanged)
public:
    /*!
     * \brief domains of the temperature in which different actions need to be taken
     */
    enum Domain
    {
        Normal,
        High,
        Critical
    };

    /*!
     * \brief constructor of JTemperatureController with fan
     * \param id of the device
     * \param parent parent of the controller
     * \param config JSON config
     */
    explicit JTemperatureController(QString id, Storable *parent, QJsonValue config);
    ~JTemperatureController();

    /*!
     * \brief constrain the value based upon the current domain of the temperature controller
     * \details The following constraining mechanism is implemented based upon the current
     * 		Domain of the temperature controller
     * 		\li \a Normal: unconstrained
     * 		\li \a High: constrained to 50 % of maximal possible value
     * 		\li \a Critical: always 0
     * \param value the value to constrain passed by reference
     * \return true if value has been constrained
     * \todo Discuss constraining mechanism with all project members
     */
    bool constrain(uint8_t& value);

    /*!
     * \brief constrain a bool. See \a constrain() for more details.
     * \details The following constraining mechanism is implemented based upon the current
     * 		Domain of the temperature controller
     * 		\li \a Normal and \a High: unconstrained
     * 		\li \a Critical: always false
     */
    bool constrain(bool& value);

    /*!
     * \brief constrain a color. See \a constrain() for more details.
     * \details The following constraining mechanism is implemented based upon the current
     * 		Domain of the temperature controller
     * 		\li \a Normal: unconstrained
     * 		\li \a High: Constrain the value ("brightness") to 50 % (see HSV color model)
     * 		\li \a Critical: always black
     */
    bool constrain(QColor& color);

    void serialize(QJsonObject &json) override;
    void deserialize(const QJsonObject &json) override;

public slots:
    double temperature() { return temperature_; };
    quint8 fanSpeed() { return fan_speed_; };
    Domain domain() { return domain_; };

    double temperatureFanStart() {return temp_fan_start_; };
    void setTemperatureFanStart(double temp_fan_start);
    double temperatureFanMax() {return temp_fan_max_; };
    void setTemperatureFanMax(double temp_fan_max);
    double temperatureHigh() { return temp_high_; }
    void setTemperatureHigh(double temp_high);
    double temperatureCritical() {return temp_crit_; };
    void setTemperatureCritical(double temp_critical);
    double temperatureHysteresis() {return temp_hysteresis_; };
    void setTemperatureHysteresis(double temp_hysteresis);
    double temperatureNoiseThreshold() {return temp_noise_threshold_; };
    void setTemperatureNoiseThreshold(double temp_noise_threshold);
    quint8 fanSpeedMinimum() {return fan_min_; };
    void setFanSpeedMinimum(quint8 min);

signals:
    void temperatureChanged(double);
    void fanSpeedChanged(quint8);
    void temperatureFanStartChanged(double);
    void temperatureFanMaxChanged(double);
    void temperatureHighChanged(double);
    void temperatureCriticalChanged(double);
    void temperatureNoiseThresholdChanged(double);
    void temperatureHysteresisChanged(double);
    void fanSpeedMinimumChanged(quint8);

    void domainChanged(Domain domain);

    void requestFanSpeedChange(quint8 fan_speed);

private slots:
    void onAnalogInputAdded(QString name, AnalogInputPtr ain);
    void onAnalogInputRemoved(QString name);
    void onAnalogOutputAdded(QString name, AnalogOutputPtr aout);
    void onAnalogOutputRemoved(QString name);

    void onAnalogOutputValueChanged(quint8 value);
    void onAnalogInputValueChanged(double value);

private:
    // operating conditions
    double temperature_;
    quint8 fan_speed_;
    void setFanSpeed(quint8 fan_speed);
    Domain domain_ = Domain::Normal;
    void setDomain(Domain domain);

    // configuration parameters
    double temp_fan_start_ = TEMP_FAN_START_DEFAULT;
    double temp_fan_max_ = TEMP_FAN_MAX_DEFAULT;
    double temp_high_ = TEMP_HIGH_DEFAULT;
    double temp_crit_ = TEMP_CRIT_DEFAULT;
    double temp_hysteresis_ = TEMP_HYSTERESIS_DEFAULT;
    double temp_noise_threshold_ = TEMP_NOISE_THRESHOLD_DEFAULT;
    quint8 fan_min_ = FAN_MIN_DEFAULT;

protected:
    bool has_fan_ = false;
    bool has_sensor_ = false;
    QString analog_input_name_, analog_output_name_;
};
#endif // J_TEMPERATUR_CONTROLLER_H
