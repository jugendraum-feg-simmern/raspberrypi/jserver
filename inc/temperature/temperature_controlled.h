#ifndef TEMPERATURE_CONTROLLED_H
#define TEMPERATURE_CONTROLLED_H

#include "j_temperature_controller.h"

class TemperatureControlled
{
public:
    virtual void onTemperatureDomainChanged(JTemperatureController::Domain) = 0;
};

#endif // TEMPERATURE_CONTROLLED_H
