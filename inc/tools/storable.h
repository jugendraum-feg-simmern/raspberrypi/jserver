/*!
 * \file storable.h
 * \brief Header of Storable class
 */
#ifndef STORABLE_H
#define STORABLE_H

#include <QString>
#include <QStringList>
#include <QJsonValue>
#include <QJsonObject>
#include <QJsonDocument>
#include <QHash>
#include <QMutex>

#define MAX_MUTEXES	16

/*!
 * \brief provide serialization and deserialization of data a well as file access.
 */
class Storable
{
public:
    Storable(const Storable&) = default;
    Storable(QString id=QString(), Storable *parent=nullptr) : id_(id), parent_(parent) {};
    ~Storable();

    QString id() { return id_; };
    Storable *parent() { return parent_; };

    /*!
     * \brief save all important data into a json object
     * \param json reference to a QJsonObject
     */
    virtual void serialize(QJsonObject &json) {};

    /*!
     * \brief load data from a json object
     * \param json reference to a QJsonObject
     */
    virtual void deserialize(const QJsonObject &json) {};

    /*!
     * \brief update the JSON node with the \a path in the \a file
     * \param value new data
     * \param filename File to update the node in
     * \param path Path in reverse order to the node in the file (e.g. "path.to.key" -> ["key", "to", "path"]).
     */
    void updateJsonValueInFile(const QJsonValue &value, QString filename, QStringList path = QStringList());

    /*!
     * \brief get a JSON value from a \a file
     * \details When \a parent_ is valid, the own \a id_ is appended to \a path and
     * 		parent_->getJsonObjectFromFile() is called with the updated \a path.
     * \param filename File to read the value from
     * \param path Path in reverse order to the node in the file (e.g. "path.to.key" -> ["key", "to", "path"]).
     * 		May be empty to read the entire node of the element
     * \return json value at \a path in \a filename. Is undefined if file not present or path invalid.
     */
    QJsonValue getJsonValueFromFile(QString filename, QStringList path = QStringList());

private:
    /*!
     * \brief update the value with a given path in a QJsonObject. If the node is not present, it is created.
     * \details QJsonObject works with objects rather than references, so it is not possible to modify
     * 		just one entry. We workaround this by recursively extracting the node from the object, updating
     * 		it and then rewriting the rest of the object. This code is heavily inspired by work from sl.sy.ifm
     * 		in \link https://forum.qt.io/topic/25096/modify-nested-qjsonvalue/4 \endlink. See also this discussion
     * 		\link https://stackoverflow.com/questions/17034336/how-to-change-qjsonobject-value-in-a-qjson-hierarchy-without-using-copies \endlink.
     * \param root_obj reference to the object to uddate
     * \param new_value value to write into the object
     * \param path to the value in the object
     * \todo Do we need a mutex for accessing the files?
     */
    void updateJsonObject(QJsonObject &root_obj, const QJsonValue &new_value, QStringList path);

    /*!
     * \brief create a mutex and insert it into the list of tracked mutexes.
     * 		When more then \a MAX_MUTEXES mutexes exist, all currently unlocked ones are dropped
     * \param mutex_id idemtifier of the mutex.
     * \attention When all mutexes are locked, the size of mutexes_ can grow beyond \a MAX_MUTEXES.
     */
    void createMutex(QString mutex_id);

    /*!
     * \brief check if the mutex is locked.
     * \details QMutex does not offer this functionality, so we test this by trying to acquire a lock. If this
     * 		succeeds, we can deduce that the mutex is not locked.
     * \param mutex pointer to the mutex to test.
     * \return true when a lock for the mutex cannot be acquired.
     */
    bool isLocked(QMutex *mutex);

    QString id_;
    Storable *parent_;
    QHash<QString, QMutex*> mutexes_;
};

#endif // STORABLE_H
