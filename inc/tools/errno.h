/*!
 * \file errno.h
 * \brief definitions of error codes
 * \details if feasible, use same codes as linux
 */
#ifndef ERRNO_H
#define ERRNO_H

#define OK          0
#define ECONVERSION 1   //!< conversion error
#define ENOTAVAIL   2   //!< device not available
#define ENOTIMPL    3   //!< not implemented
#define EOUTOFRANGE 4   //!< index out of range
#define EMATCH      5   //!< value read from hardware does not match expectation
#define EBADMSG     74  //!< message not as expected
#define ENOTSUP     95  //!< operation not supported
#define ETIMEDOUT   110 //!< timeout error
#define ENULLPTR    127 //!< nullptr called
#endif // ERRNO_H
