/*!
 * \file j_return_type.h
 * \brief Header for JReturnType
 */
#ifndef J_RETURN_TYPE_H
#define J_RETURN_TYPE_H

/*!
 * \brief general return type to return value and errno
 * \details The standard way of passing a pointer for the return value and
 *      returning the errno does not work with asynchronous program flow
 * \note using struct instead of std::pair to allow for future extension if necassary
 */
template <typename T>
struct JReturnType {
    int err_code = 0;
    T value = 0;

    JReturnType() : err_code(), value() {}
    JReturnType(int err) : err_code(err), value() {}
    JReturnType(int err, T val) : err_code(err), value(val) {}
};

/*!
 * \brief partial specialization of JReturnType to properly handle void
 */
template<>
struct JReturnType<void> {
    int err_code = 0;

    JReturnType() : err_code() {}
    JReturnType(int err) : err_code(err) {}
};


#endif // J_RETURN_TYPE_H
