#ifndef LOGGING_H
#define LOGGING_H

#include <QtGlobal>
#include <QDateTime>
#include <QFile>
#include <QLoggingCategory>

#include "inc/config.h"

// trace function calls
Q_DECLARE_LOGGING_CATEGORY(traceHardware)
Q_DECLARE_LOGGING_CATEGORY(traceArtnet)
Q_DECLARE_LOGGING_CATEGORY(traceCore)
Q_DECLARE_LOGGING_CATEGORY(traceTools)
Q_DECLARE_LOGGING_CATEGORY(traceInit)
Q_DECLARE_LOGGING_CATEGORY(traceTemperature)

// general output
Q_DECLARE_LOGGING_CATEGORY(hardware_log)
Q_DECLARE_LOGGING_CATEGORY(file_log)
Q_DECLARE_LOGGING_CATEGORY(temperature_log)
Q_DECLARE_LOGGING_CATEGORY(network_log)
Q_DECLARE_LOGGING_CATEGORY(api_log)

/*!
 * \brief custom handler to process qDebug, qInfo, ... messages
 * \details messages with higher priority than debug get logged to a file as well as printed to stderr
 * \param type
 * \param context
 * \param msg
 * \note based upon the example provided in the Qt docs
 *      https://doc.qt.io/qt-5/qcommandlineparser.html#how-to-use-qcommandlineparser-in-complex-applications
 */
void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString& msg);

void log(QtMsgType type, const QString &msg);

#endif // LOGGING_H
